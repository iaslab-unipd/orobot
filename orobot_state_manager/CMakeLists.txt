cmake_minimum_required(VERSION 2.8.3)
project(orobot_state_manager)

find_package(catkin REQUIRED
	roscpp
	rospy
	std_msgs
	message_generation)

add_service_files(
  FILES
  FallDetectionCommunicationService.srv
  PersonDetectionCommunicationService.srv
  PersonRecognitionCommunicationService.srv
  NavigationAckService.srv
  FaceRecognitionRequestService.srv
  FaceRecognitionFinishedService.srv
)

generate_messages(
  DEPENDENCIES
  std_msgs  # Or other packages containing msgs
)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES orobot_state_manager
#  CATKIN_DEPENDS other_catkin_pkg
#  DEPENDS system_lib
)
