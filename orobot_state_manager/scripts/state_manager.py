#!/usr/bin/env python

from geometry_msgs.msg import Point, Quaternion, Pose, PoseStamped
from move_base_msgs.msg import MoveBaseGoal
from orobot_srvs.srv import *
from orobot_state_manager.srv import *
from simple_navigation_goals.srv import *

import actionlib
from kobuki_msgs.msg import AutoDockingAction, AutoDockingGoal
from actionlib_msgs.msg import GoalStatus

import json
import rospy
import sys
import time

#robotId = 'f7bcc568-2b97-422e-9932-a503b084a397'

NAME = "OROBOT_STATE_MANAGER:\t\t"

class StateManager:

    def __init__(self):
        self.robotId = 'f7bcc568-2b97-422e-9932-a503b084a397'
        self.goals_array = []
        self.next_goal = 0
        self.is_navigation_stopped = False
        self.is_face_recognition_active_ = False
        self.rate = rospy.Rate(30)

    def set_goals(self, list_of_goals):
        self.goals_array = list_of_goals

        #print self.goals_array

    def send_navigation_stop_request(self):
        rospy.wait_for_service('one_goal_navigation_node/stop_navigation')
        try:
            self.is_navigation_stopped = True
            stop_simple_navigation = rospy.ServiceProxy('one_goal_navigation_node/stop_navigation', StopSimpleNavigation)
            stop_simple_navigation()
            return True
        except rospy.ServiceException, e:
            print NAME + "Service call failed: %s"%e
            return False

    def handle_fall_detection_communication_service(self, req):
        print NAME + "Sending FALLEN_PERSON message"

        data = {}
        payload = {}

        data['header'] = 'FALLEN_PERSON'
        
        payload['robotId'] = self.robotId
        payload['x'] = req.x
        payload['y'] = req.y
        
        data['payload'] = payload

        json_data = json.dumps(data)
        
        print json_data

        if not(self.is_navigation_stopped):
            print NAME + "Sending NAVIGATION STOP request"
            ret = self.send_navigation_stop_request()
            print NAME + "Was navigation stopped? " + str(ret)

        return True

    def handle_navigation_communication_service(self, req):
        print NAME + "Sending NAVIGATION message"

        if self.is_navigation_stopped:
            if self.is_face_recognition_active_:
                print NAME + "Face recognition preempted the navigation"
                return goals
            else:
                self.is_navigation_stopped = False

        if len(self.goals_array) < self.next_goal + 1:
            print NAME + "Nothing to send"
            time.sleep(1)
            goals = []
            return goals
       
        print NAME + "self.next_goal"
        print self.next_goal

        p = Point()
        p.x = self.goals_array[self.next_goal][0]
        p.y = self.goals_array[self.next_goal][1]
        p.z = self.goals_array[self.next_goal][2]
        
        q = Quaternion()
        q.x = self.goals_array[self.next_goal][3]
        q.y = self.goals_array[self.next_goal][4]
        q.z = self.goals_array[self.next_goal][5]
        q.w = 1

        pose = Pose()
        pose.position = p
        pose.orientation = q

        pose_stamped = PoseStamped()
        pose_stamped.header.seq = 0
        pose_stamped.header.stamp = rospy.Time.now()
        pose_stamped.pose = pose
        pose_stamped.header.frame_id = "map"

        goal = MoveBaseGoal()
        goal.target_pose = pose_stamped

        print NAME + "goal"
        print goal

        res = GoalsArrayResponse()
        res.goals.append(goal)

        return res

    def handle_navigation_ack_service(self, req):
        print NAME + "ACK received"
        print NAME + "next pose is available"

        if not(self.is_navigation_stopped):
            self.next_goal = self.next_goal + 1;

            if len(self.goals_array) <= self.next_goal:
                #self.next_goal = 0
                self.perform_autodocking()
                rospy.signal_shutdown("Spartaaaaaa")

        return True

    def handle_face_recognition_request_service(self, req):
        print NAME + "Face Recognition request to stop the navigation received"
        if not self.is_navigation_stopped:
            if not self.send_navigation_stop_request():
                print NAME + "Error: Face Recognition request rejected"
                return False
        self.is_face_recognition_active_ = True
        return True

    def handle_face_recognition_finished_service(self, req):
        print NAME + "Face Recognition finished, request to resume the navigation"
        self.is_face_recognition_active_ = False
        return True


    def handle_person_detection_communication_service(self, req):
        print NAME + "Sending DETECTED_PERSON message"

        data = {}
        payload = {}

        data['header'] = 'DETECTED_PERSON'

        payload['robotId'] = self.robotId
        payload['x'] = req.x
        payload['y'] = req.y
        payload['id'] = req.id

        data['payload'] = payload

        json_data = json.dumps(data)

        print NAME + json_data
        return True

    def handle_person_recognition_communication_service(self, req):
        print NAME + "Sending RECOGNIZED_PERSON message"

        data = {}
        payload = {}

        data['header'] = 'RECOGNIZED_PERSON'

        payload['robotId'] = self.robotId
        payload['name'] = req.name

        data['payload'] = payload

        json_data = json.dumps(data)

        print NAME + json_data
        return True


    def doneCb(status, result, self):
        state = ''
        if 0: print ''
        elif status == GoalStatus.PENDING   : state='PENDING'
        elif status == GoalStatus.ACTIVE    : state='ACTIVE'
        elif status == GoalStatus.PREEMPTED : state='PREEMPTED'
        elif status == GoalStatus.SUCCEEDED : state='SUCCEEDED'
        elif status == GoalStatus.ABORTED   : state='ABORTED'
        elif status == GoalStatus.REJECTED  : state='REJECTED'
        elif status == GoalStatus.PREEMPTING: state='PREEMPTING'
        elif status == GoalStatus.RECALLING : state='RECALLING'
        elif status == GoalStatus.RECALLED  : state='RECALLED'
        elif status == GoalStatus.LOST      : state='LOST'
        # Print state of action server
        #print 'Result - [ActionServer: ' + state + ']: ' + result.text

    def activeCb(self):
        if 0: print 'Action server went active.'

    def feedbackCb(feedback, self):
        # Print state of dock_drive module (or node.)
        # print 'Feedback: [DockDrive: ' + feedback.state + ']: ' + feedback.text
        #print 'Feedback... boh'
        pass
    def perform_autodocking(self):
        print NAME + "Navigation finished, performing Autodocking..."
        ########
        # Disabling safety controller
        pub = rospy.Publisher('/kobuki_safety_controller/disable', std_msgs.msg.Empty, queue_size=1)
        while(not pub.get_num_connections() > 0):
            rospy.sleep(0.2)
        for i in xrange(10):
            pub.publish()
            rospy.sleep(0.2)
        print NAME + " Published disable safety"
        ########
        time.sleep(1.5)
        client = actionlib.SimpleActionClient('dock_drive_action', AutoDockingAction)
        while not client.wait_for_server(rospy.Duration(5.0)):
            if rospy.is_shutdown(): return
            print 'Action server is not connected yet. still waiting...'
        goal = AutoDockingGoal();
        client.send_goal(goal, self.doneCb, self.activeCb, self.feedbackCb)
        print 'Goal: Sent.'
        rospy.on_shutdown(client.cancel_goal)
        client.wait_for_result()
        return client.get_result()

def fall_detection_server(state_manager):
    s = rospy.Service('orobot_state_manager/fall_detection_communication_service', 
        FallDetectionCommunicationService,
        state_manager.handle_fall_detection_communication_service)
    print NAME + "Ready to send FALLEN_PERSON message."

def navigation_server(state_manager):
    s = rospy.Service('goals_array', 
        GoalsArray,
        state_manager.handle_navigation_communication_service)
    print NAME + "Ready to send NAVIGATION GOAL."

def navigation_ack_server(state_manager):
    s = rospy.Service('orobot_state_manager/one_goal_navigation_ack', 
        NavigationAckService,
        state_manager.handle_navigation_ack_service)
    print NAME + "Ready to receive NAVIGATION ACK."

def person_detection_server(state_manager):
    s = rospy.Service('orobot_state_manager/person_detection_communication_service', 
        PersonDetectionCommunicationService,
        state_manager.handle_person_detection_communication_service)
    print NAME + "Ready to send DETECTED_PERSON message."

def person_recognition_server(state_manager):
    s = rospy.Service('orobot_state_manager/person_recognition_communication_service', 
        PersonRecognitionCommunicationService,
        state_manager.handle_person_recognition_communication_service)
    print NAME + "Ready to send RECOGNIZED_PERSON message."

def face_recognition_request_server(state_manager):
    s = rospy.Service('orobot_state_manager/face_recognition_request_service',
        FaceRecognitionRequestService,
        state_manager.handle_face_recognition_request_service)

def face_recognition_finished_server(state_manager):
    s = rospy.Service('orobot_state_manager/face_recognition_finished_service',
        FaceRecognitionFinishedService,
        state_manager.handle_face_recognition_finished_service)

if __name__ == "__main__":
    rospy.init_node('orobot_state_manager')

    s = StateManager()

    fall_detection_server(s)
    person_detection_server(s)
    person_recognition_server(s)
    face_recognition_request_server(s)
    face_recognition_finished_server(s)

    if (not(rospy.has_param('/orobot_state_manager/rows')) or
        not(rospy.has_param('/orobot_state_manager/cols')) or
        not(rospy.has_param('/orobot_state_manager/positions')) or
        not(rospy.has_param('/orobot_state_manager/position_matrix'))):
        sys.exit(-1)

    rows = rospy.get_param('/orobot_state_manager/rows') # 1
    cols = rospy.get_param('/orobot_state_manager/cols') # 6
    number_of_positions = rospy.get_param('/orobot_state_manager/positions')

    position_matrix = rospy.get_param('/orobot_state_manager/position_matrix')

    if len(position_matrix)/cols != number_of_positions:
        print NAME + "Please check positions in config file"
        sys.exit(-1)

    i = 0
    position_list=[]
    while i < len(position_matrix):
      position_list.append(position_matrix[i : i + cols])
      i += cols

    s.set_goals(position_list)

    navigation_server(s)
    navigation_ack_server(s)

    rospy.spin()
