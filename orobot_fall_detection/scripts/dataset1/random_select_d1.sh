#!/bin/bash

input_dir=~/Datasets/Omitech/Fall_detection/fall_detection_people/kinect2;
output_training_dir=~/Datasets/Omitech/Fall_detection/fall_detection_people/training_set;
output_test_dir=~/Datasets/Omitech/Fall_detection/fall_detection_people/test_set;

rm "$output_training_dir"/*
rm "$output_test_dir"/*

cd "$input_dir";

dataset_dim=374;
training_set_dim=$((374*2/3));
test_set_dim=$((dataset_dim-training_set_dim));

echo $dataset_dim;
echo $training_set_dim;
echo $test_set_dim;

tmp=$(ls *.pcd | sort -R);

echo "training set";
echo $tmp | tr " " "\n" | head -$training_set_dim | while read file; do
    no_ext_name="${file%.*}";
    #echo $no_ext_name;

    replace_with="image";
    png_file="${no_ext_name/point_cloud/$replace_with}"
    png_file=$png_file.png;
    
    cp $file "$output_training_dir";
    cp $png_file "$output_training_dir";
done

echo "test set";
echo $tmp | tr " " "\n" | tail -$test_set_dim | while read file; do
    no_ext_name="${file%.*}";
    #echo $no_ext_name;

    replace_with="image";
    png_file="${no_ext_name/point_cloud/$replace_with}"
    png_file=$png_file.png;
    
    cp $file "$output_test_dir";
    cp $png_file "$output_test_dir";
done
