from collections import defaultdict
import itertools

def pascal_test(rect1, rect2):
   # top left corner
	x11 = int(rect1[1])
	y11 = int(rect1[2])
	x12 = int(x11) + int(rect1[3])
	y12 = int(y11) + int(rect1[4])

	# bottom right corner
	x21 = int(rect2[1])
	y21 = int(rect2[2])
	x22 = int(x21) + int(rect2[3])
	y22 = int(y21) + int(rect2[4])

	x_overlap = max(0, min(x12,x22) - max(x11,x21))
	y_overlap = max(0, min(y12,y22) - max(y11,y21));
	overlapping_area = x_overlap * y_overlap
	area_sum = int(value[0][3]) * int(value[0][4]) + int(value[1][3]) * int(value[1][4])

	pascal_rate = overlapping_area / float(area_sum - overlapping_area)

	return pascal_rate

path = '/media/fall_detection_lab1/lab1_rate1_5'
groundtruth = path + '/ground_truth.txt'
results = path + '/results_02_02_16.txt' #'/fake_results.txt'
fp_check = path + '/fp_check.txt'
fn_check = path + '/fn_check.txt'
thres = 0.1
check_thres = 0.25

fp_check_file = open(fp_check, 'w')
fn_check_file = open(fn_check, 'w')

index = defaultdict(list)

# read gt
number_of_frames = 0;
for line in open(groundtruth, 'r'):
    number_of_frames+=1
    line_terms = line.split()
    if (len(line_terms) > 1):
    	index[line_terms[0]].append(line_terms)

gt_detections = len(index)
print("number_of_frames " + repr(number_of_frames))
print("Detections in GT " + repr(gt_detections))

# read our results
# write fp check file
for line in open(results, 'r'):
    line_terms = line.split()
    if (len(line_terms) > 1): # if detection by us
    	if (len(index[line_terms[0]]) == 0): # fp: detected but not in GT
    		fp_check_file.write(" ".join(line_terms) + "\n")
    	else:
    		index[line_terms[0]].append(line_terms)
false_positives = len(index) - gt_detections;

# write fn check file
for key, value in index.iteritems():
	if len(value) == 1: # fn: only in GT
		fn_check_file.write(" ".join(value[0]) + "\n")

# check tp
# update fp
num_detection = 0;
for key, value in index.iteritems():
	if len(value) == 2:
		
		res = pascal_test(value[0], value[1])

		if (res > thres):
			num_detection+=1
			print("pos " + repr(res) + " " + repr(value))
		else:
			false_positives+=1
			print("neg " + repr(res) + " " + repr(value))

		if (res < check_thres):
			fp_check_file.write(" ".join(value[0]) + " ")
			fp_check_file.write(" ".join(value[1]) + " " + repr(res) + "\n")

false_negatives = gt_detections - num_detection
true_negatives = number_of_frames - num_detection - false_positives - false_negatives;

print("Assumption of one person per frame")

print("TP " + repr(num_detection))
print("TN " + repr(true_negatives))
print("FP " + repr(false_positives))
print("FN " + repr(false_negatives))

accuracy = (num_detection + true_negatives) / float(number_of_frames)
precision = num_detection / float(num_detection + false_positives)
recall = num_detection / float(num_detection + false_negatives)

print("Accuracy " + repr(accuracy))
print("Precision " + repr(precision))
print("Recall " + repr(recall))
