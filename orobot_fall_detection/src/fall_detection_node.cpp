#include <iostream>
#include <stdlib.h>
#include <time.h>

#include <ros/package.h>
#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>    

#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>

#include <boost/foreach.hpp>
#include "boost/date_time/posix_time/ptime.hpp"

#include "orobot_fall_detection/BBoxPrinter.h"
#include "orobot_fall_detection/CloudFilter.h"
#include <orobot_fall_detection/dynamic_fall_detection_paramsConfig.h>
#include "orobot_fall_detection/ChangeStateOrobotFallDetection.h"
#include "orobot_state_manager/FallDetectionCommunicationService.h"

namespace fall_detection {

class FallDetector {

private:
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_;
    cv::Mat frame_;

    ros::NodeHandle nh_;

    // State
    bool pc_callback_;
    bool state_;

    // Topics and services
    ros::Subscriber points_sub_;
    ros::Subscriber image_sub_;
    ros::ServiceServer change_state_service_;

    // Dynamic reconfigure
    orobot_fall_detection::dynamic_fall_detection_paramsConfig params_;
    dynamic_reconfigure::Server<orobot_fall_detection::dynamic_fall_detection_paramsConfig> server_;
    dynamic_reconfigure::Server<orobot_fall_detection::dynamic_fall_detection_paramsConfig>::CallbackType f_;

    // rosservice call /orobot_fall_detection/change_fall_detection_state "{}"
    bool change_state(orobot_fall_detection::ChangeStateOrobotFallDetection::Request  &req,
        orobot_fall_detection::ChangeStateOrobotFallDetection::Response &res)
    {
      state_ = !state_;

      if (state_)
        ROS_INFO("Detection of fallen people is now active");
      else
        ROS_INFO("Detection of fallen people is now inactive");

      return true;
    }

    void printDynamicParamCb(orobot_fall_detection::dynamic_fall_detection_paramsConfig &config, uint32_t level)
    {
      params_ = config;
      ROS_INFO("Reconfigure Request: %s %s %s %f %d %f %f %f %f %d %d %d %f %f %f",
                config.enable_visualization?"True":"False",
                config.enable_audible_alarm?"True":"False",
                config.enable_communication?"True":"False",
                config.leafSize,
                config.meanK,
                config.stddevMulThresh,
                config.k2_height,
                config.ground_tolerance,
                config.cluster_tolerance,
                config.starting_min_cluster,
                config.max_cluster,
                config.min_cluster,
                config.robot_height,
                config.max_height,
                config.min_height);
    }

    void pointCloudCb(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr & msg)
    {
        *cloud_ = *msg;
        pc_callback_ = true;
    }

    void imageCb(const sensor_msgs::ImageConstPtr & img_msg)
    {
        cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(img_msg, img_msg->encoding);
        frame_ = cv_ptr->image;
    }

    /* TODO: use templates */
    void publish_cloud(const ros::Publisher & pub,
        pcl::PointCloud<pcl::PointXYZ>::Ptr msg)
    {
        msg->header.frame_id = "kinect2_head_ir_optical_frame";
        msg->header.stamp = ros::Time::now().toNSec();
        pub.publish (msg);
    }

    void publish_cloud(const ros::Publisher & pub,
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr msg)
    {
        msg->header.frame_id = "kinect2_head_ir_optical_frame";
        msg->header.stamp = ros::Time::now().toNSec();
        pub.publish (msg);
    }

    int random_intensity()
    {
        return rand() % 256;
    }

    float
    check_with_classifier(const CvRTrees & rtrees,
            const std::vector<float> & features)
    {
        cv::Mat sample = (cv::Mat_<float>(1,4)
            << 0.0f, features[0], features[1], features[2]);
        std::copy(features.begin(), features.end(),
            std::ostream_iterator<float>(std::cout, " "));

        return rtrees.predict_prob(sample);
    }

    bool
    send_alert(const float x, const float y)
    {
        ros::ServiceClient client = nh_.serviceClient<orobot_state_manager::FallDetectionCommunicationService>
                ("orobot_state_manager/fall_detection_communication_service");

        orobot_state_manager::FallDetectionCommunicationService srv;
        srv.request.x = x;
        srv.request.y = y;

        if (client.call(srv))
        {
            ROS_INFO("Sent");
            return true;
        }
        else
        {
            ROS_ERROR("Failed to call service orobot_state_manager/fall_detection_communication_service");
            return false;
        }
    }

public:

    int
    detect_fallen_person ()
    {
        ros::Publisher cloud_pub;
        ros::Publisher filtered_cloud_pub;
        ros::Publisher ground_cloud_pub;
        ros::Publisher no_ground_cloud_pub;
        ros::Publisher clusterized_cloud_pub;
        ros::Publisher bbox3D_cloud_pub;
        ros::Publisher fallen_people_image_pub;

        //ros time for not playing too much audio files
        ros::Time timer = ros::Time::now();
        double seconds_to_wait_until_play_again = 4.0;

        if (params_.enable_visualization)
        {
            cloud_pub = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >
                    ("/fall_detection/cloud", 1);
            filtered_cloud_pub = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >
                    ("/fall_detection/filtered_cloud", 1);
            ground_cloud_pub = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >
                    ("/fall_detection/ground_cloud", 1);
            no_ground_cloud_pub = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >
                    ("/fall_detection/no_ground_cloud", 1);
            clusterized_cloud_pub = nh_.advertise< pcl::PointCloud<pcl::PointXYZRGB> >
                    ("/fall_detection/clusterized_cloud", 1);
            bbox3D_cloud_pub = nh_.advertise< pcl::PointCloud<pcl::PointXYZ> >
                            ("/fall_detection/bbox3D_cloud", 1);
            fallen_people_image_pub = nh_.advertise< sensor_msgs::Image >
                ("/fall_detection/fallen_people_image", 1);
        }

        CvRTrees rtrees;
        rtrees.load(std::string(ros::package::getPath("orobot_fall_detection") +
                                "/cfg/rtrees_classifier").c_str());

        srand (time(NULL));

        BBoxPrinter bbp;
        CloudFilter cf;

        ros::Rate loop_rate(30.0);
        while(ros::ok())
        {
            if (!state_)
            {
                std::cout << "Fall detection is inactive" << std::endl;
                ros::spinOnce();
                loop_rate.sleep();
                continue;
            }

            boost::posix_time::ptime process_start, process_stop;
            process_start = boost::posix_time::microsec_clock::universal_time();

            if (!pc_callback_)
            {
                std::cout << "Waiting for a frame" << std::endl;
                ros::spinOnce();
                loop_rate.sleep();
                continue;
            }
            pc_callback_ = false;

            bbp.set_frame(frame_);

            /*
            ROS_INFO("Reconfigure Request: %s %s %f %d %f %f %f %f %d %d %d %f %f %f",
                params_.enable_visualization?"True":"False",
                params_.enable_audible_alarm?"True":"False",
                params_.leafSize,
                params_.meanK,
                params_.stddevMulThresh,
                params_.k2_height,
                params_.ground_tolerance,
                params_.cluster_tolerance,
                params_.starting_min_cluster,
                params_.max_cluster,
                params_.min_cluster,
                params_.robot_height,
                params_.max_height,
                params_.min_height);
            */

            int fallen_people = 0;

            // Extract a region of interest for efficiency and accuracy
            pcl::PointCloud<pcl::PointXYZ>::Ptr roi_cloud (new pcl::PointCloud<pcl::PointXYZ>);
            cf.extract_roi(cloud_, roi_cloud);

            // Pre-filtering
            pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud (new pcl::PointCloud<pcl::PointXYZ>);
            cf.filter_cloud(roi_cloud, params_.leafSize, params_.meanK, params_.stddevMulThresh, filtered_cloud);
            //std::cout << "Cloud before filtering: " << std::endl;
            //std::cout << *cloud << std::endl;
            //std::cout << "Cloud after filtering: " << std::endl;
            //std::cout << *filtered_cloud << std::endl;

            // Removing ground
            // TODO: extract a region of interest for efficiency
            pcl::PointCloud<pcl::PointXYZ>::Ptr ground_cloud (new pcl::PointCloud<pcl::PointXYZ>);
            pcl::PointCloud<pcl::PointXYZ>::Ptr no_ground_cloud (new pcl::PointCloud<pcl::PointXYZ>);
            cf.remove_ground(filtered_cloud,
                          params_.k2_height,
                          params_.ground_tolerance,
                          ground_cloud,
                          no_ground_cloud);

            // Find candidates
            std::vector<pcl::PointIndices> cluster_indices;
            cf.find_candidates(no_ground_cloud, params_.cluster_tolerance, params_.starting_min_cluster, params_.max_cluster, cluster_indices);
            ROS_INFO_STREAM("Clusters found! " << cluster_indices.size());

            // TODO: inefficient loop
            pcl::PointCloud<pcl::PointXYZ>::Ptr bbox3D_cloud (new pcl::PointCloud<pcl::PointXYZ>);
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr clusterized_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
            int id = 0;
            for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin ();
                it != cluster_indices.end ();
                ++it)
            {
                int random_r = random_intensity();
                int random_g = random_intensity();
                int random_b = random_intensity();
                if (params_.enable_visualization)
                {
                    random_r = random_intensity();
                    random_g = random_intensity();
                    random_b = random_intensity();
                }

                pcl::PointCloud<pcl::PointXYZ>::Ptr cluster_cloud (new pcl::PointCloud<pcl::PointXYZ>);
                for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
                {
                    cluster_cloud->points.push_back (no_ground_cloud->points[*pit]);

                    if (params_.enable_visualization)
                    {
                        pcl::PointXYZRGB pt;
                        pt.x = no_ground_cloud->points[*pit].x;
                        pt.y = no_ground_cloud->points[*pit].y;
                        pt.z = no_ground_cloud->points[*pit].z;
                        pt.r = random_r;
                        pt.g = random_g;
                        pt.b = random_b;
                        clusterized_cloud->points.push_back (pt);
                    }
                }
                cluster_cloud->width = cluster_cloud->points.size ();
                cluster_cloud->height = 1;
                cluster_cloud->is_dense = true;

                pcl::PointXYZ min;
                pcl::PointXYZ max;
                pcl::getMinMax3D (*cluster_cloud, min, max); // TODO: at least one... more efficient

                pcl::PointXYZ best_min_p;
                pcl::PointXYZ best_max_p;
                std::vector<float> sorted_dims;
                cf.get_cluster_sorted_dims(cluster_cloud, sorted_dims, best_min_p, best_max_p);

                // For checking 3D boxes
                bbox3D_cloud->points.push_back(best_min_p);
                bbox3D_cloud->points.push_back(best_max_p);
                publish_cloud(bbox3D_cloud_pub, bbox3D_cloud);

                std::stringstream label;
                label << "S:";
                label << cluster_cloud->points.size();

                cv::Point min_pt;
                cv::Point max_pt;

                // min.y < robot_height = higher cluster point is higher than turtlebot platform
                // min.y < 0 = it is higher than k2
                if(min.y < params_.max_height
                    //|| max.y < params_.min_height // no suspended/flying blobs
                    || cluster_cloud->points.size() < params_.min_cluster)
                {
                    ROS_INFO_STREAM("Loose geometric check failed!");

                    if (params_.enable_visualization)
                    {
                        bbp.basicPrint2DBoundingBox(cluster_cloud, min_pt, max_pt, false, label.str());
                        sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", bbp.frame_).toImageMsg();
                        fallen_people_image_pub.publish(msg);
                        ros::spinOnce();
                        loop_rate.sleep();
                    }

                    continue;
                }

                float confidence = check_with_classifier(rtrees, sorted_dims);

                std::cout << "confidence " << confidence << std::endl;
                label << " C:" << confidence;
                if (confidence < 0.5)
                {
                    ROS_INFO_STREAM("Classifier check failed!");

                    if (params_.enable_visualization)
                    {
                        bbp.basicPrint2DBoundingBox(cluster_cloud, min_pt, max_pt, false, label.str());
                        sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", bbp.frame_).toImageMsg();
                        fallen_people_image_pub.publish(msg);
                        ros::spinOnce();
                        loop_rate.sleep();
                    }

                    continue;
                }

                ROS_INFO_STREAM("All checks passed!");

                if (params_.enable_visualization)
                {
                    bbp.basicPrint2DBoundingBox(cluster_cloud, min_pt, max_pt, true, label.str());
                    sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", bbp.frame_).toImageMsg();
                    fallen_people_image_pub.publish(msg);
                    ros::spinOnce();
                    loop_rate.sleep();

                }

                if (params_.enable_audible_alarm)
                {
                    if(fabs(ros::Time::now().sec - timer.sec) > seconds_to_wait_until_play_again)
                    {
                        timer = ros::Time::now();
                        std::stringstream ss;
                        ss << "mplayer "<<
                              ros::package::getPath("orobot_fall_detection") <<
                              "/cfg/uomoaterra.ogg &";
                        int ret = system(ss.str().c_str());
                    }

                }

                if (params_.enable_communication)
                {
                    bool ret = send_alert(min.x, min.y); // temporary, use centroid or something else
                }

                fallen_people++;
            }

            if (params_.enable_visualization)
            {
                publish_cloud(cloud_pub, cloud_);
                publish_cloud(filtered_cloud_pub, filtered_cloud);
                publish_cloud(ground_cloud_pub, ground_cloud);
                publish_cloud(no_ground_cloud_pub, no_ground_cloud);
                publish_cloud(clusterized_cloud_pub, clusterized_cloud);
            }

            std::cout << "NUMBER OF FALLEN PEOPLE: " << fallen_people << std::endl;

            ros::spinOnce();
            loop_rate.sleep();

            process_stop = boost::posix_time::microsec_clock::universal_time();
            std::cout << "FPS = " << (1e6f) / (process_stop - process_start).total_microseconds()  << std::endl;
        }

        return 0;
    }

    FallDetector() :
        nh_(),
        pc_callback_(false),
        state_(true)
    {
        cloud_.reset(new pcl::PointCloud<pcl::PointXYZ>);

        points_sub_ = nh_.subscribe
            ("/kinect2_head/depth_ir/points", 1, &FallDetector::pointCloudCb, this);
        image_sub_ = nh_.subscribe
            ("/kinect2_head/rgb/image", 1, &FallDetector::imageCb, this);
            // if dataset /kinect2_head/rgb_rect/image_repub

        f_ = boost::bind(&FallDetector::printDynamicParamCb, this, _1, _2);
        server_.setCallback(f_);

        change_state_service_ = nh_.advertiseService("orobot_fall_detection/change_fall_detection_state", &FallDetector::change_state, this);
    }

};

}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "fall_detection_node");

    fall_detection::FallDetector fd;
    int ret = fd.detect_fallen_person();

    ros::spin();
    return 0;
}
