#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <time.h>

#include <ros/package.h>
#include <ros/ros.h>

#include <dynamic_reconfigure/server.h>
#include <orobot_fall_detection/dynamic_fall_detection_paramsConfig.h>

// read image
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <opencv2/ml/ml.hpp>    

// read cloud
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>

#include "boost/date_time/posix_time/ptime.hpp"
#include <boost/filesystem.hpp>

#include <pcl/common/common.h>
#include <pcl/common/pca.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/segmentation/extract_clusters.h>

// intrinsics
const float sizeX = 1920;
const float sizeY = 1080;
const float focal_x = 1081.37;
const float focal_y = 1081.37;
const float cx = 959.5;
const float cy = 539.5;

// other global variables
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
cv::Mat frame;
bool pc_callback = false;
orobot_fall_detection::dynamic_fall_detection_paramsConfig params;

int
getPointUVCoordinates(const pcl::PointXYZ & pt, 
    cv::Point & UV_coordinates)
{
  if (pt.z > 0)
  {
    // project point on camera's image plane
    UV_coordinates.x = round((focal_x * (pt.x / pt.z) + cx));
    UV_coordinates.y = round((focal_y * (pt.y / pt.z) + cy));

    // point is visible!
    float normalized_uv_x = UV_coordinates.x / sizeX;
    float normalized_uv_y = UV_coordinates.y / sizeY;

    if (normalized_uv_x >= 0.0 && normalized_uv_x <= 1.0
        && normalized_uv_y >= 0.0 && normalized_uv_y <= 1.0)
      return 0; // point was visible by the camera

    return 1;
  }

  // point is NOT visible by the camera
  return 2;
}

/* The translation from kinect2_head_ir_optical_frame
to kinect2_head_rgb_optical_frame is neglected */
void
print2DBoundingBox(const pcl::PointXYZ & old_ref_min_p_cluster, 
    const pcl::PointXYZ & old_ref_max_p_cluster,
    const bool found,
    const std::string & label = "")
{
    cv::Point min_pt;
    int ret1 = getPointUVCoordinates(old_ref_min_p_cluster, min_pt);
    cv::Point max_pt;
    int ret2 = getPointUVCoordinates(old_ref_max_p_cluster, max_pt);

    if (ret1 != 0 || ret2 != 0)
    {
        std::cerr << "Warning: out of image bounds box" << std::endl;
    }
    if (found)
    {
        cv::rectangle(frame, cv::Point(min_pt.x, min_pt.y),
                  cv::Point(max_pt.x, max_pt.y),
                  cv::Scalar(255,0,0), 20);
    }
    else
    {
        cv::rectangle(frame, cv::Point(min_pt.x, min_pt.y),
                  cv::Point(max_pt.x, max_pt.y),
                  cv::Scalar(0,0,255), 20);
    }
    cv::circle(frame, min_pt,
              20, cv::Scalar(255,0,0), 5);
    cv::circle(frame, max_pt,
              20, cv::Scalar(0,255,0), 5); // max "point" but lower point in 2D... why?
    cv::Point center_pt;
    center_pt.x = (min_pt.x + max_pt.x) / 2.0;
    center_pt.y = (min_pt.y + max_pt.y) / 2.0;
    cv::putText(frame, label, cv::Point(min_pt.x, min_pt.y),
                cv::FONT_HERSHEY_SIMPLEX, 2, CV_RGB(0,0,0), 5, 8);

    cv::namedWindow("frame", CV_WINDOW_KEEPRATIO);
    cv::imshow( "frame", frame );
    cv::waitKey(1);
}

void callback(orobot_fall_detection::dynamic_fall_detection_paramsConfig &config, uint32_t level) {
  params = config;
  ROS_INFO("Reconfigure Request: %s %s %f %d %f %f %f %f %d %d %d %f %f %f", 
            config.enable_visualization?"True":"False",
            config.enable_audible_alarm?"True":"False",
            config.leafSize, 
            config.meanK, 
            config.stddevMulThresh,
            config.k2_height,
            config.ground_tolerance,
            config.cluster_tolerance,
            config.starting_min_cluster,
            config.max_cluster,
            config.min_cluster,
            config.robot_height,
            config.max_height,
            config.min_height);
}

void pointCloudCb(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr & msg)
{
    *cloud = *msg;
    pc_callback = true;
}

void imageCb(const sensor_msgs::ImageConstPtr & img_msg)
{
    cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(img_msg, img_msg->encoding);
    frame = cv_ptr->image;
}

/* TODO: use templates */
void publish_cloud(const ros::Publisher & pub, 
    pcl::PointCloud<pcl::PointXYZ>::Ptr msg)
{
    msg->header.frame_id = "kinect2_head_ir_optical_frame";
    //msg->header.stamp = ros::Time::now().toNSec();
    pub.publish (msg);
}

void publish_cloud(const ros::Publisher & pub, 
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr msg)
{
    msg->header.frame_id = "kinect2_head_ir_optical_frame";
    //msg->header.stamp = ros::Time::now().toNSec();
    pub.publish (msg);
}

int random_intensity()
{
    return rand() % 256;
}

void
filter_cloud(const pcl::PointCloud<pcl::PointXYZ>::Ptr & input_cloud,
             const float leafSize,
             const int meanK,
             const float stddevMulThresh,
             pcl::PointCloud<pcl::PointXYZ>::Ptr & output_cloud)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::VoxelGrid<pcl::PointXYZ> vg;
    vg.setInputCloud (input_cloud);
    vg.setLeafSize (leafSize, leafSize, leafSize);
    vg.filter (*tmp_cloud);

    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
    sor.setInputCloud (tmp_cloud);
    sor.setMeanK (meanK);
    sor.setStddevMulThresh (stddevMulThresh);
    sor.filter (*output_cloud);
}

void
remove_ground(const pcl::PointCloud<pcl::PointXYZ>::Ptr & input_cloud,
              const float k2_height, // in meters
              const float ground_tolerance,
              pcl::PointCloud<pcl::PointXYZ>::Ptr & ground_cloud,
              pcl::PointCloud<pcl::PointXYZ>::Ptr & no_ground_cloud)
{
    float lower_limit = k2_height - ground_tolerance; // y is inverted!
    float upper_limit = k2_height + 10*ground_tolerance; // attention to points under the ground! k2 fault

    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud (input_cloud);
    pass.setFilterFieldName ("y");
    pass.setFilterLimits (lower_limit, upper_limit);
    pass.filter (*ground_cloud);

    pass.setFilterLimitsNegative (true);
    pass.filter (*no_ground_cloud);
}

void
find_candidates(const pcl::PointCloud<pcl::PointXYZ>::Ptr & input_cloud,
                const float cluster_tolerance,
                const int min_cluster,
                const int max_cluster,
                std::vector<pcl::PointIndices> & cluster_indices)
{
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud (input_cloud);

    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
    ec.setClusterTolerance (cluster_tolerance);
    ec.setMinClusterSize (min_cluster);
    ec.setMaxClusterSize (max_cluster);
    ec.setSearchMethod (tree);
    ec.setInputCloud (input_cloud);
    ec.extract (cluster_indices);
}

void computeOBB(const pcl::PointCloud<pcl::PointXYZ>::Ptr& point_cloud_ptr,
                Eigen::Vector3f& tfinal,
                Eigen::Quaternionf& qfinal,
                pcl::PointXYZ& min_pt,
                pcl::PointXYZ& max_pt,
                pcl::PointXYZ& old_ref_min_pt,
                pcl::PointXYZ& old_ref_max_pt)
{
    // compute principal direction
    Eigen::Vector4f centroid;
    pcl::compute3DCentroid(*point_cloud_ptr, centroid);
    Eigen::Matrix3f covariance;
    computeCovarianceMatrixNormalized(*point_cloud_ptr, centroid, covariance);
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
    Eigen::Matrix3f eigDx = eigen_solver.eigenvectors();
    eigDx.col(2) = eigDx.col(0).cross(eigDx.col(1));

    // move the points to that reference frame
    Eigen::Matrix4f p2w(Eigen::Matrix4f::Identity());
    p2w.block<3,3>(0,0) = eigDx.transpose();
    p2w.block<3,1>(0,3) = -1.f * (p2w.block<3,3>(0,0) * centroid.head<3>());
    pcl::PointCloud<pcl::PointXYZ> cPoints;
    pcl::transformPointCloud(*point_cloud_ptr, cPoints, p2w);

    pcl::getMinMax3D(cPoints, min_pt, max_pt);
    const Eigen::Vector3f mean_diag = 0.5f*(max_pt.getVector3fMap() + min_pt.getVector3fMap());

    // final transform
    qfinal = Eigen::Quaternionf(eigDx);
    tfinal = eigDx*mean_diag + centroid.head<3>();

    // draw the cloud and the box
    //      pcl::visualization::PCLVisualizer viewer;
    //      viewer.addPointCloud(point_cloud_ptr);
    //      viewer.addCube(tfinal, qfinal, max_pt.x - min_pt.x, max_pt.y - min_pt.y, max_pt.z - min_pt.z);
    //      viewer.spin();

    Eigen::Affine3f inv_p2w(p2w); // it would be better to retrieve them with the indexes
    inv_p2w = inv_p2w.inverse();
    old_ref_min_pt = pcl::transformPoint(min_pt, inv_p2w);
    old_ref_max_pt = pcl::transformPoint(max_pt, inv_p2w);
}

void
get_cluster_sorted_dims(const pcl::PointCloud<pcl::PointXYZ>::Ptr & cluster_cloud,
                        std::vector<float> & dims)
{
    dims.resize(3);

    pcl::PointXYZ min_p_cluster;
    pcl::PointXYZ max_p_cluster;
    pcl::PointXYZ old_ref_min_p_cluster;
    pcl::PointXYZ old_ref_max_p_cluster;

    Eigen::Vector3f trasl;
    Eigen::Quaternionf quat;
    computeOBB(cluster_cloud, trasl, quat,
               min_p_cluster, max_p_cluster,
               old_ref_min_p_cluster, old_ref_max_p_cluster);

    dims[0] = max_p_cluster.x - min_p_cluster.x;
    dims[1] = max_p_cluster.y - min_p_cluster.y;
    dims[2] = max_p_cluster.z - min_p_cluster.z;
    std::sort(dims.begin(), dims.end());
}

float
check_with_classifier(const CvRTrees & rtrees,
        const std::vector<float> & features)
{
    cv::Mat sample = (cv::Mat_<float>(1,4) 
        << 0.0f, features[0], features[1], features[2]);
    std::copy(features.begin(), features.end(), 
        std::ostream_iterator<float>(std::cout, " "));

    return rtrees.predict_prob(sample);
}

void
add_entry_to_training_set(std::ofstream & training_file,
    const std::vector<float> & sorted_dims,
    const int c)
{
    training_file << c << "," 
        << sorted_dims[0] << ","
        << sorted_dims[1] << ","
        << sorted_dims[2] << std::endl;
}

// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
void get_sorted_all(const boost::filesystem::path& root, const std::string& ext, std::vector<boost::filesystem::path>& ret)
{
    if(!boost::filesystem::exists(root) || !boost::filesystem::is_directory(root)) return;

    boost::filesystem::recursive_directory_iterator it(root);
    boost::filesystem::recursive_directory_iterator endit;

    while(it != endit)
    {
        if(boost::filesystem::is_regular_file(*it) && it->path().extension() == ext) 
            ret.push_back(it->path());
        ++it;
    }

    std::sort(ret.begin(), ret.end());
}

int main (int argc, char **argv)
{
    ros::init(argc, argv, "fall_detection_node");
    ros::NodeHandle nh;

    boost::filesystem::path input_folder = "/home/morris/dataset/dataset1/test_set/";
    std::vector<boost::filesystem::path> clouds;
    std::vector<boost::filesystem::path> images;
    get_sorted_all(input_folder, ".pcd", clouds);
    get_sorted_all(input_folder, ".png", images);

    std::cout << "dataset size " << clouds.size() << std::endl;

    /*
    ros::Subscriber points_sub = nh.subscribe
            ("/kinect2_head/depth_ir/points", 1, pointCloudCb);
    ros::Subscriber image_sub = nh.subscribe
            ("/kinect2_head/rgb/image", 1, imageCb);
    */

    dynamic_reconfigure::Server<orobot_fall_detection::dynamic_fall_detection_paramsConfig> server;
    dynamic_reconfigure::Server<orobot_fall_detection::dynamic_fall_detection_paramsConfig>::CallbackType f;

    f = boost::bind(&callback, _1, _2);
    server.setCallback(f);
    
    ros::Publisher cloud_pub;
    ros::Publisher filtered_cloud_pub;
    ros::Publisher ground_cloud_pub;
    ros::Publisher no_ground_cloud_pub;
    ros::Publisher clusterized_cloud_pub;
    if (params.enable_visualization)
    {
        cloud_pub = nh.advertise< pcl::PointCloud<pcl::PointXYZ> > 
                ("/fall_detection/cloud", 1);
        filtered_cloud_pub = nh.advertise< pcl::PointCloud<pcl::PointXYZ> > 
                ("/fall_detection/filtered_cloud", 1);
        ground_cloud_pub = nh.advertise< pcl::PointCloud<pcl::PointXYZ> > 
                ("/fall_detection/ground_cloud", 1);
        no_ground_cloud_pub = nh.advertise< pcl::PointCloud<pcl::PointXYZ> > 
                ("/fall_detection/no_ground_cloud", 1);
        clusterized_cloud_pub = nh.advertise< pcl::PointCloud<pcl::PointXYZRGB> > 
                ("/fall_detection/clusterized_cloud", 1);
    }

    std::ofstream training_file;
    std::stringstream tfs;
    tfs << ros::package::getPath("orobot_fall_detection")
       << "/cfg/training_file.csv";
    training_file.open(tfs.str().c_str());

    CvRTrees rtrees;
    rtrees.load(std::string(ros::package::getPath("orobot_fall_detection") +
                            "/cfg/rtrees_classifier").c_str());

    srand (time(NULL));

    int total_candidates = 0;
    int true_positives = 0;
    int false_positives = 0;
    int false_negatives = 0;
    ros::Rate loop_rate(30.0);
    for (unsigned int i = 0; i < clouds.size() && ros::ok(); i++)
    { 
        std::cout << "Current cloud " << clouds[i] << std::endl;
        std::cout << "Current image " << images[i] << std::endl;
        if (pcl::io::loadPCDFile<pcl::PointXYZ> (clouds[i].c_str(), *cloud) == -1) //* load the file
        {
            PCL_ERROR ("Couldn't read file pcd \n");
            return (-1);
        }
        frame = cv::imread(images[i].c_str(), CV_LOAD_IMAGE_COLOR);
        pc_callback = true;

        boost::posix_time::ptime process_start, process_stop;
        process_start = boost::posix_time::microsec_clock::universal_time();

        if (!pc_callback)
        {
            std::cout << "Waiting for a frame" << std::endl;
            ros::spinOnce();
            loop_rate.sleep();
            continue;
        }
        pc_callback = false;

        /*
        ROS_INFO("Reconfigure Request: %s %s %f %d %f %f %f %f %d %d %d %f %f %f", 
            params.enable_visualization?"True":"False",
            params.enable_audible_alarm?"True":"False",
            params.leafSize, 
            params.meanK, 
            params.stddevMulThresh,
            params.k2_height,
            params.ground_tolerance,
            params.cluster_tolerance,
            params.starting_min_cluster,
            params.max_cluster,
            params.min_cluster,
            params.robot_height,
            params.max_height,
            params.min_height);
        */

        int fallen_people = 0;

        // Pre-filtering
        pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        filter_cloud(cloud, params.leafSize, params.meanK, params.stddevMulThresh, filtered_cloud);
        //std::cout << "Cloud before filtering: " << std::endl;
        //std::cout << *cloud << std::endl;
        //std::cout << "Cloud after filtering: " << std::endl;
        //std::cout << *filtered_cloud << std::endl;
        
        // Removing ground
        // TODO: extract a region of interest for efficiency
        pcl::PointCloud<pcl::PointXYZ>::Ptr ground_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>::Ptr no_ground_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        remove_ground(filtered_cloud,
                      params.k2_height,
                      params.ground_tolerance,
                      ground_cloud,
                      no_ground_cloud);

        // Find candidates
        std::vector<pcl::PointIndices> cluster_indices;
        find_candidates(no_ground_cloud, params.cluster_tolerance, params.starting_min_cluster, params.max_cluster, cluster_indices);
        ROS_INFO_STREAM("Clusters found! " << cluster_indices.size());

        // TODO: inefficient loop
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr clusterized_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
        int id = 0;
        for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); 
            it != cluster_indices.end (); 
            ++it)
        {
            int random_r = random_intensity();
            int random_g = random_intensity();
            int random_b = random_intensity();
            if (params.enable_visualization)
            {
                random_r = random_intensity();
                random_g = random_intensity();
                random_b = random_intensity();
            }

            pcl::PointCloud<pcl::PointXYZ>::Ptr cluster_cloud (new pcl::PointCloud<pcl::PointXYZ>);
            for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
            {
                cluster_cloud->points.push_back (no_ground_cloud->points[*pit]);

                if (params.enable_visualization)
                {
                    pcl::PointXYZRGB pt;
                    pt.x = no_ground_cloud->points[*pit].x;
                    pt.y = no_ground_cloud->points[*pit].y;
                    pt.z = no_ground_cloud->points[*pit].z;
                    pt.r = random_r;
                    pt.g = random_g;
                    pt.b = random_b;
                    clusterized_cloud->points.push_back (pt);
                }
            }
            cluster_cloud->width = cluster_cloud->points.size ();
            cluster_cloud->height = 1;
            cluster_cloud->is_dense = true;

            pcl::PointXYZ min;
            pcl::PointXYZ max;
            pcl::getMinMax3D (*cluster_cloud, min, max); // TODO: at least one... more efficient

            std::stringstream label;
            label << "S:";
            label << cluster_cloud->points.size();

            // min.y < robot_height = higher cluster point is higher than turtlebot platform
            // min.y < 0 = it is higher than k2
            if(min.y < params.max_height 
                //|| max.y < params.min_height // no suspended/flying blobs
                || cluster_cloud->points.size() < params.min_cluster)
            {
                ROS_INFO_STREAM("Loose geometric check failed!");
                
                if (params.enable_visualization)
                {
                    print2DBoundingBox(min, max, false, label.str());
                }

                continue;
            }

            total_candidates++;

            std::vector<float> sorted_dims;
            get_cluster_sorted_dims(cluster_cloud, sorted_dims);
            float confidence = check_with_classifier(rtrees, sorted_dims);

            label << " C:" << confidence;
            if (confidence < 0.5)
            {
                ROS_INFO_STREAM("Classifier check failed!");
                
                if (params.enable_visualization)
                {
                    print2DBoundingBox(min, max, false, label.str());
                }
                
                continue;
            }

            ROS_INFO_STREAM("All checks passed!");
            
            if (params.enable_visualization)
            {
                print2DBoundingBox(min, max, true, label.str());
            }

            if (params.enable_audible_alarm)
            {
                std::stringstream ss;
                ss << "mplayer "<<
                      ros::package::getPath("orobot_fall_detection") <<
                      "/cfg/uomoaterra.ogg &";
                int ret = system(ss.str().c_str());
            }

            cv::namedWindow("frame", CV_WINDOW_KEEPRATIO);
            cv::imshow( "frame", frame );
            char c = cv::waitKey(0);

            if (c == 'k')
            {
                true_positives++;
            }
            else
            {
                false_positives++;
            }

            fallen_people++;            
        }

        if (params.enable_visualization)
        {
            publish_cloud(cloud_pub, cloud);
            publish_cloud(filtered_cloud_pub, filtered_cloud);
            publish_cloud(ground_cloud_pub, ground_cloud);
            publish_cloud(no_ground_cloud_pub, no_ground_cloud);
            publish_cloud(clusterized_cloud_pub, clusterized_cloud);
        }

        std::cout << "NUMBER OF FALLEN PEOPLE: " << fallen_people << std::endl;
        if (fallen_people < 1)
        {
            false_negatives++;
        }

        ros::spinOnce();
        loop_rate.sleep();

        process_stop = boost::posix_time::microsec_clock::universal_time();
        std::cout << "FPS = " << (1e6f) / (process_stop - process_start).total_microseconds()  << std::endl;
    }

    std::cout << "total candidates " << total_candidates << std::endl;
    std::cout << "true positives " << true_positives << std::endl; // max is 118
    std::cout << "false positives " << false_positives << std::endl;
    std::cout << "false negatives " << false_negatives << std::endl; // -7

    training_file.close();

    return 0;
}
