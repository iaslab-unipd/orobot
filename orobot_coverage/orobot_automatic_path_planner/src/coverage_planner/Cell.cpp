/*
 *  Copyright (c) 2015-, Marco Carraro <carraromarco89@gmail.com>,
 *                       Morris Antonello <morris.antonello@dei.unipd.it>
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *     3. Neither the name of the copyright holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "coverage_planner/Cell.h"

namespace orobot
{

void Cell::initializeCell(const cv::Point2i& p1, const cv::Point2i& p2, const bool is_empty, const bool is_reachable,
                          const uint& cell_dim)
{
    p1_ = p1;
    p2_ = p2;
    center_ = cv::Point2i(std::ceil((p1_.x + p2_.x) / 2.0f), std::ceil((p1_.y + p2_.y) / 2.0f));
    is_empty_ = is_empty;
    is_reachable_ = is_reachable;
    cell_dim_ = cell_dim;
    is_valid_ = true;
}

std::ostream& operator<<(std::ostream& ss, const Cell& cell)
{
    if(!cell.isValid())
    {
        ss << "cell invalid";
        return ss;
    }
    ss << "Cell P1=" << cell.getP1() << " P2=" << cell.getP2() << " cell_dim=" << cell.getCellDim();
    return ss;
}

}
