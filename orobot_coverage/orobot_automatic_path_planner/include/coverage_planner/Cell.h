/*
 *  Copyright (c) 2015-, Marco Carraro <carraromarco89@gmail.com>,
 *                       Morris Antonello <morris.antonello@dei.unipd.it>
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     1. Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *     3. Neither the name of the copyright holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef COVERAGE_CELL___H
#define COVERAGE_CELL___H


#include <opencv2/opencv.hpp>

namespace orobot
{

class CellNotValidException: public std::exception
{
  virtual const char* what() const throw()
  {
    return "Cell not initialized correctly! Have you just called the default constructor withouth initialize it?";
  }
} not_valid_exc;

class Cell
{
private:
    cv::Point2i p1_;
    cv::Point2i p2_;
    cv::Point2i center_;
    bool is_empty_;
    bool is_reachable_;
    uint cell_dim_;
    bool is_valid_;
public:
    inline Cell()
    {
        is_valid_ = false;
    }
    inline Cell(const Cell& cell)
    {
        this->initializeCell(cell.p1_,cell.p2_,cell.is_empty_,cell.is_reachable_,cell.cell_dim_);
    }
    inline Cell(const cv::Point2i& p1, const bool is_empty, const uint cell_dim)
    {
        this->initializeCell(p1,cv::Point2i(p1.x + cell_dim, p1.y + cell_dim),is_empty,false,cell_dim);
    }
    void initializeCell(const cv::Point2i& p1, const cv::Point2i& p2, const bool is_empty,
                               const bool is_reachable, const uint& cell_dim);
    inline void setIsEmpty(const bool is_empty)
    {
        is_empty_ = is_empty;
    }
    inline void setIsReachable(const bool is_reachable)
    {
        is_reachable_ = is_reachable;
    }
    inline const bool isEmpty() const
    {
        return is_empty_;
    }
    inline const cv::Point2i& getP1() const
    {
        if(!is_valid_)
            throw not_valid_exc;
        return p1_;
    }
    inline const cv::Point2i& getP2() const
    {
        if(!is_valid_)
            throw not_valid_exc;
        return p2_;
    }
    inline const cv::Point2i& getCenter() const
    {
        if(!is_valid_)
            throw not_valid_exc;
        return center_;
    }
    inline const bool isReachable() const
    {
        return is_reachable_;
    }
    inline const uint getCellDim() const
    {
        return cell_dim_;
    }
    inline const bool isValid() const
    {
        return is_valid_;
    }
    friend std::ostream& operator<<(std::ostream& ss, const Cell& cell);
};

}
#endif //COVERAGE_CELL___H
