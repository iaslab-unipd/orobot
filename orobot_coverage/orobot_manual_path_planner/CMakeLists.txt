cmake_minimum_required(VERSION 2.8.3)
project(orobot_manual_path_planner)

find_package(catkin REQUIRED COMPONENTS 
  costmap_2d
  dynamic_reconfigure
  geometry_msgs
  roscpp
  std_msgs
  visualization_msgs
  message_generation
  orobot_automatic_path_planner)

find_package(Boost REQUIRED)

include_directories(
  ${catkin_INCLUDE_DIRS}
)

catkin_package(
  INCLUDE_DIRS
  LIBRARIES
  CATKIN_DEPENDS 
  roscpp
  geometry_msgs
)

add_executable(manual_planner src/manual_planner.cpp)
target_link_libraries(manual_planner ${catkin_LIBRARIES})

add_executable(test_manual_path test/test_manual_path.cpp)
target_link_libraries(test_manual_path ${catkin_LIBRARIES})
