#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <time.h>

#include <ros/ros.h>

// read image
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "boost/date_time/posix_time/ptime.hpp"
#include <boost/filesystem.hpp>

#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/io/pcd_io.h>

#include <image_geometry/pinhole_camera_model.h>
#include <depth_image_proc/depth_conversions.h>

int number_of_digits = 5;
int downsampling_factor = 5;

void catch_cloud(const sensor_msgs::PointCloud2::ConstPtr& cloud)
{
    std::stringstream ss;
    ss << "/home/morris/dataset/fall_detection_dataset/test/cloud/cloud_"
    //ss << "/home/morris/dataset/fall_detection_dataset/lab1_rate1_5/cloud/cloud_"
       << std::setfill('0') << std::setw(number_of_digits) << cloud->header.seq * downsampling_factor << ".pcd";
    pcl::io::savePCDFile(ss.str(), *cloud);
}

// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
void get_sorted_all(const boost::filesystem::path& root, const std::string& ext, std::vector<boost::filesystem::path>& ret)
{
    if(!boost::filesystem::exists(root) || !boost::filesystem::is_directory(root)) return;

    boost::filesystem::recursive_directory_iterator it(root);
    boost::filesystem::recursive_directory_iterator endit;

    while(it != endit)
    {
        if(boost::filesystem::is_regular_file(*it) && it->path().extension() == ext) 
            ret.push_back(it->path());
        ++it;
    }

    std::sort(ret.begin(), ret.end());
}

int main (int argc, char **argv)
{
    ros::init(argc, argv, "publish_depth_images");
    ros::NodeHandle nh;

    boost::filesystem::path depth_folder = "/data/fall_detection_cad1/cad1_rate1_5/raw_depth/";
    std::string cloud_folder = "/data/fall_detection_cad1/cad1_rate1_5/cloud/";

    std::vector<boost::filesystem::path> depths;
    get_sorted_all(depth_folder, ".png", depths);

    std::cout << "depth size " << depths.size() << std::endl;
    
    ros::Publisher depth_pub;
    depth_pub = nh.advertise<cv_bridge::CvImage>("/dataset/image_rect", 0);

    ros::Publisher camera_pub;
    camera_pub = nh.advertise<sensor_msgs::CameraInfo>("/dataset/camera_info", 0);

    ros::Subscriber cloud_sub;
    //cloud_sub = nh.subscribe<sensor_msgs::PointCloud2>("/kinect2_head/depth_ir/points", 0, catch_cloud);

    ros::Rate loop_rate(1.0);

    /*
    int init_trip_length = 25;
    for (unsigned int i = 0; i < init_trip_length && ros::ok(); i++)
    {
        ros::spinOnce();
        loop_rate.sleep();
    }
    */

    for (unsigned int i = 0; i < depths.size() && ros::ok(); i++)
    {
        /*
        int safe_trip_length = 1;
        for (unsigned int j = 0; j < safe_trip_length  && ros::ok(); j++)
        {
            ros::spinOnce();
            loop_rate.sleep();
        }
        */

        std::cout << "Current depth " << depths[i] << std::endl;

        cv::Mat depth_frame = cv::imread(depths[i].c_str(), CV_LOAD_IMAGE_ANYDEPTH); // mono16

        /*
        double min;
        double max;
        cv::minMaxIdx(depth_frame, &min, &max);
        cv::Mat adjMap;
        cv::convertScaleAbs(depth_frame, adjMap, 255 / max);

        cv::imshow("adjMap", adjMap);
        cv::waitKey(1);
        */

        sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), sensor_msgs::image_encodings::TYPE_16UC1, depth_frame).toImageMsg(); // mono16, enc::TYPE_16UC1
        msg->header.seq = i * downsampling_factor;
        msg->header.stamp = ros::Time::now();
        msg->header.frame_id = "/kinect2_head_ir_optical_frame";

        sensor_msgs::CameraInfo info_msg;
        info_msg.header = msg->header;
        //info_msg.header.seq = i;
        //info_msg.header.stamp = ros::Time::now();
        //info_msg.header.frame_id = "/kinect2_head_ir_optical_frame";
        info_msg.height = 424;
        info_msg.width = 512;
        info_msg.distortion_model = "plumb_bob";
        info_msg.D.resize(5, 0);
        info_msg.K = {363.939381, 0.0, 255.041927, 0.0, 358.529588, 211.314985, 0.0, 0.0, 1.0};
        info_msg.R = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
        info_msg.P = {363.939381, 0.0, 255.041927, 0.0, 0.0, 358.529588, 211.314985, 0.0, 0.0, 0.0, 1.0, 0.0};

        sensor_msgs::PointCloud2::Ptr cloud_msg(new sensor_msgs::PointCloud2);
        cloud_msg->header = msg->header;
        cloud_msg->height = msg->height;
        cloud_msg->width  = msg->width;
        cloud_msg->is_dense = false;
        cloud_msg->is_bigendian = false;
        sensor_msgs::PointCloud2Modifier pcd_modifier(*cloud_msg);
        pcd_modifier.setPointCloud2FieldsByString(1, "xyz");
        image_geometry::PinholeCameraModel model;
        bool ret = model.fromCameraInfo(info_msg);
        depth_image_proc::convert<uint16_t>(msg, cloud_msg, model);

        std::stringstream ss;
        ss << cloud_folder
           << "cloud_" << std::setfill('0') << std::setw(number_of_digits) << msg->header.seq << ".pcd";
        pcl::io::savePCDFile(ss.str(), *cloud_msg);

        depth_pub.publish(msg);
        camera_pub.publish(info_msg);

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
