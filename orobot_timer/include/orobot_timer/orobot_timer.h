#include <ros/ros.h>

namespace orobot
{

class OrobotTimer
{
private:
    bool activation_flag_;
    double callback_rate_;
    std::string name_;
    ros::Time date_;
    ros::Timer timer;
    ros::NodeHandle nh_;
    ros::NodeHandle private_nh_;

    void readParametersFromYAML();

public:
    inline OrobotTimer():
        activation_flag_(false),
        private_nh_("~")
    {
        readParametersFromYAML();
        timer = nh_.createTimer(ros::Duration(callback_rate_),
                                &OrobotTimer::callback,
                                this);
    }

    void callback(const ros::TimerEvent& timer_event);

    inline bool isActive() const
    {
        return activation_flag_;
    }

    inline void resetTimer()
    {
        activation_flag_ = false;
    }

};

}
