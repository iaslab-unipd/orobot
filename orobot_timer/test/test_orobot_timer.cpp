#include <orobot_timer/orobot_timer.h>
#include <sound_play/sound_play.h>
#include <orobot_speech_recognition/SpeechRecognition.h>
#include <std_srvs/Empty.h>
#include <ros/package.h>

bool check_timer = false;

bool callback(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
{
    ROS_INFO_STREAM("OROBOT_TIMER -> Request caught");
    check_timer = true;
    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_orobot_timer");

    orobot::OrobotTimer orobot_timer;

    ros::NodeHandle nh;
    orobot_speech_recognition::SpeechRecognition msg;

    ros::ServiceClient client = nh.serviceClient<orobot_speech_recognition::SpeechRecognition>("speech_recognition");
    ros::ServiceServer service = nh.advertiseService("check_timer", callback);

    ros::Rate rate(60);
    while(ros::ok())
    {
        if(orobot_timer.isActive() && check_timer)
        {
            check_timer = false;
            std::stringstream ss;
            ss << "mplayer "<<
                  ros::package::getPath("orobot_timer") <<
                  "/conf/medicina.ogg";
            int ret = system(ss.str().c_str());
            //sc.say("Hai preso la medicina?", "voice_lp_diphone");
            msg.request.junk = 0;
            if(client.call(msg))
            {
                while(msg.response.response == -1)
                {
                    std::stringstream ss4;
                    ss4 << "mplayer "<<
                          ros::package::getPath("orobot_timer") <<
                          "/conf/nonhocapito.ogg";
                    int ret = system(ss4.str().c_str());
                    ros::Duration(0.2).sleep();
                    client.call(msg);
                }
                switch(msg.response.response)
                {
                case 0:
                {
                    ROS_INFO_STREAM("Medicina non presa");
                    std::stringstream ss2;
                    ss2 << "mplayer "<<
                           ros::package::getPath("orobot_timer") <<
                           "/conf/male.ogg";
                    ret = system(ss2.str().c_str());
                    ros::Duration(30.0).sleep();
                    break;
                }
                case 1:
                {
                    ROS_INFO_STREAM("Medicina presa!");
                    std::stringstream ss3;
                    ss3 << "mplayer "<<
                           ros::package::getPath("orobot_timer") <<
                           "/conf/bravo.ogg";
                    ret = system(ss3.str().c_str());
                    orobot_timer.resetTimer();
                    break;
                }
                default:
                    ROS_ERROR_STREAM("Error, Google had not understood the audio or limit exceeded");
                }
            }
            else
            {
                ROS_ERROR_STREAM("Error! Cannot call the speech_recognition service!");
            }
        }
        else if (check_timer)
        {
            ROS_INFO_STREAM("OROBOT_TIMER -> It is not yet time to remind");
        }

        ros::spinOnce();
        rate.sleep();
    }

    ros::shutdown();
    return 0;
}
