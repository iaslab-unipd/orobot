/*
 * mainIdentification.cpp
 *
 *  Created on: Oct 17, 2012
 *      Author: Matteo Munaro
 */

// ROS includes:
#include <ros/ros.h>
//#include <ros/package.h>

//togliere quelli inutili
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/sac_model_plane.h>
/////////////////////////////////

#include <faceRecognition/forest.hpp>
#include <faceRecognition/multi_part_sample.hpp>
#include <faceRecognition/head_pose_sample.hpp>
#include <faceRecognition/face_utils.hpp>
#include <istream>
#include <opencv2/opencv.hpp>
#include <boost/progress.hpp>
#include <boost/thread.hpp>
#include <faceRecognition/face_forest.hpp>
#include <faceRecognition/feature_channel_factory.hpp>
#include <faceRecognition/timing.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <onlineIdentification/pointCloudUtils.h>
#include <onlineIdentification/readAndDisplayUtils.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <tf/transform_listener.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>

using namespace std;
using namespace cv;
namespace enc = sensor_msgs::image_encodings;

cv::Mat depthDistortionParameters(1, 5, CV_32F);
bool new_rgb, new_pointcloud, tf_ready;
cv_bridge::CvImagePtr rgb_cv;
cv_bridge::CvImagePtr depth_cv;
pcl::PointCloud<pcl::PointXYZ> depthPointcloud;
pcl::PointCloud<pcl::PointXYZ> skeletonCloud;
std::vector<Eigen::Quaternion<float> > skeletonQuality;

float x3Dn, y3Dn, z3Dn;
float x2Dn, y2Dn;
float x3Dh, y3Dh, z3Dh;
float x2Dh, y2Dh;

double coeff1, coeff2, coeff3, coeff4, coeff5, coeff6, coeff7, coeff8, coeff9;

// For ground selection:
std::vector<cv::Point> _points;
bool _finished = false;
bool estimate_ground = false;
Eigen::Matrix3f points;
Eigen::VectorXf plane_coeffs;

int kfd = 0;
struct termios cooked;
void quit(int sig) {

	tcsetattr(kfd, TCSANOW, &cooked);
	ros::shutdown();
	exit(0);
}

void selectFace(FaceForest& faceForestObject, int depth2rgbScaleFactor,
		bool faceDetectionFlag, cv::Mat& rgbImage, cv::Mat& imageToDisplay,
		cv::Mat& imageSelectionGray, std::vector<Face>& faces, bool debugFlag,
		float x2Dn, float y2Dn, float x2Dh, float y2Dh, int id_number,
		int counter, bool trainingFlag) {
	//shoulder center is equal to the neck point
	cv::Point shoulderCenter(x2Dn, y2Dn);
	//head is the head point
	cv::Point head(x2Dh, y2Dh);
	//head center is between head and neck joint, but nearer to head:
	cv::Point headCenter((shoulderCenter.x + 2 * head.x) / 3,
			(shoulderCenter.y + 2 * head.y) / 3);
	//evaluate head dimension (to decide the box dimensions for face recognition)
	complex<float> pointsDifference(shoulderCenter.x - head.x,
			shoulderCenter.y - head.y);
	float headDimension = std::abs(pointsDifference);
	// Select box around head && inside the rgb image:
	cv::Point topLeftCorner(std::max((float) 0, headCenter.x - headDimension),
			std::max((float) 0, headCenter.y - headDimension));
	cv::Point bottomRightCorner(
			std::min((float) rgbImage.cols - 1, headCenter.x + headDimension),
			std::min((float) rgbImage.rows - 1, headCenter.y + headDimension));
	int roiWidth = bottomRightCorner.x - topLeftCorner.x + 1;
	int roiHeight = bottomRightCorner.y - topLeftCorner.y + 1;
	//check if roi is inside the image
	if (topLeftCorner.x >= 0 && topLeftCorner.x < rgbImage.cols
			&& bottomRightCorner.x >= 0 && bottomRightCorner.x < rgbImage.cols
			&& topLeftCorner.y >= 0 && topLeftCorner.y < rgbImage.rows
			&& bottomRightCorner.y >= 0
			&& bottomRightCorner.y < rgbImage.rows) {

		cv::rectangle(imageToDisplay, topLeftCorner, bottomRightCorner, red, 3);

		//Select region of interest
		cv::Mat roi(rgbImage,
				cv::Rect(topLeftCorner.x, topLeftCorner.y, roiWidth,
						roiHeight));

		cv::Mat faceToSave = roi.clone();

		// Convert region of interest to grayscale
		cvtColor(roi, imageSelectionGray, CV_BGR2GRAY);
		// Face detection inside the image selection:
		if (faceDetectionFlag) {
			faceForestObject.analize_image(imageSelectionGray, faces);
		} else {
			Face face;
			faceForestObject.analize_face(imageSelectionGray,
					cv::Rect(0, 0, imageSelectionGray.cols,
							imageSelectionGray.rows), face);
			faces.push_back(face);
		}

		if (faces.size() > 1)		// If more than one face is detected
				{	// choose the biggest one
			int biggestWidth = 0;
			int biggestIndex = 0;
			for (uint i = 0; i < faces.size(); i++) {
				if (faces[i].bbox.width > biggestWidth) {
					biggestWidth = faces[i].bbox.width;
					biggestIndex = i;
				}
			}
			faces[0] = faces[biggestIndex];	// put the selected face at the first position
		}

		if (faces.size() > 0)		// If at least one face is detected:
				{
			// Display results:
			// display output face detector:
			cv::rectangle(imageToDisplay,
					cv::Point(faces[0].bbox.x + topLeftCorner.x,
							faces[0].bbox.y + topLeftCorner.y),
					cv::Point(
							faces[0].bbox.x + faces[0].bbox.width
									+ topLeftCorner.x,
							faces[0].bbox.y + faces[0].bbox.height
									+ topLeftCorner.y), blue, 3);

			if (debugFlag) {
				// display output facial feature detection:
				roi = cv::Mat(rgbImage,
						cv::Rect(topLeftCorner.x, topLeftCorner.y, roiWidth,
								roiHeight));
				FaceForest::show_results(roi, faces, "", 1, false);
			}

			if (counter == 0 && trainingFlag) {
				//save face for identification
				stringstream ss;
				ss << id_number;
				string str = ss.str();
				cv::imwrite("/home/andrea/descriptors/face_" + str + ".jpg",
						faceToSave);
			}
		}
	} else {
		ROS_ERROR("roi out of image");
	}
}

void computeSkeletonFeatures(
		//salvare in row invece che in colonna per evitare il reshape?
		pcl::PointCloud<pcl::PointXYZ>& originalSdkSkeletonJoints,
		cv::Mat& skeletonFeatures) {

	float sqrtGroundCoeffs = (plane_coeffs
			- Eigen::Vector4f(0.0f, 0.0f, 0.0f, plane_coeffs(3))).norm();

	// distance head-floor (3-floor)
	Eigen::Vector4f headPoint(originalSdkSkeletonJoints.points[3].x,
			originalSdkSkeletonJoints.points[3].y,
			originalSdkSkeletonJoints.points[3].z, 1);
	skeletonFeatures.at<float>(0, 0) = std::fabs(headPoint.dot(plane_coeffs));
	skeletonFeatures.at<float>(0, 0) /= sqrtGroundCoeffs;

	// distance neck-floor (2-floor)
	Eigen::Vector4f neckPoint(originalSdkSkeletonJoints.points[2].x,
			originalSdkSkeletonJoints.points[2].y,
			originalSdkSkeletonJoints.points[2].z, 1);
	skeletonFeatures.at<float>(1, 0) = std::fabs(neckPoint.dot(plane_coeffs));
	skeletonFeatures.at<float>(1, 0) /= sqrtGroundCoeffs;

	// distance neck-left shoulder (2-4)
	Eigen::Vector4f leftShoulderPoint(originalSdkSkeletonJoints.points[4].x,
			originalSdkSkeletonJoints.points[4].y,
			originalSdkSkeletonJoints.points[4].z, 1);
	skeletonFeatures.at<float>(2, 0) = (neckPoint - leftShoulderPoint).norm();

	// distance neck-right shoulder (2-8)
	Eigen::Vector4f rightShoulderPoint(originalSdkSkeletonJoints.points[8].x,
			originalSdkSkeletonJoints.points[8].y,
			originalSdkSkeletonJoints.points[8].z, 1);
	skeletonFeatures.at<float>(3, 0) = (neckPoint - rightShoulderPoint).norm();

	// distance torso center - right shoulder (1-8)
	Eigen::Vector4f torsoPoint(originalSdkSkeletonJoints.points[1].x,
			originalSdkSkeletonJoints.points[1].y,
			originalSdkSkeletonJoints.points[1].z, 1);
	skeletonFeatures.at<float>(4, 0) = (torsoPoint - rightShoulderPoint).norm();

	// right arm length (8-9 + 9-10)
	Eigen::Vector4f rightElbowPoint(originalSdkSkeletonJoints.points[9].x,
			originalSdkSkeletonJoints.points[9].y,
			originalSdkSkeletonJoints.points[9].z, 1);
	Eigen::Vector4f rightWristPoint(originalSdkSkeletonJoints.points[10].x,
			originalSdkSkeletonJoints.points[10].y,
			originalSdkSkeletonJoints.points[10].z, 1);
	skeletonFeatures.at<float>(5, 0) =
			(rightShoulderPoint - rightElbowPoint).norm()
					+ (rightWristPoint - rightElbowPoint).norm();

	// left arm length (4-5 + 5-6)
	Eigen::Vector4f leftElbowPoint(originalSdkSkeletonJoints.points[5].x,
			originalSdkSkeletonJoints.points[5].y,
			originalSdkSkeletonJoints.points[5].z, 1);
	Eigen::Vector4f leftWristPoint(originalSdkSkeletonJoints.points[6].x,
			originalSdkSkeletonJoints.points[6].y,
			originalSdkSkeletonJoints.points[6].z, 1);
	skeletonFeatures.at<float>(6, 0) =
			(leftShoulderPoint - leftElbowPoint).norm()
					+ (leftWristPoint - leftElbowPoint).norm();

	// right leg length (16-17)
	Eigen::Vector4f rightHipPoint(originalSdkSkeletonJoints.points[16].x,
			originalSdkSkeletonJoints.points[16].y,
			originalSdkSkeletonJoints.points[16].z, 1);
	Eigen::Vector4f rightKneePoint(originalSdkSkeletonJoints.points[17].x,
			originalSdkSkeletonJoints.points[17].y,
			originalSdkSkeletonJoints.points[17].z, 1);
	skeletonFeatures.at<float>(7, 0) = (rightHipPoint - rightKneePoint).norm();

	// left leg length (12-13)
	Eigen::Vector4f leftHipPoint(originalSdkSkeletonJoints.points[12].x,
			originalSdkSkeletonJoints.points[12].y,
			originalSdkSkeletonJoints.points[12].z, 1);
	Eigen::Vector4f leftKneePoint(originalSdkSkeletonJoints.points[13].x,
			originalSdkSkeletonJoints.points[13].y,
			originalSdkSkeletonJoints.points[13].z, 1);
	skeletonFeatures.at<float>(8, 0) = (leftHipPoint - leftKneePoint).norm();

	// torso length	(0-2)
	Eigen::Vector4f hipCenterPoint(originalSdkSkeletonJoints.points[0].x,
			originalSdkSkeletonJoints.points[0].y,
			originalSdkSkeletonJoints.points[0].z, 1);
	skeletonFeatures.at<float>(9, 0) = (hipCenterPoint - neckPoint).norm();

	// hips distance (16-0 + 12-0)
	skeletonFeatures.at<float>(10, 0) = (leftHipPoint - hipCenterPoint).norm()
			+ (rightHipPoint - hipCenterPoint).norm();

	// ratio torso/right leg
	skeletonFeatures.at<float>(11, 0) = skeletonFeatures.at<float>(9, 0)
			/ skeletonFeatures.at<float>(7, 0);

	// ratio torso/left leg
	skeletonFeatures.at<float>(12, 0) = skeletonFeatures.at<float>(9, 0)
			/ skeletonFeatures.at<float>(8, 0);
}

void computeFaceFiducialPointsQuality(Face& face,
		cv::Mat& fiducialPointsQuality) {
	// compute binary vector stating if the facial fiducial points are reliable or not
	fiducialPointsQuality.zeros(fiducialPointsQuality.rows,
			fiducialPointsQuality.cols, CV_32F);
	if ((face.ffd_cordinates[0].x < face.ffd_cordinates[1].x)
			&& (face.ffd_cordinates[0].y < face.ffd_cordinates[8].y))
		fiducialPointsQuality.at<float>(0, 0) = 1;
	else
		fiducialPointsQuality.at<float>(0, 0) = 0;
	if ((face.ffd_cordinates[1].x < face.ffd_cordinates[6].x)
			&& (face.ffd_cordinates[1].y < face.ffd_cordinates[8].y))
		fiducialPointsQuality.at<float>(1, 0) = 1;
	else
		fiducialPointsQuality.at<float>(1, 0) = 0;
	if ((face.ffd_cordinates[2].x < face.ffd_cordinates[5].x)
			&& (face.ffd_cordinates[2].y > face.ffd_cordinates[5].y))
		fiducialPointsQuality.at<float>(2, 0) = 1;
	else
		fiducialPointsQuality.at<float>(2, 0) = 0;
	if ((face.ffd_cordinates[3].x > face.ffd_cordinates[5].x)
			&& (face.ffd_cordinates[3].y > face.ffd_cordinates[9].y))
		fiducialPointsQuality.at<float>(3, 0) = 1;
	else
		fiducialPointsQuality.at<float>(3, 0) = 0;
	if ((face.ffd_cordinates[4].y > face.ffd_cordinates[5].y)
			&& (face.ffd_cordinates[4].y > face.ffd_cordinates[2].y))
		fiducialPointsQuality.at<float>(4, 0) = 1;
	else
		fiducialPointsQuality.at<float>(4, 0) = 0;
	if ((face.ffd_cordinates[5].y > face.ffd_cordinates[8].y)
			&& (face.ffd_cordinates[5].y < face.ffd_cordinates[2].y))
		fiducialPointsQuality.at<float>(5, 0) = 1;
	else
		fiducialPointsQuality.at<float>(5, 0) = 0;
	if ((face.ffd_cordinates[6].x < face.ffd_cordinates[7].x)
			&& (face.ffd_cordinates[6].y < face.ffd_cordinates[9].y))
		fiducialPointsQuality.at<float>(6, 0) = 1;
	else
		fiducialPointsQuality.at<float>(6, 0) = 0;
	if ((face.ffd_cordinates[7].x > face.ffd_cordinates[6].x)
			&& (face.ffd_cordinates[7].y < face.ffd_cordinates[9].y))
		fiducialPointsQuality.at<float>(7, 0) = 1;
	else
		fiducialPointsQuality.at<float>(7, 0) = 0;
	if ((face.ffd_cordinates[8].x < face.ffd_cordinates[9].x)
			&& (face.ffd_cordinates[8].y > face.ffd_cordinates[1].y))
		fiducialPointsQuality.at<float>(8, 0) = 1;
	else
		fiducialPointsQuality.at<float>(8, 0) = 0;
	if ((face.ffd_cordinates[9].x > face.ffd_cordinates[8].x)
			&& (face.ffd_cordinates[9].y < face.ffd_cordinates[5].y))
		fiducialPointsQuality.at<float>(9, 0) = 1;
	else
		fiducialPointsQuality.at<float>(9, 0) = 0;
}

bool mapJointsTo2D(float x3D, float y3D, float z3D, float &x2D, float &y2D,
		double c1, double c2, double c3, double c4, double c5, double c6,
		double c7, double c8, double c9) {
	Eigen::Matrix3f intrinsicMatrix;
	intrinsicMatrix << c1, c2, c3, c4, c5, c6, c7, c8, c9; //RGB intrinsic matrix

	// Compute projection matrix:
	Eigen::MatrixXf projectionMatrix;
	Eigen::MatrixXf m(3, 4);
	m << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0;
	projectionMatrix = intrinsicMatrix * m;
	Eigen::Vector4f homogeneousPoint;
	Eigen::Vector3f projectedPoint;
	homogeneousPoint << x3D, y3D, z3D, 1;
	projectedPoint = projectionMatrix * homogeneousPoint;
	projectedPoint /= projectedPoint(2);

	x2D = projectedPoint(0);
	y2D = projectedPoint(1);
	if (projectedPoint(0) != projectedPoint(0)) {
		return false;
	}
	return true;
}

void drawSkeletonPoints(pcl::PointCloud<pcl::PointXYZ>& skeletonCloud,
		cv::Mat& imageToDisplay) {
	for (uint i = 0; i < skeletonCloud.size(); i++) {
		float x;
		float y;
		mapJointsTo2D(skeletonCloud.points[i].x, skeletonCloud.points[i].y,
				skeletonCloud.points[i].z, x, y, coeff1, coeff2, coeff3, coeff4,
				coeff5, coeff6, coeff7, coeff8, coeff9);
		if (x >= 0 && x < imageToDisplay.cols && y >= 0
				&& y < imageToDisplay.rows) {
			Point ptn = Point(x, y);
			circle(imageToDisplay, ptn, 6, Scalar(0, 0, 255), -1, 8);
		}

	}
}

Eigen::Vector3f computeRelativeOrientation(int link_index, int base_link_index,
		std::vector<Eigen::Quaternion<float> >& skeletonsData) {
	// Returns relative orientation (in terms of Euler angles (x,y,z) in degrees) of a link with respect to another
	Eigen::Vector3f euler_angles;
	Eigen::Quaternion<float> base_link_orientation;
	Eigen::Quaternion<float> link_orientation = skeletonsData[link_index];

	if (base_link_index >= 0) {
		base_link_orientation = skeletonsData[base_link_index];
	} else {
		base_link_orientation = Eigen::Quaternion<float>(1, 0, 0, 0);
	}

	// Compute relative orientation:
	Eigen::Quaternion<float> link_relative_orientation = link_orientation
			* base_link_orientation.inverse();
	link_relative_orientation.normalize();
	euler_angles = (link_relative_orientation.toRotationMatrix().eulerAngles(0,
			1, 2)) * 180 / M_PI; // convert to euler angles in degrees

	return (euler_angles);
}

bool skeletonIsValid(std::vector<Eigen::Quaternion<float> >& skeletonsData) {
	Eigen::Vector3f euler_angles_hip = computeRelativeOrientation(0, -1,
			skeletonsData);  //hip?torso absolute orientation
	Eigen::Vector3f euler_angles_upper_left_leg_rel_hip =
			computeRelativeOrientation(13, 0, skeletonsData); // up_left_leg orientation wrt hip?torso
	Eigen::Vector3f euler_angles_upper_right_leg_rel_hip =
			computeRelativeOrientation(17, 0, skeletonsData); // up_right_leg orientation wrt hip?torso
	Eigen::Vector3f euler_angles_lower_left_leg_rel_hip =
			computeRelativeOrientation(14, 0, skeletonsData); // low_left_leg orientation wrt hip?torso
	Eigen::Vector3f euler_angles_lower_right_leg_rel_hip =
			computeRelativeOrientation(18, 0, skeletonsData); // low_right_leg orientation wrt hip?torso
	Eigen::Vector3f euler_angles_upper_left_leg_rel_upper_right_leg =
			computeRelativeOrientation(13, 17, skeletonsData); // up_left_leg orientation wrt up_right_leg
	return (std::fabs(euler_angles_hip(0)) < 90)
			&& (std::fabs(euler_angles_upper_left_leg_rel_hip(0)) < 90)
			&& (std::fabs(euler_angles_upper_right_leg_rel_hip(0)) < 90)
			&& (std::fabs(euler_angles_lower_left_leg_rel_hip(0)) < 90)
			&& (std::fabs(euler_angles_lower_right_leg_rel_hip(0)) < 90)
			&& (euler_angles_upper_left_leg_rel_upper_right_leg(1) < 60)
			&& (euler_angles_upper_left_leg_rel_upper_right_leg(1) > -30)
			&& (std::fabs(euler_angles_upper_left_leg_rel_upper_right_leg(2))
					< 30);
}

void readDescriptors(Mat& descriptors_vector, vector<Mat>& users_faces,
		int number_of_users, string descriptors_dir) {

	//read descriptors, user i has files "i_0.yml"..."i_29.yml" and file "face_i.jpg"
	for (int user = 1; user <= number_of_users; user++) {
		for (int j = 0; j <= 29; j++) {
			//descriptors
			stringstream ss;
			ss << descriptors_dir << user << "_" << j << ".yml";
			FileStorage fs(ss.str(), FileStorage::READ);
			Mat temp;
			fs["faceDescriptor"] >> temp;
			//resize temp with 1 row and save it in a toInsert row
			temp = temp.reshape(1, 1);
			descriptors_vector.push_back(temp);
		}
		//face image
		stringstream ss;
		ss << descriptors_dir << "face_" << user << ".jpg";
		Mat temp = imread(ss.str());
		users_faces.push_back(temp);
	}
}

void readSkeletonFeatures(Mat& features_vector, int number_of_users,
		string descriptors_dir) {

	//read features, user i has files "is_1.yml"..."is_30.yml"
	for (int user = 1; user <= number_of_users; user++) {
		for (int j = 0; j <= 29; j++) {
			//features
			stringstream ss;
			ss << descriptors_dir << user << "s_" << j << ".yml";
			FileStorage fs(ss.str(), FileStorage::READ);
			Mat temp;
			fs["skeletonFeatures"] >> temp;
			temp = temp.reshape(1, 1);
			features_vector.push_back(temp);
		}
	}
}

void calculateMeanStdDev(Mat& face_descriptors, Mat& skeleton_features,
		vector<float>& med, vector<float>& dev, int number_of_users) {
	int s = number_of_users * 30;
	//FACE FEATURES MEAN
	for (int row = 0; row < face_descriptors.rows; row++) {
		//for each feature
		for (int feat = 0; feat < face_descriptors.cols; feat++) {
			med[feat] = med[feat]
					+ (float) (face_descriptors.at<float>(row, feat) / s);
		}
	}
	//FACE FEATURES VAR
	for (int row = 0; row < face_descriptors.rows; row++) {
		//for each feature
		for (int feat = 0; feat < face_descriptors.cols; feat++) {
			dev[feat] = dev[feat]
					+ pow(
							(float) (face_descriptors.at<float>(row, feat)
									- med[feat]), 2);
		}
	}
	//SKELETON FEATURES MEAN
	for (int row = 0; row < skeleton_features.rows; row++) {
		//for each feature
		for (int feat = 0; feat < skeleton_features.cols; feat++) {
			med[640 + feat] = med[640 + feat]
					+ (float) (skeleton_features.at<float>(row, feat) / s);
		}
	}
	//SKELETON FEATURES VAR
	for (int row = 0; row < skeleton_features.rows; row++) {
		//for each feature
		for (int feat = 0; feat < skeleton_features.cols; feat++) {
			dev[640 + feat] = dev[640 + feat]
					+ pow(
							(float) (skeleton_features.at<float>(row, feat)
									- med[640 + feat]), 2);
		}
	}
	//STD DEV
	for (int feat = 0; feat < dev.size(); dev++) {
		dev[feat] = sqrt(dev[feat] / s);
	}
}

void normalizeData(Mat& face_descriptors, Mat& skeleton_features,
		vector<float>& med, vector<float>& dev) {
	for (int i = 0; i < face_descriptors.rows; i++) {
		for (int j = 0; j < face_descriptors.cols; j++) {
			face_descriptors.at(i, j) = (face_descriptors.at(i, j) - med[j])
					/ dev[j];
		}
	}
	for (int i = 0; i < skeleton_features.rows; i++) {
		for (int j = 0; j < skeleton_features.cols; j++) {
			skeleton_features.at(i, j) = (skeleton_features.at(i, j)
					- med[640 + j]) / dev[640 + j];
		}
	}
}

void click_cb(int event, int x, int y, int flags, void* param) {
	switch (event) {
	case CV_EVENT_LBUTTONUP: {
		//TODO control if depth is nan
		cv::Point p(x, y);
		_points.push_back(p);
		break;
	}
	case CV_EVENT_RBUTTONUP: {
		_finished = true;
		break;
	}
	}
}

void drawGraphs(int face_f[], int skeleton_f[], int n_users) {
	cv::namedWindow("FACE GRAPH", CV_WINDOW_AUTOSIZE);
	cv::moveWindow("FACE GRAPH", 0, 0);
	cv::namedWindow("SKELETON GRAPH", CV_WINDOW_AUTOSIZE);
	cv::moveWindow("SKELETON GRAPH", 0, 300);

	Mat face = Mat(130, 600, CV_8UC1);
	Mat skeleton = Mat(130, 600, CV_8UC1);

	face.setTo(cv::Scalar(255, 255, 255));
	skeleton.setTo(cv::Scalar(255, 255, 255));

	cv::line(face, Point(50, 5), Point(50, 95), Scalar(0, 0, 0), 2, 1);
	cv::line(face, Point(550, 5), Point(50, 5), Scalar(0, 0, 0), 2, 1);
	cv::line(skeleton, Point(50, 5), Point(50, 95), Scalar(0, 0, 0), 2, 1);
	cv::line(skeleton, Point(550, 5), Point(50, 5), Scalar(0, 0, 0), 2, 1);

	for (int i = 0; i < n_users; i++) {
		stringstream ss;
		ss << i;
		cv::putText(face, ss.str(), Point(50 + (int) (500 / 9) * i, 119),
				FONT_HERSHEY_SIMPLEX, 1, Scalar::all(0), 1, 1);
		cv::putText(skeleton, ss.str(), Point(50 + (int) (500 / 9) * i, 119),
				FONT_HERSHEY_SIMPLEX, 1, Scalar::all(0), 1, 1);
	}

	for (int i = 0; i < 5; i++) {
		cv::line(face, Point(50, 20 * (i + 1)), Point(550, 20 * (i + 1)),
				Scalar(0, 0, 0), 2, 1);
		cv::line(skeleton, Point(50, 20 * (i + 1)), Point(550, 20 * (i + 1)),
				Scalar(0, 0, 0), 2, 1);
	}

	for (int i = 0; i < n_users; i++) {
		circle(face, Point(50 + (int) (500 / 9) * i, 110 - (5 + face_f[i] * 2)),
				6, Scalar(0, 0, 255), 1, 1);
		circle(skeleton,
				Point(50 + (int) (500 / 9) * i, 110 - (5 + skeleton_f[i] * 2)),
				6, Scalar(0, 0, 255), 1, 1);
	}

	cv::imshow("FACE GRAPH", face);
	cv::imshow("SKELETON GRAPH", skeleton);
	cv::waitKey(0); //wait until a button is pressed
}

class Elab {
public:
	Elab();
	void rgbCallback(const sensor_msgs::Image::ConstPtr& msg);
	void depthCallback(const sensor_msgs::Image::ConstPtr& msg);
	void cloudCallback(const sensor_msgs::PointCloud2::ConstPtr& msg);
private:
	tf::TransformListener listener;
};

Elab::Elab() {
}

void Elab::rgbCallback(const sensor_msgs::Image::ConstPtr& msg) {
//convert message to CvImagePtr
	try {
		rgb_cv = cv_bridge::toCvCopy(msg, enc::BGR8);
		cv::imshow("RGB IMAGE", rgb_cv->image);
		cv::waitKey(1);
		new_rgb = true;

	} catch (cv_bridge::Exception& e) {
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

	try {
		tf::StampedTransform transform;

		//update skeleton pointCloud from tf datas
		//and evaluate if it is a good skeleton
		/*
		 * 0 torso /skeleton_1_torso
		 * 1 torso /skeleton_1_torso
		 * 2 neck /skeleton_1_neck
		 * 3 head /skeleton_1_head
		 * 4 left shoulder /skeleton_1_left_shoulder
		 * 5 left elbow /skeleton_1_left_elbow
		 * 6 left hand /skeleton_1_left_hand
		 * 7 left hand /skeleton_1_left_hand
		 * 8 right shoulder /skeleton_1_right_shoulder
		 * 9 right elbow /skeleton_1_right_elbow
		 * 10 right hand /skeleton_1_right_hand
		 * 11 right hand /skeleton_1_right_hand
		 * 12 left hip /skeleton_1_left_hip
		 * 13 left knee /skeleton_1_left_knee
		 * 14 left ankle /skeleton_1_left_ankle
		 * 15 left ankle /skeleton_1_left_ankle
		 * 16 right hip /skeleton_1_right_hip
		 * 17 right knee /skeleton_1_right_knee
		 * 18 right ankle /skeleton_1_right_ankle
		 * 19 right ankle /skeleton_1_right_ankle
		 */
		skeletonCloud.clear();
		vector<Eigen::Quaternion<float> >().swap(skeletonQuality);

		pcl::PointXYZ XYZpoint;
		Eigen::Quaternion<float> rotation_values;

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_torso", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //0
		skeletonCloud.points.push_back(XYZpoint); //1
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_neck", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		x3Dn = transform.getOrigin().x(); //for face selection
		y3Dn = transform.getOrigin().y(); //for face selection
		z3Dn = transform.getOrigin().z(); //for face selection
		skeletonCloud.points.push_back(XYZpoint); //2
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_head", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		x3Dh = transform.getOrigin().x(); //for face selection
		y3Dh = transform.getOrigin().y(); //for face selection
		z3Dh = transform.getOrigin().z(); //for face selection
		skeletonCloud.points.push_back(XYZpoint); //3
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_left_shoulder", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //4
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_left_elbow", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //5
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_left_hand", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //6
		skeletonCloud.points.push_back(XYZpoint); //7
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_right_shoulder", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //8
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_right_elbow", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //9
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_right_hand", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //10
		skeletonCloud.points.push_back(XYZpoint); //11
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_left_hip", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //12
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_left_knee", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //13
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_left_ankle", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //14
		skeletonCloud.points.push_back(XYZpoint); //15
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_right_hip", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //16
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_right_knee", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //17
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);

		listener.lookupTransform("/camera_rgb_optical_frame",
				"/skeleton_1_right_ankle", ros::Time(0), transform);
		XYZpoint.x = transform.getOrigin().x();
		XYZpoint.y = transform.getOrigin().y();
		XYZpoint.z = transform.getOrigin().z();
		skeletonCloud.points.push_back(XYZpoint); //18
		skeletonCloud.points.push_back(XYZpoint); //19
		rotation_values.w() = transform.getRotation().w();
		rotation_values.x() = transform.getRotation().x();
		rotation_values.y() = transform.getRotation().y();
		rotation_values.z() = transform.getRotation().z();
		skeletonQuality.push_back(rotation_values);
		skeletonQuality.push_back(rotation_values);

		//get points of neck and head in 2D for face selection
		mapJointsTo2D(x3Dn, y3Dn, z3Dn, x2Dn, y2Dn, coeff1, coeff2, coeff3,
				coeff4, coeff5, coeff6, coeff7, coeff8, coeff9);
		mapJointsTo2D(x3Dh, y3Dh, z3Dh, x2Dh, y2Dh, coeff1, coeff2, coeff3,
				coeff4, coeff5, coeff6, coeff7, coeff8, coeff9);

		tf_ready = true;

	} catch (tf::TransformException ex) {
		ROS_ERROR("Waiting for tf topics to be created.");
	}
}

void Elab::depthCallback(const sensor_msgs::Image::ConstPtr& msg) {
//convert message to CvImagePtr
	try {
		depth_cv = cv_bridge::toCvCopy(msg, enc::TYPE_8UC1);

	} catch (cv_bridge::Exception& e) {
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

	cv::imshow("DEPTH IMAGE", depth_cv->image);
	cv::waitKey(1);
}

void Elab::cloudCallback(const sensor_msgs::PointCloud2::ConstPtr& msg) {
//convert message to PointColud<PointsXYZ>
	pcl::fromROSMsg(*msg, depthPointcloud);
	new_pointcloud = true;
}

int main(int argc, char** argv) {

	ros::init(argc, argv, "onlineId");

	ros::NodeHandle n;

	Elab i;

	int training = 0;
	n.getParam("trainingFlag", training);

	bool trainingFlag;

	if (training == 0) {
		trainingFlag = true;
		ROS_ERROR("--- Training mode ---");
	} else {
		trainingFlag = false;
		ROS_ERROR("--- Test mode ---");
	}

	ros::Subscriber rgb_sub = n.subscribe("/camera/rgb/image_color", 1,
			&Elab::rgbCallback, &i);
	ros::Subscriber cloud_sub = n.subscribe("camera/depth_registered/points", 1,
			&Elab::cloudCallback, &i);
	ros::Subscriber depth_sub = n.subscribe("camera/depth_registered/image_raw",
			1, &Elab::depthCallback, &i);

	cv::namedWindow("RGB IMAGE", CV_WINDOW_KEEPRATIO);
	cv::moveWindow("RGB IMAGE", 0, 10);
	cv::namedWindow("DEPTH IMAGE", CV_WINDOW_KEEPRATIO);
	cv::moveWindow("DEPTH IMAGE", 800, 10);
	cv::namedWindow("DISPLAY IMAGE", CV_WINDOW_KEEPRATIO);
	cv::moveWindow("DISPLAY IMAGE", 400, 10);
	cv::namedWindow("FACE MATCH IMAGE",
			CV_WINDOW_KEEPRATIO | CV_WINDOW_KEEPRATIO);
	cv::moveWindow("FACE MATCH IMAGE", 60, 360);
	cv::namedWindow("SKELETON MATCH IMAGE",
			CV_WINDOW_KEEPRATIO | CV_WINDOW_KEEPRATIO);
	cv::moveWindow("SKELETON MATCH IMAGE", 460, 360);

	rgb_cv.reset(new cv_bridge::CvImage);
	depth_cv.reset(new cv_bridge::CvImage);

	new_rgb = false;
	new_pointcloud = false;
	tf_ready = false;

	x3Dn = 0;
	y3Dn = 0;
	z3Dn = 0;
	x2Dn = 0;
	y2Dn = 0;
	x3Dh = 0;
	y3Dh = 0;
	z3Dh = 0;
	x2Dh = 0;
	y2Dh = 0;

//set mapJointsTo2D rgb parameters
	coeff1 = 5.70e+02;
	coeff2 = 0.0;
	coeff3 = 3.20e+02;
	coeff4 = 0.0;
	coeff5 = 5.70e+02;
	coeff6 = 2.40e+02;
	coeff7 = 0.0;
	coeff8 = 0.0;
	coeff9 = 1.0;

	Eigen::Matrix3f rgbIntrinsicMatrix;
	Eigen::Matrix3f depthIntrinsicMatrix;
	Eigen::Vector3f translationIR_RGB;
	Eigen::Vector3f rotationAnglesIR_RGB;
	Eigen::Affine3f depthToRGBTransform;

	int counter = 0;
	int acquire_step = 1;
	float keypointSize = 6;

// parse config file
	ForestParam mp_param;
	FaceForestOptions faceForestOption;
	ForestParam head_param;

//parameters and data structures for face selection
	rgbIntrinsicMatrix << 5.25e+02, 0.0, 3.10e+02, 0.0, 5.25e+02, 2.495e+02, 0.0, 0.0, 1.0;
	depthIntrinsicMatrix << 575.8157348632812, 0.0, 314.5, 0.0, 575.8157348632812, 235.5, 0.0, 0.0, 1.0; // default (depth camera_info)

	translationIR_RGB << 0.0250, 0.00000, 0.000;	// default
	rotationAnglesIR_RGB << 0.0, 0.0, 0.0; 			// default

	depthDistortionParameters =
			(cv::Mat_<double>(1, 5) << -2.6386489753128833e-01, 9.9966832163729757e-01, -7.6275862143610667e-04, 5.0350940090814270e-03, -1.3053628089976321e+00);

// Affine transformation between IR and RGB cameras:
	pcl::getTransformation(translationIR_RGB(0), translationIR_RGB(1),
			translationIR_RGB(2), rotationAnglesIR_RGB(0),
			rotationAnglesIR_RGB(1), rotationAnglesIR_RGB(2),
			depthToRGBTransform);
////////////////////////////////

	/*
	 //// PARAMETERS: ////
	 bool saveResultFlag = true; // if true: save result as png image
	 bool use_predefined_bbox = false; // if true: use face bbox written in the text file; if false: use a face detection algorithm
	 keypointSize = 6; // keypoint size for SURF descriptor computation (feature radius)
	 float minHeadPose = -0.25; //15;		// min value for face.headpose for considering a face to be frontal
	 float maxHeadPose = 0.25; //15;		// max value for face.headpose for considering a face to be frontal
	 int dimensionStride = 1; // 1, 2, 4 for choosing depth resolution VGA, QVGA, QQVGA
	 bool faceDetectionFlag = true; // use Viola&Jones face detector for better face localization
	 float voxelSize = 0.01; // voxel size in meters (for downsampling the point cloud)
	 bool groundInitialized = false; // true if the ground plane coefficients have been already computed
	 /////////////////////
	 */

//// INITIALIZATION HEAD POSE ESTIMATION AND FACE FEATURES DETECTION /////
//don't delete
	std::string face_cascade = "data/haarcascade_frontalface_alt.xml";
	std::string headpose_config_file = "data/config_headpose.txt";
	std::string ffd_config_file = "data/config_ffd.txt";
// parse config file
	assert(loadConfigFile(ffd_config_file, mp_param));
	faceForestOption.face_detection_option.path_face_cascade = face_cascade;
	assert(loadConfigFile(headpose_config_file, head_param));
	faceForestOption.head_pose_forest_param = head_param;
	faceForestOption.mp_forest_param = mp_param;

	FaceForest faceForestObject(faceForestOption);
//////////////////////////////////////////////////////////////////////////

//// READ NUMBER OF USERS & DESCRIPTORS ////
//read number of users
	int number_of_users = 0;
	ifstream ReadFile("/home/andrea/descriptors/number_of_users.txt");
	ReadFile >> number_of_users;
	ROS_ERROR("Number of users %d.", number_of_users);

//used only if test mode
	Mat face_descriptors; //mat in cui ogni riga è un descrittore, l'utente i ha i descr in righe 30*i...30*i+29
	vector<Mat> users_faces; //jpg files with the face of users, index i->user i
	Mat skeleton_features;
	vector<DMatch> matchF;
	vector<DMatch> matchS;
	FlannBasedMatcher matcher;
	Mat unknown = imread("/home/andrea/descriptors/unknown.jpg");
	std::vector<float> med(653, 0); //640+13=653
	std::vector<float> dev(653, 0);

	if (trainingFlag) {
		number_of_users++;
		//if in training mode, add 1 to the number of users
		ofstream SaveFile("/home/andrea/descriptors/number_of_users.txt");
		SaveFile << number_of_users;
		SaveFile.close();
	} else {
		ROS_ERROR("Reading descriptors.");
		//if in test mode, read face descriptors, faces, skeleton features
		readDescriptors(face_descriptors, users_faces, number_of_users,
				"/home/andrea/descriptors/");
		readSkeletonFeatures(skeleton_features, number_of_users,
				"/home/andrea/descriptors/");
		calculateMeanStdDev(face_descriptors, skeleton_features, med, dev,
				number_of_users);
		normalizeData(face_descriptors, skeleton_features, med, dev);
		ROS_ERROR("Descriptors loaded.");
	}

//frequencies arrays
	int face_frequencies[number_of_users];
	int skeleton_frequencies[number_of_users];
	for (int i = 0; i < number_of_users; i++) {
		face_frequencies[i] = 0;
		skeleton_frequencies[i] = 0;
	}

//timer
	Timing evaluationTimer;
	int counterTimer = 0;

	while (ros::ok()) {

		ros::spinOnce();

		if (new_rgb && new_pointcloud) { //at least one rgb frame and one point cloud

			if (!_finished) { //first of all calculate ground plane coeff
							  //either in training mode or test mode
				cv::setMouseCallback("RGB IMAGE", &click_cb, NULL);
				while (!_finished) { //right click to finish
					for (uint k = 0; k < _points.size(); k++) {
						cv::Point p = _points[k];
						cv::circle(rgb_cv->image, p, 5, cv::Scalar(0, 0, 255),
								-1);
					}
					cv::imshow("RGB IMAGE", rgb_cv->image);
					cv::waitKey(2);
				}
				pcl::PointCloud<pcl::PointXYZ>::Ptr plane(
						new pcl::PointCloud<pcl::PointXYZ>);
				std::vector<int> index;
				for (int k = 0; k < 3; k++) {
					cv::Point p_2D = _points[k];
					pcl::PointXYZ p_3D = depthPointcloud.at(int(p_2D.x),
							int(p_2D.y));
					for (int j = 0; j < 3; j++)
						points(k, j) = p_3D.data[j];
					index.push_back(k);
					plane->push_back(p_3D);
				}
				pcl::SampleConsensusModelPlane<pcl::PointXYZ> modelPlane(plane);
				modelPlane.computeModelCoefficients(index, plane_coeffs);
				ROS_ERROR("Plane coeffs calculated: %f %f %f %f.",
						plane_coeffs(0), plane_coeffs(1), plane_coeffs(2),
						plane_coeffs(3));
				_finished = true; //to be sure

			} else if (tf_ready) {

				//timer
				if (counterTimer == 20) {
					ROS_ERROR("FPS: %f",
							20 / (evaluationTimer.elapsed() / 1000));
					evaluationTimer.restart();
					counterTimer = 0;
				}

				//face selection
				std::vector<Face> faces;
				cv::Mat imageSelectionGray;
				cv::Mat imageToDisplay = rgb_cv->image.clone();
				bool faceDetectionFlag = true;
				selectFace(faceForestObject, 1, faceDetectionFlag,
						rgb_cv->image, imageToDisplay, imageSelectionGray,
						faces, false, x2Dn, y2Dn, x2Dh, y2Dh, number_of_users,
						counter, trainingFlag);

				//skeleton features
				cv::Mat skeletonFeatures(13, 1, CV_32FC1);
				computeSkeletonFeatures(skeletonCloud, skeletonFeatures);

				drawSkeletonPoints(skeletonCloud, imageToDisplay);

				if (faces.size() > 0) { //if there is at least a face

					// draw green rectangle
					cv::rectangle(imageToDisplay, cv::Point(20, 20),
							cv::Point(60, 60), cv::Scalar(0, 255, 0), 20);

					// Face descriptor computation:
					Face currentFace = faces[0];
					// matrix with descriptors of the fiducial points (one for every row)
					cv::Mat faceDescriptor(SURF_DESCRIPTOR_DIMENSION,
							NUMBER_OF_FIDUCIAL_POINTS, CV_32FC1);
					FaceForest::compute_face_descriptor(imageSelectionGray,
							currentFace, faceDescriptor, keypointSize, false);
					//cv::Mat faceQuality(NUMBER_OF_FIDUCIAL_POINTS + 1, 1,
					//		CV_32F);
					//computeFaceFiducialPointsQuality(currentFace, faceQuality);
					//faceQuality.at<float>(NUMBER_OF_FIDUCIAL_POINTS, 0) =
					//		currentFace.headpose;

					if (trainingFlag) { //save 30 descriptors&features
						if (counter < 30 && skeletonIsValid(skeletonQuality)) { //if skeleton too is good
						//save the face descriptor:
							stringstream ss;
							ss << number_of_users << "_" << counter;
							string str = ss.str();
							std::string filename = "/home/andrea/descriptors/"
									+ str + ".yml";
							FileStorage fs(filename, FileStorage::WRITE);
							fs << "faceDescriptor" << faceDescriptor; // write face features
							fs.release();

							//save skeleton features:
							ss.str(std::string());
							ss << number_of_users << "s_" << counter;
							str = ss.str();
							filename = "/home/andrea/descriptors/" + str
									+ ".yml";
							FileStorage fs1(filename, FileStorage::WRITE);
							fs1 << "skeletonFeatures" << skeletonFeatures; // write skeleton features
							fs1.release();

							ROS_ERROR(
									"Saved descriptors&features files number: [%d]",
									(counter + 1));
							counter++;
						}
					} else if (counter < 50) {

						//face reidentification, use faceDescriptor evaluated above to find a match
						//and skeletonFeatures matching, use skeletonFeatures evaluated above

						//reshape current descriptors
						Mat current_face = faceDescriptor.reshape(1, 1);
						Mat current_skeleton = skeletonFeatures.reshape(1, 1);
						//normalize current descriptors
						normalizeData(current_face, current_skeleton, med, dev);
						//flann
						matcher.match(current_face, face_descriptors, matchF);
						matcher.match(current_skeleton, skeleton_features,
								matchS);

						if (matchF[0].distance <= 1.9) {
							//show matched user
							cv::imshow("FACE MATCH IMAGE",
									users_faces[(int) (matchF[0].trainIdx / 30)]);
							face_frequencies[(int) (matchF[0].trainIdx / 30)]++;
						} else {
							//show grey screen
							cv::imshow("FACE MATCH IMAGE", unknown);
						}
						if (matchS[0].distance <= 0.05) {
							//show matched user
							cv::imshow("SKELETON MATCH IMAGE",
									users_faces[(int) (matchS[0].trainIdx / 30)]);
							skeleton_frequencies[(int) (matchS[0].trainIdx / 30)]++;
						} else {
							//show grey screen
							cv::imshow("SKELETON MATCH IMAGE", unknown);
						}

						counter++;
					} else {
						for (int i = 0; i < number_of_users; i++) {
							ROS_ERROR(
									"User %d got %d face matches and %d skeleton matches.",
									(i + 1), face_frequencies[i],
									skeleton_frequencies[i]);
						}
						//drawGraphs(face_frequencies, skeleton_frequencies,
						//		number_of_users);
						break;
					}

					//draw points in imageToDisplay
					int thickness = -1;
					int lineType = 8;

					if (x2Dn >= 0 && x2Dn < imageToDisplay.cols && y2Dn >= 0
							&& y2Dn < imageToDisplay.rows) {

						Point ptn = Point(x2Dn, y2Dn);
						Point pth = Point(x2Dh, y2Dh);

						circle(imageToDisplay, ptn, 6, Scalar(0, 0, 255),
								thickness, lineType);
						circle(imageToDisplay, pth, 6, Scalar(0, 0, 255),
								thickness, lineType);
					}

					//show images
					cv::imshow("RGB IMAGE", rgb_cv->image);
					cv::imshow("DISPLAY IMAGE", imageToDisplay);
					cv::waitKey(1);
				}

				//timer
				counterTimer++;
			}
		}
	}

	return 0;
}
