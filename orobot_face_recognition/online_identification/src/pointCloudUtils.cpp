/*
 * pointCloudUtils.cpp
 *
 *  Created on: Oct 18, 2012
 *      Author: Matteo Munaro
 */

#include <onlineIdentification/pointCloudUtils.h>

void createPointcloudFromDepthImage(cv::Mat& depthImage, pcl::PointCloud<pcl::PointXYZ>::Ptr& outputPointcloud, Eigen::Matrix3f& depthIntrinsicMatrix, int dimensionStride)
{
	// For point clouds XYZ
	float depthFocalInvertedX = 1/depthIntrinsicMatrix(0,0);	// 1/fx
	float depthFocalInvertedY = 1/depthIntrinsicMatrix(1,1);	// 1/fy

	pcl::PointXYZ newPoint;

	outputPointcloud->points.resize(depthImage.cols*depthImage.rows, newPoint);
	outputPointcloud->width = depthImage.cols;
	outputPointcloud->height = depthImage.rows;
	for (int i=0;i<depthImage.rows;i=i+dimensionStride)
	{
		for (int j=0;j<depthImage.cols;j=j+dimensionStride)
		{
			float depthValue = depthImage.at<float>(i,j);

			if (depthValue > 0)
			{
				// Find 3D position respect to rgb frame:
				newPoint.z = depthValue;
				newPoint.x = (j - depthIntrinsicMatrix(0,2)) * newPoint.z * depthFocalInvertedX;
				newPoint.y = (i - depthIntrinsicMatrix(1,2)) * newPoint.z * depthFocalInvertedY;
				outputPointcloud->at(j,i) = newPoint;
			}
			else
			{
				newPoint.z = std::numeric_limits<float>::quiet_NaN();
				newPoint.x = std::numeric_limits<float>::quiet_NaN();
				newPoint.y = std::numeric_limits<float>::quiet_NaN();
				outputPointcloud->at(j,i) = newPoint;
			}
		}
	}
}

void createPointcloudFromRegisteredDepthImage(cv::Mat& depthImage, cv::Mat& rgbImage, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& outputPointcloud, Eigen::Matrix3f& rgbIntrinsicMatrix, int dimensionStride)
{
	int scaleFactor = rgbImage.cols / depthImage.cols;
	// For point clouds XYZRGB
	float rgbFocalInvertedX = 1/rgbIntrinsicMatrix(0,0);	// 1/fx
	float rgbFocalInvertedY = 1/rgbIntrinsicMatrix(1,1);	// 1/fy

	pcl::PointXYZRGB newPoint;
	for (int i=0;i<depthImage.rows;i=i+dimensionStride)
	{
		for (int j=0;j<depthImage.cols;j=j+dimensionStride)
		{
			float depthValue = depthImage.at<float>(i,j);

			if (depthValue == depthValue)
			{
				// Find 3D position respect to rgb frame:
				newPoint.z = depthValue;
				newPoint.x = (j - rgbIntrinsicMatrix(0,2)) * newPoint.z * rgbFocalInvertedX;
				newPoint.y = (i - rgbIntrinsicMatrix(1,2)) * newPoint.z * rgbFocalInvertedY;
				newPoint.r = rgbImage.at<cv::Vec3b>(scaleFactor*i,scaleFactor*j)[2];
				newPoint.g = rgbImage.at<cv::Vec3b>(scaleFactor*i,scaleFactor*j)[1];
				newPoint.b = rgbImage.at<cv::Vec3b>(scaleFactor*i,scaleFactor*j)[0];
				outputPointcloud->push_back(newPoint);
			}
			else
			{
				newPoint.z = std::numeric_limits<float>::quiet_NaN();
				newPoint.x = std::numeric_limits<float>::quiet_NaN();
				newPoint.y = std::numeric_limits<float>::quiet_NaN();
				newPoint.r = std::numeric_limits<unsigned char>::quiet_NaN();
				newPoint.g = std::numeric_limits<unsigned char>::quiet_NaN();
				newPoint.b = std::numeric_limits<unsigned char>::quiet_NaN();
				outputPointcloud->push_back(newPoint);
			}
		}
	}
}

void transformPointcloudWithAffineTransform(pcl::PointCloud<pcl::PointXYZ>::Ptr& XYZ_CloudIn, pcl::PointCloud<pcl::PointXYZ>::Ptr& XYZ_CloudOut, Eigen::Affine3f transform)
{
	for (size_t i = 0; i < XYZ_CloudIn->points.size(); i++)
	{
		pcl::PointXYZ pCurrent = XYZ_CloudIn->points[i];
		//pcl::PointXYZ registeredPoint = pcl::transformXYZ(transform, pCurrent);
		pcl::PointXYZ registeredPoint = pcl::transformPoint(pCurrent, transform);
		XYZ_CloudOut->push_back(registeredPoint);
	}
}

void createDepthImageFromPointcloud(pcl::PointCloud<pcl::PointXYZ>::Ptr& XYZ_Cloud, cv::Mat& depthImage, Eigen::Matrix3f intrinsicMatrix, int rows, int cols)
{
	// Compute projection matrix:
	Eigen::MatrixXf projectionMatrix;
	Eigen::MatrixXf m(3,4);
	m << 1,0,0,0,
		 0,1,0,0,
		 0,0,1,0;
	projectionMatrix = intrinsicMatrix * m;

	Eigen::Vector4f homogeneousPoint;
	Eigen::Vector3f projectedPoint;
	depthImage = cv::Mat::zeros(rows, cols, CV_32F);
	for (size_t i = 0; i < XYZ_Cloud->points.size(); i++)
	{
		pcl::PointXYZ* pCurrent = &XYZ_Cloud->points[i];
		if (!isnan(pCurrent->z))
		{
			homogeneousPoint << pCurrent->x, pCurrent->y, pCurrent->z, 1;
			projectedPoint = projectionMatrix * homogeneousPoint;
			projectedPoint /= projectedPoint(2);
			if ((projectedPoint(0) >= 0) && (projectedPoint(1) >= 0) && (projectedPoint(0) < depthImage.cols)
					&& (projectedPoint(1) < depthImage.rows))
			{
				depthImage.at<float>(projectedPoint(1),projectedPoint(0)) = pCurrent->z;
			}
		}
	}
}

void createDepthImageAndUserMapFromPointcloud(pcl::PointCloud<pcl::PointXYZ>::Ptr& XYZ_Cloud, cv::Mat& depthImage, cv::Mat& userMapIn, cv::Mat& userMapOut, Eigen::Matrix3f intrinsicMatrix)
{
	// Compute projection matrix:
	Eigen::MatrixXf projectionMatrix;
	Eigen::MatrixXf m(3,4);
	m << 1,0,0,0,
		 0,1,0,0,
		 0,0,1,0;
	projectionMatrix = intrinsicMatrix * m;

	Eigen::Vector4f homogeneousPoint;
	Eigen::Vector3f projectedPoint;
	depthImage = cv::Mat::zeros(depthImage.rows, depthImage.cols, CV_32F);
	userMapOut = cv::Mat::zeros(userMapOut.rows, userMapOut.cols, CV_8U);
	int depthWidth = depthImage.cols;
	for (int i = 0; i < XYZ_Cloud->points.size(); i++)
	{
		pcl::PointXYZ* pCurrent = &XYZ_Cloud->points[i];
		if (!isnan(pCurrent->z))
		{
			homogeneousPoint << pCurrent->x, pCurrent->y, pCurrent->z, 1;
			projectedPoint = projectionMatrix * homogeneousPoint;
			projectedPoint /= projectedPoint(2);
			if ((projectedPoint(0) >= 0) && (projectedPoint(1) >= 0) && (projectedPoint(0) < depthImage.cols)
					&& (projectedPoint(1) < depthImage.rows))
			{
				depthImage.at<float>(projectedPoint(1),projectedPoint(0)) = pCurrent->z;
				userMapOut.at<unsigned char>(projectedPoint(1),projectedPoint(0)) = userMapIn.at<unsigned char>((int)i/depthWidth, (int)i%depthWidth);
			}
		}
	}
}

void depthOverlayOnRGB(cv::Mat& rgbImage, cv::Mat& depthImage, float maxDistance)
{	// used to show a part of the depth image over the RGB image
	for (int i=0;i<depthImage.rows;i++)
	{
		for (int j=0;j<depthImage.cols;j++)
		{	// if the depth of this point is under maxDistance
			if ((depthImage.at<float>(i,j) < maxDistance) && (depthImage.at<float>(i,j) > 0))
				rgbImage.at<cv::Vec3b>(i,j)[2] = 255;	// put red value to the maximum for that point
		}
	}
}

void displayPointcloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr xyzrgbPointcloud, pcl::visualization::PCLVisualizer& viewer,
		bool& pclViewerInitialized, std::string cloudName)
{
	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(xyzrgbPointcloud);
	if (pclViewerInitialized)
	{
		viewer.updatePointCloud(xyzrgbPointcloud, rgb, cloudName);
	}
	else
	{
		viewer.addPointCloud<pcl::PointXYZRGB> (xyzrgbPointcloud, rgb, cloudName);
		pclViewerInitialized = true;
	}
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, cloudName);
	viewer.spinOnce();
}

void displayPointcloud(pcl::PointCloud<pcl::PointXYZ>::Ptr xyzPointcloud, pcl::visualization::PCLVisualizer& viewer,
		bool& pclViewerInitialized, std::string cloudName)
{
	if (pclViewerInitialized)
	{
		viewer.updatePointCloud(xyzPointcloud, cloudName);
	}
	else
	{
		viewer.addPointCloud<pcl::PointXYZ> (xyzPointcloud, cloudName);
		pclViewerInitialized = true;
	}
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, cloudName);
	viewer.spinOnce();
}

void mapSkeletonToRGB(std::vector<float>& skeletonsData, pcl::PointCloud<pcl::PointXYZ>::Ptr& originalSdkSkeletonJoints,
		pcl::PointCloud<pcl::PointXYZ>::Ptr& depthPointcloud, Eigen::Affine3f depthToRGBTransform, Eigen::Matrix3f& rgbIntrinsicMatrix, int depthWidth,
		int NUMBER_OF_TRACKED_SKELETON, int NUMBER_OF_SKELETAL_JOINTS, int NUMBER_OF_JOINT_INFO)
{
	// Compute projection matrix:
	Eigen::MatrixXf projectionMatrix;
	Eigen::MatrixXf m(3,4);
	m << 1,0,0,0,
			0,1,0,0,
			0,0,1,0;
	projectionMatrix = rgbIntrinsicMatrix * m;

	// create a point cloud with skeleton joints 3D position
	pcl::PointCloud<pcl::PointXYZ>::Ptr skeletonCloud(new pcl::PointCloud<pcl::PointXYZ>());
	pcl::PointCloud<pcl::PointXYZ>::Ptr registeredSkeletonCloud(new pcl::PointCloud<pcl::PointXYZ>());
	pcl::PointXYZ XYZpoint;
	int xPos, yPos;
	for (int t = 0; t < NUMBER_OF_TRACKED_SKELETON; t++)				// for every user
	{
		if (skeletonsData[t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] > 0) // if it is a valid user
		{
			for (int i = 0; i < NUMBER_OF_SKELETAL_JOINTS; i++)			// for every skeleton joint
			{
//				XYZpoint.x = skeletonsData[1 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
//				XYZpoint.y = skeletonsData[2 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
//				XYZpoint.z = skeletonsData[3 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
//				skeletonCloud->points.push_back(XYZpoint);

				XYZpoint.x = skeletonsData[1 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				XYZpoint.y = skeletonsData[2 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				XYZpoint.z = skeletonsData[3 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				originalSdkSkeletonJoints->points.push_back(XYZpoint);
				xPos = skeletonsData[4 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				yPos = skeletonsData[5 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];

				// check if depthPointcloud->points[xPos + yPos*depthWidth] is a valid point
				pcl::PointXYZ depthPoint = depthPointcloud->points[xPos + yPos*depthWidth];
				if (isnan(depthPoint.z))		// if it is not a valid point (it is NaN)
				{
					// select the nearest valid point:
					int validX, validY;
					findNearestInImageValidPoint(xPos, yPos, depthWidth, 10, depthPointcloud, depthPoint, validX, validY);
				}
				skeletonCloud->points.push_back(depthPoint);
				//skeletonCloud->points.push_back(depthPointcloud->points[xPos + yPos*depthWidth]);

////				char vowel;
//				if (depthPoint.x != depthPoint.x)		// if depth point not valid
//				{										// try with a point near there
////					vowel = 'A';
//					depthPoint = depthPointcloud->points[xPos + yPos*depthWidth + 2];
//					if (depthPoint.x != depthPoint.x)
//					{
////						vowel = 'E';
//						depthPoint = depthPointcloud->points[xPos + yPos*depthWidth - 2];
//						if (depthPoint.x != depthPoint.x)
//						{
////							vowel = 'I';
//							depthPoint = depthPointcloud->points[xPos + (yPos+2)*depthWidth];
//							if (depthPoint.x != depthPoint.x)
//							{
////								vowel = 'O';
//								depthPoint = depthPointcloud->points[xPos + (yPos-2)*depthWidth];
//								if (depthPoint.x != depthPoint.x)
//								{
////									vowel = 'U';
//									depthPoint = depthPointcloud->points[xPos + yPos*depthWidth];
////									countU++;
//								}
//							}
//						}
//					}
////					std::cout << vowel << "!!" << std::endl;
//				}
//
//				skeletonCloud->points.push_back(depthPoint);
//				//skeletonCloud->points.push_back(depthPointcloud->points[xPos + yPos*depthWidth]);
			}
		}
	}

//	if (countU > 0)
//		std::cout << countU << std::endl;

	// transform this point cloud to RGB frame
	transformPointcloudWithAffineTransform(skeletonCloud, registeredSkeletonCloud, depthToRGBTransform);

	// obtain joints position in the RGB image
	int currentPointIndex = 0;
	Eigen::Vector4f homogeneousPoint;
	Eigen::Vector3f projectedPoint;
	for (int t = 0; t < NUMBER_OF_TRACKED_SKELETON; t++)				// for every user
	{
		if (skeletonsData[t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] > 0) // if it is a valid user
		{
			for (int i = 0; i < NUMBER_OF_SKELETAL_JOINTS; i++)			// for every skeleton joint
			{
				XYZpoint = registeredSkeletonCloud->points[currentPointIndex];
				homogeneousPoint << XYZpoint.x, XYZpoint.y, XYZpoint.z, 1;
				projectedPoint = projectionMatrix * homogeneousPoint;
				projectedPoint /= projectedPoint(2);
//				if ((projectedPoint(0) >= 0) && (projectedPoint(1) >= 0) && (projectedPoint(0) < depthImage.cols)
//						&& (projectedPoint(1) < depthImage.rows))
				skeletonsData[1 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = XYZpoint.x;
				skeletonsData[2 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = XYZpoint.y;
				skeletonsData[3 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = XYZpoint.z;
				skeletonsData[4 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = projectedPoint(0);
				skeletonsData[5 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = projectedPoint(1);
				if (projectedPoint(0) != projectedPoint(0))
				{
					skeletonsData[6 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = 0;
				}
				currentPointIndex++;
			}
		}
	}
}

void mapSkeletonToRGBOpenNI(std::vector<float>& skeletonsData, pcl::PointCloud<pcl::PointXYZ>::Ptr& originalSdkSkeletonJoints,
		pcl::PointCloud<pcl::PointXYZ>::Ptr& depthPointcloud, Eigen::Affine3f depthToRGBTransform, Eigen::Matrix3f& rgbIntrinsicMatrix, int depthWidth,
		int NUMBER_OF_TRACKED_SKELETON, int NUMBER_OF_SKELETAL_JOINTS, int NUMBER_OF_JOINT_INFO)
{
	// Compute projection matrix:
	Eigen::MatrixXf projectionMatrix;
	Eigen::MatrixXf m(3,4);
	m << 1,0,0,0,
			0,1,0,0,
			0,0,1,0;
	projectionMatrix = rgbIntrinsicMatrix * m;

	// create a point cloud with skeleton joints 3D position
	pcl::PointCloud<pcl::PointXYZ>::Ptr skeletonCloud(new pcl::PointCloud<pcl::PointXYZ>());
	pcl::PointCloud<pcl::PointXYZ>::Ptr registeredSkeletonCloud(new pcl::PointCloud<pcl::PointXYZ>());
	pcl::PointXYZ XYZpoint;
	int xPos, yPos;
	//	int countU = 0;
	for (int t = 0; t < NUMBER_OF_TRACKED_SKELETON; t++)				// for every user
	{
		if (skeletonsData[t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] > 0) // if it is a valid user
		{
			for (int i = 0; i < NUMBER_OF_SKELETAL_JOINTS; i++)			// for every skeleton joint
			{
				XYZpoint.x = skeletonsData[1 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				XYZpoint.y = skeletonsData[2 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				XYZpoint.z = skeletonsData[3 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				skeletonCloud->points.push_back(XYZpoint);

//				XYZpoint.x = skeletonsData[1 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
//				XYZpoint.y = skeletonsData[2 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
//				XYZpoint.z = skeletonsData[3 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				originalSdkSkeletonJoints->points.push_back(XYZpoint);
				//                xPos = skeletonsData[4 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				//                yPos = skeletonsData[5 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];

				//                // check if depthPointcloud->points[xPos + yPos*depthWidth] is a valid point
				//                pcl::PointXYZ depthPoint = depthPointcloud->points[xPos + yPos*depthWidth];
				//                if (isnan(depthPoint.z))		// if it is not a valid point (it is NaN)
				//                {
				//                    // select the nearest valid point:
				//                    int validX, validY;
				//                    findNearestInImageValidPoint(xPos, yPos, depthWidth, 10, depthPointcloud, depthPoint, validX, validY);
				//                }
				//                skeletonCloud->points.push_back(depthPoint);
				//skeletonCloud->points.push_back(depthPointcloud->points[xPos + yPos*depthWidth]);
			}
		}
	}

	// transform this point cloud to RGB frame
	transformPointcloudWithAffineTransform(skeletonCloud, registeredSkeletonCloud, depthToRGBTransform);

	// obtain joints position in the RGB image
	int currentPointIndex = 0;
	Eigen::Vector4f homogeneousPoint;
	Eigen::Vector3f projectedPoint;
	for (int t = 0; t < NUMBER_OF_TRACKED_SKELETON; t++)				// for every user
	{
		if (skeletonsData[t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] > 0) // if it is a valid user
		{
			for (int i = 0; i < NUMBER_OF_SKELETAL_JOINTS; i++)			// for every skeleton joint
			{
				XYZpoint = registeredSkeletonCloud->points[currentPointIndex];
				homogeneousPoint << XYZpoint.x, XYZpoint.y, XYZpoint.z, 1;
				projectedPoint = projectionMatrix * homogeneousPoint;
				projectedPoint /= projectedPoint(2);
				//				if ((projectedPoint(0) >= 0) && (projectedPoint(1) >= 0) && (projectedPoint(0) < depthImage.cols)
				//						&& (projectedPoint(1) < depthImage.rows))
				skeletonsData[1 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = XYZpoint.x;
				skeletonsData[2 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = XYZpoint.y;
				skeletonsData[3 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = XYZpoint.z;
				skeletonsData[4 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = projectedPoint(0);
				skeletonsData[5 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = projectedPoint(1);
				if (projectedPoint(0) != projectedPoint(0))
				{
					skeletonsData[6 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = 0;
				}
				currentPointIndex++;
			}
		}
	}
}

void selectPointsWithGivenIndex(pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud, cv::Mat& indexMap, unsigned char index, pcl::PointCloud<pcl::PointXYZ>::Ptr outputPointcloud)
{
	for (int i=0;i<indexMap.rows;i++)
	{
		for (int j=0;j<indexMap.cols;j++)
		{
			if (indexMap.at<unsigned char>(i,j) == index)
			{
				outputPointcloud->points.push_back(inputPointcloud->points[j + i*indexMap.cols]);
			}
		}
	}
}

void pointDistanceFromSegment(float cx, float cy, float ax, float ay, float bx, float by, float &distanceSegment, float &distanceLine)
{
	// find the distance from the point (cx,cy) to the line
	// determined by the points (ax,ay) and (bx,by)
	//
	// distanceSegment = distance from the point to the line segment
	// distanceLine = distance from the point to the line (assuming	infinite extent in both directions
	/*
	Subject 1.02: How do I find the distance from a point to a line?
    Let the point be C (Cx,Cy) and the line be AB (Ax,Ay) to (Bx,By).
    Let P be the point of perpendicular projection of C on AB.  The parameter
    r, which indicates P's position along AB, is computed by the dot product
    of AC and AB divided by the square of the length of AB:
    (1)     AC dot AB
        r = ---------
            ||AB||^2

    r has the following meaning:
        r=0      P = A
        r=1      P = B
        r<0      P is on the backward extension of AB
        r>1      P is on the forward extension of AB
        0<r<1    P is interior to AB
    The length of a line segment in d dimensions, AB is computed by:
        L = sqrt( (Bx-Ax)^2 + (By-Ay)^2 + ... + (Bd-Ad)^2)
    so in 2D:
        L = sqrt( (Bx-Ax)^2 + (By-Ay)^2 )
    and the dot product of two vectors in d dimensions, U dot V is computed:
        D = (Ux * Vx) + (Uy * Vy) + ... + (Ud * Vd)
    so in 2D:
        D = (Ux * Vx) + (Uy * Vy)
    So (1) expands to:
            (Cx-Ax)(Bx-Ax) + (Cy-Ay)(By-Ay)
        r = -------------------------------
                          L^2
    The point P can then be found:
        Px = Ax + r(Bx-Ax)
        Py = Ay + r(By-Ay)
    And the distance from A to P = r*L.
    Use another parameter s to indicate the location along PC, with the
    following meaning:
           s<0      C is left of AB
           s>0      C is right of AB
           s=0      C is on AB
    Compute s as follows:
            (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay)
        s = -----------------------------
                        L^2
    Then the distance from C to P = |s|*L.
	 */

	float r_numerator = (cx-ax)*(bx-ax) + (cy-ay)*(by-ay);
	float r_denomenator = (bx-ax)*(bx-ax) + (by-ay)*(by-ay);
	float r = r_numerator / r_denomenator;
	float px = ax + r*(bx-ax);
    float py = ay + r*(by-ay);
    float s =  ((ay-cy)*(bx-ax)-(ax-cx)*(by-ay) ) / r_denomenator;
	distanceLine = fabs(s)*sqrt(r_denomenator);
	// (xx,yy) is the point on the lineSegment closest to (cx,cy)
	float xx = px;
	float yy = py;
	if ( (r >= 0) && (r <= 1) )
	{
		distanceSegment = distanceLine;
	}
	else
	{
		float dist1 = (cx-ax)*(cx-ax) + (cy-ay)*(cy-ay);
		float dist2 = (cx-bx)*(cx-bx) + (cy-by)*(cy-by);
		if (dist1 < dist2)
		{
			xx = ax;
			yy = ay;
			distanceSegment = sqrt(dist1);
		}
		else
		{
			xx = bx;
			yy = by;
			distanceSegment = sqrt(dist2);
		}
	}
}

void pointDistanceFromSegment(Eigen::Vector3f C, Eigen::Vector3f A, Eigen::Vector3f B, float &distanceSegment, float &distanceLine)
{
	//Compute the distance from the segment AB to C:
	int dot1 = (C-B).dot(B-A);
	if(dot1 > 0)
	{
		distanceSegment = sqrt((B-C).dot(B-C));
	}
	else
	{
		int dot2 = (C-A).dot(A-B);
		if(dot2 > 0)
		{
			distanceSegment = sqrt((A-C).dot(A-C));
		}
		else
		{
			distanceSegment = -1;
		}
	}
}

void pointDistanceFromSegment3D(Eigen::Vector3f& p, Eigen::Vector3f& v, Eigen::Vector3f& w, float &distanceSegment, float &distanceLine)
{
	// Return minimum distance between line segment vw and point p
	float l2 = ((v-w).squaredNorm());  // i.e. |w-v|^2 -  avoid a sqrt
	if (l2 == 0.0)
	{
		distanceSegment = (p-v).norm();   // v == w case
		return;
	}
	// Consider the line extending the segment, parameterized as v + t (w - v).
	// We find projection of point p onto the line.
	// It falls where t = [(p-v) . (w-v)] / |w-v|^2
	float t = (p - v).dot(w - v) / l2;
	if (t < 0.0)
	{
		distanceSegment = (p-v).norm();
		return;       // Beyond the 'v' end of the segment
	}
	else
	{
		if (t > 1.0)
		{
			distanceSegment = (p-w).norm();
			return;        // Beyond the 'w' end of the segment
		}
	}
	Eigen::Vector3f projection = v + t * (w - v);  // Projection falls on the segment
	distanceSegment = (p - projection).norm();
}

void computeLinksExtremePoints(std::vector<float>& skeletonsData, int skelNumber, int NUMBER_OF_JOINT_INFO,
		int NUMBER_OF_SKELETAL_JOINTS, std::vector<cv::Vec3f>& bodyPartsColors, cv::Mat& extremePoints)
{
	// Compute links extreme points:
	for (unsigned int j = 0; j < bodyPartsColors.size()-2; j++)
		//**//for (unsigned int j = 0; j < bodyPartsColors.size(); j++)
	{
		extremePoints.at<unsigned int>(j,0) = (unsigned int)(skeletonsData[4 + (skeletonsData[8 + (j+1)*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS])
		                                             *NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
		extremePoints.at<unsigned int>(j,1) = (unsigned int)(skeletonsData[5 + (skeletonsData[8 + (j+1)*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS])
		                                             *NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
		extremePoints.at<unsigned int>(j,2) = (unsigned int)(skeletonsData[4 + (skeletonsData[9 + (j+1)*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS])
		                                             *NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
		extremePoints.at<unsigned int>(j,3) = (unsigned int)(skeletonsData[5 + (skeletonsData[9 + (j+1)*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS])
		                                             *NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	}
	// Add two more (fictitious) links (neck-hip left, neck-hip right):
	extremePoints.at<unsigned int>(19,0) = (unsigned int)(skeletonsData[4 + 2*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<unsigned int>(19,1) = (unsigned int)(skeletonsData[5 + 2*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<unsigned int>(19,2) = (unsigned int)(skeletonsData[4 + 16*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<unsigned int>(19,3) = (unsigned int)(skeletonsData[5 + 16*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<unsigned int>(20,0) = (unsigned int)(skeletonsData[4 + 2*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<unsigned int>(20,1) = (unsigned int)(skeletonsData[5 + 2*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<unsigned int>(20,2) = (unsigned int)(skeletonsData[4 + 12*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<unsigned int>(20,3) = (unsigned int)(skeletonsData[5 + 12*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
}

void computeLinksExtremePoints3D(std::vector<float>& skeletonsData, int skelNumber, int NUMBER_OF_JOINT_INFO,
		int NUMBER_OF_SKELETAL_JOINTS, std::vector<cv::Vec3f>& bodyPartsColors, cv::Mat& extremePoints)
{
	// Compute links extreme points:
	for (unsigned int j = 0; j < bodyPartsColors.size()-2; j++)
		//**//for (unsigned int j = 0; j < bodyPartsColors.size(); j++)
	{
		extremePoints.at<float>(j,0) = (skeletonsData[1 + (skeletonsData[8 + (j+1)*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS])
		                                             *NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
		extremePoints.at<float>(j,1) = (skeletonsData[2 + (skeletonsData[8 + (j+1)*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS])
		                                             *NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
		extremePoints.at<float>(j,2) = (skeletonsData[3 + (skeletonsData[8 + (j+1)*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS])
		                                             *NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
		extremePoints.at<float>(j,3) = (skeletonsData[1 + (skeletonsData[9 + (j+1)*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS])
		                                             *NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
		extremePoints.at<float>(j,4) = (skeletonsData[2 + (skeletonsData[9 + (j+1)*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS])
		                                             *NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
		extremePoints.at<float>(j,5) = (skeletonsData[3 + (skeletonsData[9 + (j+1)*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS])
		                                             *NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	}
	// Add two more (fictitious) links (neck-hip left, neck-hip right):
	extremePoints.at<float>(19,0) = (skeletonsData[1 + 2*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<float>(19,1) = (skeletonsData[2 + 2*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<float>(19,2) = (skeletonsData[3 + 2*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<float>(19,3) = (skeletonsData[1 + 16*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<float>(19,4) = (skeletonsData[2 + 16*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<float>(19,5) = (skeletonsData[3 + 16*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<float>(20,0) = (skeletonsData[1 + 2*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<float>(20,1) = (skeletonsData[2 + 2*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<float>(20,2) = (skeletonsData[3 + 2*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<float>(20,3) = (skeletonsData[1 + 12*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<float>(20,4) = (skeletonsData[2 + 12*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);
	extremePoints.at<float>(20,5) = (skeletonsData[3 + 12*NUMBER_OF_JOINT_INFO + skelNumber*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS]);

//	for (int j = 0; j < 21; j++)
//	{
//		std::cout << extremePoints.at<float>(j,0) << " " << extremePoints.at<float>(j,1) << " " << extremePoints.at<float>(j,2) << " "
//				<< extremePoints.at<float>(j,3) << " " << extremePoints.at<float>(j,4) << " " << extremePoints.at<float>(j,5)<< std::endl;
//	}
}

void assignLabels(pcl::PointCloud<pcl::PointXYZ>::Ptr& personPointcloud, std::vector<float>& skeletonsData, cv::Mat& extremePoints,
		Eigen::Matrix3f& depthIntrinsicMatrix, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& personPointcloudLabeled,
		cv::Mat& personPointsLabels, std::vector<cv::Vec3f>& bodyPartsColors)
{
	// Compute projection matrix:
	Eigen::MatrixXf projectionMatrix;
	Eigen::MatrixXf m(3,4);
	m << 1,0,0,0,
		 0,1,0,0,
		 0,0,1,0;
	projectionMatrix = depthIntrinsicMatrix * m;

	Eigen::Vector4f homogeneousPoint;
	Eigen::Vector3f projectedPoint;
	cv::Mat distances(bodyPartsColors.size(),1,CV_32F);
	double minVal, maxVal;
	int minIdx;
	float distanceToLine;
	pcl::PointXYZRGB coloredPoint;
	for (unsigned int i = 0; i < personPointcloud->points.size(); i++)
	{
		// Project the 3D point on the depth image:
		homogeneousPoint << personPointcloud->points[i].x, personPointcloud->points[i].y, personPointcloud->points[i].z, 1;
		projectedPoint = projectionMatrix * homogeneousPoint;
		projectedPoint /= projectedPoint(2);
		// Find distances to all skeleton links:
		for (unsigned int j = 0; j < bodyPartsColors.size(); j++)
		{
			pointDistanceFromSegment(projectedPoint(0), projectedPoint(1), extremePoints.at<unsigned int>(j,0), extremePoints.at<unsigned int>(j,1),
					extremePoints.at<unsigned int>(j,2), extremePoints.at<unsigned int>(j,3), distances.at<float>(j,0), distanceToLine);
		}
		cv::minMaxIdx(distances, &minVal, &maxVal, &minIdx);

		// Create colored point:
		coloredPoint.x = personPointcloud->points[i].x;
		coloredPoint.y = personPointcloud->points[i].y;
		coloredPoint.z = personPointcloud->points[i].z;
		coloredPoint.r = bodyPartsColors[minIdx].val[0];
		coloredPoint.g = bodyPartsColors[minIdx].val[1];
		coloredPoint.b = bodyPartsColors[minIdx].val[2];
		personPointcloudLabeled->points.push_back(coloredPoint);// add colored point to labeled pointcloud
		personPointsLabels.at<unsigned int>(i,0) = (unsigned int)(minIdx);		// write label number
	}
}

void assignLabels3D(pcl::PointCloud<pcl::PointXYZ>::Ptr& personPointcloud, std::vector<float>& skeletonsData, cv::Mat& extremePoints,
		Eigen::Matrix3f& depthIntrinsicMatrix, pcl::PointCloud<pcl::PointXYZRGB>::Ptr& personPointcloudLabeled,
		cv::Mat& personPointsLabels, std::vector<cv::Vec3f>& bodyPartsColors)
{
	cv::Mat distances(bodyPartsColors.size(),1,CV_32F);
	double minVal, maxVal;
	int minIdx;
	float distanceToLine;
	pcl::PointXYZRGB coloredPoint;
	for (unsigned int i = 0; i < personPointcloud->points.size(); i++)
	{
		Eigen::Vector3f currentPoint(personPointcloud->points[i].x, personPointcloud->points[i].y, personPointcloud->points[i].z);

		// Find distances to all skeleton links:
		for (unsigned int j = 0; j < bodyPartsColors.size(); j++)
		{
			Eigen::Vector3f extreme1(extremePoints.at<float>(j,0), extremePoints.at<float>(j,1), extremePoints.at<float>(j,2));
			Eigen::Vector3f extreme2(extremePoints.at<float>(j,3), extremePoints.at<float>(j,4), extremePoints.at<float>(j,5));
			pointDistanceFromSegment3D(currentPoint, extreme1, extreme2, distances.at<float>(j,0), distanceToLine);
		}
		cv::minMaxIdx(distances, &minVal, &maxVal, &minIdx);

		// Create colored point:
		coloredPoint.x = personPointcloud->points[i].x;
		coloredPoint.y = personPointcloud->points[i].y;
		coloredPoint.z = personPointcloud->points[i].z;
		coloredPoint.r = bodyPartsColors[minIdx].val[0];
		coloredPoint.g = bodyPartsColors[minIdx].val[1];
		coloredPoint.b = bodyPartsColors[minIdx].val[2];
		personPointcloudLabeled->points.push_back(coloredPoint);// add colored point to labeled pointcloud
		personPointsLabels.at<unsigned int>(i,0) = (unsigned int)(minIdx);		// write label number
	}
}

void selectPointsWithGivenLabel(unsigned int queryLabel, pcl::PointCloud<pcl::PointXYZ>::Ptr& inputCloud,
		cv::Mat& labels, pcl::PointCloud<pcl::PointXYZ>::Ptr& outputCloud)
{
	for (unsigned int i = 0; i < inputCloud->points.size(); i++)
	{
		if (labels.at<unsigned int>(i,0) == queryLabel)
		{
			outputCloud->points.push_back(inputCloud->points[i]);
		}
	}
}

void projectSkeletonToImage(std::vector<float>& skeletonsData, Eigen::Matrix3f& intrinsicMatrix, int NUMBER_OF_TRACKED_SKELETON,
		int NUMBER_OF_JOINT_INFO, int NUMBER_OF_SKELETAL_JOINTS)
{
	// Take skeleton 3D points and project them to the image, according to the camera intrinsic parameters
	// The (x,y) coordinates are then written at the right places of the skeletonsData vector

	// Compute projection matrix:
	Eigen::MatrixXf projectionMatrix;
	Eigen::MatrixXf m(3,4);
	m << 1,0,0,0,
			0,1,0,0,
			0,0,1,0;
	projectionMatrix = intrinsicMatrix * m;

	Eigen::Vector4f homogeneousPoint;
	Eigen::Vector3f projectedPoint;
	for (int t = 0; t < NUMBER_OF_TRACKED_SKELETON; t++)							// for every user
	{
		if (skeletonsData[t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] > 0) 	// if it is a valid user
		{
			for (int i = 0; i < NUMBER_OF_SKELETAL_JOINTS; i++)						// for every skeleton joint
			{
				// 3D skeleton point in homogeneous coordinates:
				homogeneousPoint << skeletonsData[1 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS],	// x
				                    skeletonsData[2 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS],	// y
				                    skeletonsData[3 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS],	// z
				                    1;

				// Project point to the image:
				projectedPoint = projectionMatrix * homogeneousPoint;
				projectedPoint /= projectedPoint(2);

				// Write image coordinates in the skeleton vector:
				skeletonsData[4 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = projectedPoint(0);
				skeletonsData[5 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] = projectedPoint(1);
			}
		}
	}
}

void computeSkeletonPointcloud(std::vector<float>& skeletonsData, int NUMBER_OF_TRACKED_SKELETON, int NUMBER_OF_JOINT_INFO, int NUMBER_OF_SKELETAL_JOINTS,
		pcl::PointCloud<pcl::PointXYZ>::Ptr& originalSdkSkeletonJoints)
{
	pcl::PointXYZ XYZpoint;
	for (int t = 0; t < NUMBER_OF_TRACKED_SKELETON; t++)							// for every user
	{
		if (skeletonsData[t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS] > 0) 	// if it is a valid user
		{
			for (int i = 0; i < NUMBER_OF_SKELETAL_JOINTS; i++)						// for every skeleton joint
			{
				XYZpoint.x = skeletonsData[1 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				XYZpoint.y = skeletonsData[2 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				XYZpoint.z = skeletonsData[3 + i*NUMBER_OF_JOINT_INFO + t*NUMBER_OF_JOINT_INFO*NUMBER_OF_SKELETAL_JOINTS];
				originalSdkSkeletonJoints->points.push_back(XYZpoint);
			}
		}
	}
}

void createDensePointcloud(pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud, cv::Mat& image, Eigen::Matrix3f& intrinsicMatrix,
		pcl::PointCloud<pcl::PointXYZ>::Ptr& outputPointcloud)
{
	// Create pointcloud (semi-organized - one-to-one correspondence with the depth image) from the depth pointcloud
	// For point clouds XYZ
	// Fill a pointcloud with rows x cols NaN points:
	pcl::PointXYZ newPoint;
	for (int i=0;i<image.rows;i=i++)
	{
		for (int j=0;j<image.cols;j=j++)
		{
			newPoint.z = std::numeric_limits<float>::quiet_NaN();
			newPoint.x = std::numeric_limits<float>::quiet_NaN();
			newPoint.y = std::numeric_limits<float>::quiet_NaN();
			outputPointcloud->push_back(newPoint);
		}
	}

	// Compute projection matrix:
	Eigen::MatrixXf projectionMatrix;
	Eigen::MatrixXf m(3,4);
	m << 1,0,0,0,
			0,1,0,0,
			0,0,1,0;
	projectionMatrix = intrinsicMatrix * m;

	Eigen::Vector4f homogeneousPoint;
	Eigen::Vector3f projectedPoint;

	// For every point of the input pointcloud:
	for (unsigned int i = 0; i < inputPointcloud->points.size(); i++)
	{
		// find its corresponding image coordinates:
		homogeneousPoint << inputPointcloud->points[i].x, inputPointcloud->points[i].y,	inputPointcloud->points[i].z, 1;
		projectedPoint = projectionMatrix * homogeneousPoint;
		projectedPoint /= projectedPoint(2);

		// substitute the point of the output pointcloud at that position with the current point of the input pointcloud:
		outputPointcloud->points[(unsigned int) (projectedPoint[0] + projectedPoint[1]*image.cols)] = inputPointcloud->points[i];
	}
}

void fromMillimetersToMeters(pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud)
{
	for (unsigned int i = 0; i < inputPointcloud->points.size(); i++)
	{
		inputPointcloud->points[i].x = inputPointcloud->points[i].x/1000;
		inputPointcloud->points[i].y = inputPointcloud->points[i].y/1000;
		inputPointcloud->points[i].z = inputPointcloud->points[i].z/1000;
	}
}

void findNearestInImageValidPoint(int x, int y, int width, int radius, pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud, pcl::PointXYZ& point,
		int& outX, int& outY)
{
	for (int i = 1; i < radius; i++)
	{
		for (int addX = -i; addX <= i; addX++)
		{
			for (int addY = -i; addY <= i; addY++)
			{
				point = inputPointcloud->points[x+addX + (y+addY)*width];
				if (!isnan(point.z))
				{
					outX = x+addX;
					outY = y+addY;
					return;
				}
			}
		}
	}
}

void findNearestInImageAndDirectionValidPoint(int x, int y, int x2, int y2, int width, int radius, pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud,
		pcl::PointXYZ& point, int& outX, int& outY)
{
	for (int i = 1; i < radius; i++)
	{
		for (int addX = -i; addX <= i; addX++)
		{
			for (int addY = -i; addY <= i; addY++)
			{
				if (((x < (x+addX) < x2) && (y < (y+addY) < y2)) || ((x2 < (x+addX) < x) && (y2 < (y+addY) < y)) )
				{
					point = inputPointcloud->points[x+addX + (y+addY)*width];
					if (!isnan(point.z))
					{
						outX = x+addX;
						outY = y+addY;
						return;
					}
				}
			}
		}
	}
}

void mapExtremePointsTo3D(cv::Mat& extremePoints, pcl::PointCloud<pcl::PointXYZ>::Ptr& inputPointcloud, int imageWidth, cv::Mat& extremePoints3D)
{
	pcl::PointXYZ point3D;
	for (unsigned int j = 0; j < extremePoints.rows; j++)
	{
		point3D = inputPointcloud->points[extremePoints.at<unsigned int>(j,0) + extremePoints.at<unsigned int>(j,1)*imageWidth];
		extremePoints3D.at<float>(j,0) = point3D.x;
		extremePoints3D.at<float>(j,1) = point3D.y;
		extremePoints3D.at<float>(j,2) = point3D.z;
		point3D = inputPointcloud->points[extremePoints.at<unsigned int>(j,2) + extremePoints.at<unsigned int>(j,3)*imageWidth];
		extremePoints3D.at<float>(j,3) = point3D.x;
		extremePoints3D.at<float>(j,4) = point3D.y;
		extremePoints3D.at<float>(j,5) = point3D.z;

		if ((isnan(extremePoints3D.at<float>(j,0))) || (isnan(extremePoints3D.at<float>(j,3))))
			std::cout << "*******************STOPPPPPPPP: error in mapExtremePointsTo3D!!!!" << std::endl;
	}
}

void zPassThroughFiltering(pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, float minValue, float maxValue, pcl::PointCloud<pcl::PointXYZ>::Ptr& cloudFiltered)
{
	pcl::PointXYZ currentPoint;
	for (unsigned int i = 0; i < cloud->points.size(); i++)
	{
		currentPoint = cloud->points[i];
		if ((currentPoint.z > minValue) & (currentPoint.z < maxValue))
		{
			cloudFiltered->points.push_back(currentPoint);
		}
	}
}

