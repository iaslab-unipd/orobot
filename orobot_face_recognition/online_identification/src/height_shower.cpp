#include <opt_msgs/TrackArray.h>
#include <visualization_msgs/MarkerArray.h>
#include <ros/ros.h>


opt_msgs::TrackArray _tracks;
bool _callback = false;
void tracksCb(const opt_msgs::TrackArrayConstPtr& tracks_msg)
{
    _tracks = *tracks_msg;
    _callback = true;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "height_shower");

    ros::NodeHandle nh;
    ros::Subscriber sub = nh.subscribe<opt_msgs::TrackArray>("/tracker/tracks_smoothed", 1,
                                                             tracksCb);
    ros::Publisher pub = nh.advertise<visualization_msgs::MarkerArray>("/heights", 1);
    ros::Rate rate(30);
    while(ros::ok())
    {
        while(!_callback)
        {
            ros::spinOnce();
            rate.sleep();
        }

        visualization_msgs::MarkerArray array;
        array.markers.resize(_tracks.tracks.size());
        for(int i = 0; i < _tracks.tracks.size(); ++i)
        {
            const opt_msgs::Track& t = _tracks.tracks[i];
            visualization_msgs::Marker& marker = array.markers[i];
            marker.header.frame_id = "kinect2_head_ir_optical_frame";
            marker.header.stamp = ros::Time();
            marker.id = i;
            marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
            std::stringstream ss;
            ss << t.height;
            marker.text = ss.str().c_str();
            marker.action = visualization_msgs::Marker::ADD;
            marker.pose.position.x = 1;
            marker.pose.position.y = -1;
            marker.pose.position.z = t.distance;
            marker.pose.orientation.x = 0.0;
            marker.pose.orientation.y = 0.0;
            marker.pose.orientation.z = 0.0;
            marker.pose.orientation.w = 1.0;
            marker.scale.x = 2;
            marker.scale.y = 2;
            marker.scale.z = 2;
            marker.color.a = 1.0; // Don't forget to set the alpha!
            marker.color.r = 0.0;
            marker.color.g = 1.0;
            marker.color.b = 0.0;
        }

        //only if using a MESH_RESOURCE marker type:
        pub.publish( array );

        ros::spinOnce();
        rate.sleep();
    }
    ros::shutdown();

}
