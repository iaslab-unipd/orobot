/*
 * mainIdentification.cpp
 *
 *  Created on: Oct 17, 2012
 *      Author: Matteo Munaro
 */

// ROS includes:
#include <ros/ros.h>
#include <ros/package.h>

//togliere quelli inutili
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/sac_model_plane.h>
/////////////////////////////////

#include <faceRecognition/forest.hpp>
#include <faceRecognition/multi_part_sample.hpp>
#include <faceRecognition/head_pose_sample.hpp>
#include <faceRecognition/face_utils.hpp>
#include <istream>
#include <opencv2/opencv.hpp>
#include <boost/progress.hpp>
#include <boost/thread.hpp>
#include <faceRecognition/face_forest.hpp>
#include <faceRecognition/feature_channel_factory.hpp>
#include <faceRecognition/timing.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <onlineIdentification/pointCloudUtils.h>
#include <onlineIdentification/readAndDisplayUtils.h>
#include <onlineIdentification/onlineIdFunctions.h>
#include <onlineIdentification/CandidateUsers.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/Int32.h>
//#include <action_model/IdMessage.h>
#include <opt_msgs/TrackArray.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <tf/transform_listener.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <string>

#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/synchronizer.h>
#include <message_filters/subscriber.h>

#include <sound_play/sound_play.h>
#include <unistd.h>

#include <orobot_state_manager/FaceRecognitionRequestService.h>
#include <orobot_state_manager/FaceRecognitionFinishedService.h>

using namespace std;
using namespace cv;
namespace enc = sensor_msgs::image_encodings;

cv::Mat depthDistortionParameters(1, 5, CV_32F);

// For ground selection:
std::vector<cv::Point> _points;
bool _finished = false;
bool estimate_ground = false;
Eigen::Matrix3f points;
Eigen::VectorXf plane_coeffs;

int rows = 400;
int cols = 330;

int kfd = 0;
struct termios cooked;
void quit(int sig)
{
  tcsetattr(kfd, TCSANOW, &cooked);
  ros::shutdown();
  exit(0);
}

void sleepok(int t, ros::NodeHandle &nh)
{
  if (nh.ok())
    sleep(t);
}

void createInfoWindows(int ind, int current_user_id)
{
  stringstream ss;
  int s = 400 * (ind + 1);
  if (s > 1200)
    s = 1200;
  //create new info window for this id
  ss << "ID " << current_user_id;
  cv::namedWindow(ss.str(), 0);
  cv::moveWindow(ss.str(), s, 360);
  cv::resizeWindow(ss.str(), cols, rows);
  //create new elaboration window for this id
  ss.str("");
  ss << "ELAB ID " << current_user_id;
  cv::namedWindow(ss.str(), 0);
  cv::moveWindow(ss.str(), s, 10);
  cv::resizeWindow(ss.str(), 300, 250);
  cv::waitKey(1);
}

void readHeights(std::vector<double>& heights,
                 const int number_of_users,
                 const std::string& descriptorsPath)
{
  heights.reserve(number_of_users);
  for (int user = 0; user < number_of_users; user++)
  {
    stringstream ss;
    ss << descriptorsPath << (user+1) << "_h.txt";
    double value;
    ifstream ReadFile(ss.str().c_str());
    ReadFile >> value;
    heights.push_back(value);
  }
}

void click_cb(int event, int x, int y, int flags, void* param)
{
  switch (event)
  {
    case CV_EVENT_LBUTTONUP:
      {
        //TODO control if depth is nan
        cv::Point p(x, y);
        _points.push_back(p);
        break;
      }
    case CV_EVENT_RBUTTONUP:
      {
        _finished = true;
        break;
      }
  }
}


int main(int argc, char** argv)
{

  ros::init(argc, argv, "onlineId");

  ros::NodeHandle n;
  ros::NodeHandle nh;

  /*while(!ros::service::waitForService("orobot_state_manager/face_recognition_request_service"))
  {
      ros::Rate(10).sleep();
  }
  while(!ros::service::waitForService("orobot_state_manager/face_recognition_finished_service"))
  {
      ros::Rate(10).sleep();
  }*/

  

  //onlineIdFunctions object
  //	onlineIdFunctions onlineId(5.70e+02, 0.0, 3.20e+02, 0.0, 5.70e+02, 2.40e+02,
  //			0.0, 0.0, 1.0);
  onlineIdFunctions onlineId(1081.3720703125, 0, 959.5,
                             0, 1081.3720703125, 539.5,
                             0, 0, 1);
  //

  //SoundClient
  sound_play::SoundClient sc;
  //

  //CandidateUsers object
  CandidateUsers cand_us;
  //

  //descriptors path
  string descriptorsPath =
      ros::package::getPath("online_identification") + "/descriptors/";
  //			"/home/iaslab/Workspace/Hydro/rosbuild_ws/onlineIdentification/descriptors/";
  string num_users_filePath =
      ros::package::getPath("online_identification") + "/descriptors/number_of_users.txt";
  //			"/home/iaslab/Workspace/Hydro/rosbuild_ws/onlineIdentification/descriptors/number_of_users.txt";
  string unknownPath =
      ros::package::getPath("online_identification") + "/descriptors/unknown.jpg";
  //			"/home/iaslab/Workspace/Hydro/rosbuild_ws/onlineIdentification/descriptors/unknown.jpg";
  string face_config_dir =
      ros::package::getPath("online_identification") + "/data/";
  //			"/home/iaslab/Workspace/Hydro/rosbuild_ws/onlineIdentification/data/";
  //

  //launch parameters (trainingF modeF svmF stopC)
  int training = 0; //training=0 training; else testing
  int mode = 2; 	  //mode=0 face; mode=1 skeleton; mode=2 face+skeleton
  //in training mode it's always face+skeleton
  int svm = 1;      //0 NN; else SVM
  //in training mode this flag isn't used

  int stopCondition = 0;  //0=5seconds recognition
  //1=until match recognition

  int seconds_to_take_decision = 10;
  double minimum_age_to_be_considered = 0;
  double maximum_distance_to_be_considered = 3.0f;
  int number_of_training_frames = 30;

  // Threshold defining
  double threshold_1 = 29.0f;
  double threshold_2 = 23.0f;
  double threshold_3 = 23.0f;
  double distance_1 = 1.62f;
  double distance_2 = 2.50f;
  double distance_3 = 3.41f;
  double max_height_difference = 0.03f;
  double min_frames_to_be_classified = 5;

  n.getParam("min_frames_to_be_classified", min_frames_to_be_classified);
  n.getParam("number_of_training_frames", number_of_training_frames);
  n.getParam("trainingFlag", training);
  n.getParam("modeFlag", mode);
  n.getParam("svmFlag", svm);
  n.getParam("stopCondition", stopCondition);
  n.getParam("seconds_to_take_decision", seconds_to_take_decision);
  n.getParam("minimum_age_to_be_considered", minimum_age_to_be_considered);
  n.getParam("max_distance_to_be_considered", maximum_distance_to_be_considered);
  n.getParam("threshold_1", threshold_1);
  n.getParam("threshold_2", threshold_2);
  n.getParam("threshold_3", threshold_3);
  n.getParam("distance_1", distance_1);
  n.getParam("distance_2", distance_2);
  n.getParam("distance_3", distance_3);
  n.getParam("max_height_difference", max_height_difference);


  bool trainingFlag, svmFlag;

  if (training == 0)
  {
    trainingFlag = true;
    std::cout << "--- TRAINING mode ---" << std::endl;
  }
  else
  {
    trainingFlag = false;
    std::cout << "--- TEST mode ---" << std::endl;
    if (svm != 0)
    {
      svmFlag = true;
      std::cout << "--- Using SVM ---" << std::endl;
    }
    else
    {
      svmFlag = false;
      std::cout << "--- Using NEAREST NEIGHBOR ---" << std::endl;
    }
    if (mode == 0)
      std::cout << "--- Only FACE descriptors ---" << std::endl;
    if (mode == 1)
      std::cout << "--- Only SKELETON descriptors ---" << std::endl;
    if (mode == 2)
      std::cout << "--- FACE+SKELETON descriptors ---" << std::endl;
    if (stopCondition == 0)
    {
      std::cout << "--- ''" << seconds_to_take_decision << " SECONDS'' "
                << "recognition ---" << std::endl;
    }
    if (stopCondition == 1)
    {
      std::cout << "--- ''UNTIL MATCH'' recognition ---" << std::endl;
    }
  }
  //

  //ros
  ros::Subscriber rgb_sub = n.subscribe("/kinect2_head/rgb/image", 1,
                                        &onlineIdFunctions::rgbCallback, &onlineId);
  //  ros::Subscriber cloud_sub = n.subscribe("/kinect2_head/depth_ir/points", 1,
  //                                          &onlineIdFunctions::cloudCallback, &onlineId);
  //  ros::Subscriber depth_sub = n.subscribe("/kinect2_head/depth/image",
  //                                          1, &onlineIdFunctions::depthCallback, &onlineId);
  ros::Subscriber ids_sub = n.subscribe("/tracker/tracks_smoothed", 1,
                                        &onlineIdFunctions::idsCallback, &onlineId);
  //	ros::Subscriber tracker_sub = n.subscribe("/tracker/results", 1,
  //			&onlineIdFunctions::trackerCallback, &onlineId);
  //  ros::Publisher pub = n.advertise<std_msgs::Int32>("/person_to_follow", 1);
  //  ros::Subscriber sub_lost = n.subscribe("/lost_track", 1,
  //                                         &onlineIdFunctions::lostCallback, &onlineId);
  //

  // parameters
  float height_percentage(0.3), left_width_percentage(0.2), top_height_offset(0.25),
      right_width_percentage(0.25), bottom_height_offset(0.75);
  //  n.getParam("height_percentage", height_percentage);
  //  n.getParam("top_height_offset", top_height_offset);
  //  n.getParam("right_width_percentage", right_width_percentage);
  //  n.getParam("bottom_height_offset", bottom_height_offset);
  //  n.getParam("left_width_percentage", left_width_percentage);

  //windows
  cv::namedWindow("RGB IMAGE", CV_WINDOW_KEEPRATIO);
  cv::moveWindow("RGB IMAGE", 0, 10);
  cv::namedWindow("USERS LIST", 0);
  cv::moveWindow("USERS LIST", 0, 360);
  cv::resizeWindow("USERS LIST", cols - 100, rows);
  //cv::namedWindow("DEPTH IMAGE", CV_WINDOW_KEEPRATIO);
  //cv::moveWindow("DEPTH IMAGE", 800, 10);
  //

  onlineId.rgb_cv.reset(new cv_bridge::CvImage);
  onlineId.depth_cv.reset(new cv_bridge::CvImage);

  Eigen::Matrix3f rgbIntrinsicMatrix;
  Eigen::Matrix3f depthIntrinsicMatrix;
  Eigen::Vector3f translationIR_RGB;
  Eigen::Vector3f rotationAnglesIR_RGB;
  Eigen::Affine3f depthToRGBTransform;

  int counter = 0;
  float keypointSize = 6;

  rgbIntrinsicMatrix << 1081.3720703125, 0, 959.5,
      0, 1081.3720703125, 539.5,
      0, 0, 1;
  depthIntrinsicMatrix << 365.9466857910156, 0, 257.2561950683594,
      0, 365.9466857910156, 202.9217071533203,
      0, 0, 1;

  translationIR_RGB << 0.0000, 0.00000, 0.000;
  rotationAnglesIR_RGB << 0.0, 0.0, 0.0;

  depthDistortionParameters =
      (cv::Mat_<double>(1, 5) << 0.09807992726564407, -0.2776226103305817,
       0, 0, 0.09507202357053757);
  //

  // Affine transformation between IR and RGB cameras:
  pcl::getTransformation(translationIR_RGB(0), translationIR_RGB(1),
                         translationIR_RGB(2), rotationAnglesIR_RGB(0),
                         rotationAnglesIR_RGB(1),
                         rotationAnglesIR_RGB(2), depthToRGBTransform);
  //

  //// INITIALIZATION HEAD POSE ESTIMATION AND FACE FEATURES DETECTION /////
  std::string face_cascade = face_config_dir
      + "haarcascade_frontalface_alt.xml";
  std::string headpose_config_file = face_config_dir + "config_headpose.txt";
  std::string ffd_config_file = face_config_dir + "config_ffd.txt";
  // parse config file
  ForestParam mp_param;
  loadConfigFile(ffd_config_file, mp_param);
  FaceForestOptions faceForestOption;
  faceForestOption.face_detection_option.path_face_cascade = face_cascade;
  ForestParam head_param;
  loadConfigFile(headpose_config_file, head_param);
  faceForestOption.head_pose_forest_param = head_param;
  faceForestOption.mp_forest_param = mp_param;
  faceForestOption.mp_forest_param.treePath = face_config_dir + "trees_ffd/";
  faceForestOption.head_pose_forest_param.treePath = face_config_dir
      + "trees_headpose/tree_";
  FaceForest faceForestObject = FaceForest(faceForestOption);
  //

  //READ NUMBER OF USERS & DESCRIPTORS
  //read number of users
  int number_of_users = 0;
  ifstream ReadFile(num_users_filePath.c_str());
  ReadFile >> number_of_users;
  std::cout << "--- Number of users " << number_of_users << " ---"
            << std::endl;

  //used only if test mode
  Mat descriptors;
  // Heights
  std::vector<double> heights;
  // mat in cui ogni riga è un descrittore, l'utente i ha i descr in
  // righe 30*i...30*i+29
  vector<Mat> users_faces;
  // jpg files with the face of users, index i->user i
  vector<DMatch> match;
  FlannBasedMatcher matcher;
  Mat h;
  float b = 0;
  Mat unknown = imread(unknownPath.c_str());
  std::vector<float> med;
  std::vector<float> dev;
  onlineId.fillMedDevWithZero(med, dev, mode);

  if (trainingFlag)
  {
    number_of_users++;
  }
  else
  {
    std::cout << "--- Reading descriptors. ---" << std::endl << "--- 0%  ";
    //if in test mode, read descriptors (based on mode choice)
    onlineId.readDescriptors(descriptors, users_faces, number_of_users,
                             descriptorsPath, mode);
    readHeights(heights, number_of_users, descriptorsPath);
    std::cout << "  50%  ";
    if (!svmFlag)
    { //only if NN normalize descriptors now
      onlineId.calculateMeanStdDev(descriptors, med, dev, number_of_users);
      onlineId.normalizeData(descriptors, med, dev);
      std::cout << "100% ---" << endl;
    }
    else
    { //if SVM read h and b
      //std::cout << "READING H AND B..." << std::endl;
      onlineId.read_h_b(h, b, mode,
                        ros::package::getPath("online_identification") +
                        "/data/Generic_SVM_face");
      // std::cout << b << std::endl;
      // std::cout << h << std::endl;
      std::cout << "100% ---" << endl;
    }
    std::cout << "--- Descriptors loaded and normalized. ---" << std::endl;
  }
  //

  if (!trainingFlag)
  {
    //list of users window
    onlineId.usersWindow(users_faces, cols - 100, rows);
    //
  }

  //timer
  //Timing evaluationTimer;
  //int counterTimer = 0;
  Timing timeFromStart;
  //

  Timing code;
  bool coutFlag = false;

  bool first_frame_arrived = false;

  double height = 0;
  double distance = 0;
  int counter_heights = 0;
  double actual_height = 0;

  bool face_recognition_request = false;

  //main cycle
  while (ros::ok())
  {

    ros::spinOnce();

    if (onlineId.do_identification == 0)
    { //if first identification or last authorized track was lost
      if (onlineId.new_rgb && onlineId.ids_ready)
      { //at least one rgb frame and one point cloud since start
          onlineId.ids_ready = false;
          onlineId.new_rgb = false;
        const float cols_proportion = onlineId.rgb_cv->image.cols / 512.0f;
        const float rows_proportion = onlineId.rgb_cv->image.rows / 424.0f;
        bool _finish(false);

        if (trainingFlag)
        { //TRAINING

          for (std::vector<opt_msgs::Track>::const_iterator iter =
               onlineId.tracks_.tracks.begin();
               iter < onlineId.tracks_.tracks.end() && !_finish; ++iter)
          {
            // Face ROI
            const opt_msgs::Track& t = *iter;
            if (t.age < minimum_age_to_be_considered
                ||
                t.distance > maximum_distance_to_be_considered) continue;
            const int ind = t.id;
            const float TOP_LEFT_X = t.box_2D.x * cols_proportion;
            const float TOP_LEFT_Y = t.box_2D.y * rows_proportion;
            const float WIDTH = t.box_2D.width * cols_proportion;
            const float HEIGHT =t.box_2D.height * rows_proportion;
            const float SCALED_HEIGHT = HEIGHT * height_percentage;
            cv::Point top_left(TOP_LEFT_X, TOP_LEFT_Y);
            cv::Point bottom_right = top_left + cv::Point(WIDTH, HEIGHT);
            cv::Point scaled_top_left(std::min(top_left.x + WIDTH *
                                               left_width_percentage,
                                               float(onlineId.rgb_cv->image.cols)),
                                      std::max(0.0f, top_left.y - HEIGHT *
                                               top_height_offset));
            cv::Point scaled_bottom_right(std::max(bottom_right.x - WIDTH *
                                                   left_width_percentage,
                                                   float(scaled_top_left.x)),
                                          std::max(float(scaled_top_left.y),
                                                   bottom_right.y -
                                                   HEIGHT *
                                                   bottom_height_offset));

            //face selection
            std::vector<Face> faces;
            cv::Mat imageSelectionGray;
            cv::Mat imageToDisplay = onlineId.rgb_cv->image.clone();
            onlineId.selectFaceWithROI(faceForestObject, 1, true,
                                       onlineId.rgb_cv->image, imageToDisplay,
                                       imageSelectionGray, faces, false,
                                       number_of_users,
                                       counter, trainingFlag, descriptorsPath,
                                       scaled_top_left, scaled_bottom_right);

            if (faces.size() > 0)
            { //if there is at least a face

              // draw green rectangle
              cv::rectangle(imageToDisplay, cv::Point(20, 20),
                            cv::Point(60, 60), cv::Scalar(0, 255, 0), 20);

              // Face descriptor computation:
              Face currentFace = faces[0];
              cv::Mat faceDescriptor(SURF_DESCRIPTOR_DIMENSION,
                                     NUMBER_OF_FIDUCIAL_POINTS, CV_32FC1);
              FaceForest::compute_face_descriptor(imageSelectionGray,
                                                  currentFace, faceDescriptor,
                                                  keypointSize, false);
              //save 30 face descriptors & 30 skeleton descriptors
              if (counter < number_of_training_frames)
              {
                //save the face descriptor:
                stringstream ss;
                ss << number_of_users << "_" << counter;
                string str = ss.str();
                std::string filename = descriptorsPath + str
                    + ".yml";
                FileStorage fs(filename, FileStorage::WRITE);
                fs << "faceDescriptor" << faceDescriptor; // write face features
                fs.release();
                std::cout << "Distance: " << t.distance << std::endl;
                height += t.height;
                distance += t.distance;
                std::cout
                    << "--- Saved descriptors&features files number: "
                    << (counter + 1) << " ---" << std::endl;
                counter++;
              }
              else
              {
                //save new number_of_users
                ofstream SaveFile(num_users_filePath.c_str());
                SaveFile << number_of_users;
                SaveFile.close();
                //close node
                cout << "--- Saved all descriptors ---" << endl;
                sc.say("Finished");
                cv::destroyWindow("TRAINING");
                _finish = true;
                // Save new height file
                std::stringstream ss;
                ss << descriptorsPath << number_of_users << "_h.txt";
                ofstream save_height(ss.str().c_str());
                save_height << height / counter;
                save_height.close();
                // Save new distance file
                ss.str("");
                ss.flush();
                ss << descriptorsPath << number_of_users << "_d.txt";
                ofstream save_distance(ss.str().c_str());
                save_distance << distance / counter;
                save_distance.close();
                ros::shutdown();
                return 0;
              }
            }

            if(!_finish)
            {
              //update display
              cv::imshow("TRAINING", imageToDisplay);
              cv::waitKey(1);
              //
            }

          }
        }
        else
        { //TESTING
          int i = 0;
          int distance_index = 0;
          for (uint k = 0;
               k < onlineId.tracks_.tracks.size();
               ++k)
          {
            // Face ROI
            const opt_msgs::Track& t = onlineId.tracks_.tracks[k];
            if (t.age < minimum_age_to_be_considered
                ||
                t.distance > maximum_distance_to_be_considered
                ||
                t.height < 1.0
                ||
                t.height > 2.5) { continue;}
            /*std::cout << "x: " << t.x << " y: " << t.y << std::endl;
            // Face Recognition Request
            if(!face_recognition_request) //check if I already had autorization from the state_manager
            {
                orobot_state_manager::FaceRecognitionRequestService obj;
                if(!ros::service::call("orobot_state_manager/face_recognition_request_service", obj))
                {
                    std::cout << "FACERECOGNITION: Error in requesting the navigation preemption... I'm dying..." << std::endl;
                    ros::shutdown();
                }
                if(!obj.response.ret)
                {
                    std::cout << "FACERECOGNITION: Failed to obtain the navigation preemption... I will retry..." << std::endl;
                    continue;
                }
                face_recognition_request = true;
            }*/

            actual_height = t.height;

            // Setting the correct index descriptors to match basing on the
            // track distance
            double min_1 = std::min(std::abs(t.distance - distance_1),
                                    std::abs(t.distance - distance_2));
            double min_2 = std::min(std::abs(t.distance - distance_3),
                                    min_1);
            if (min_2 != min_1) // the track is closer to the third distance
            {
              distance_index = 2;
            }
            else
            {
              if(min_1 == std::abs(t.distance - distance_2))
              { // The track is closer to the second distance
                distance_index = 1;
              }
            }
            // Selecting the descriptors
            cv::Mat closest_descriptors(descriptors.rows / 3,
                                        descriptors.cols,
                                        descriptors.type());
            for (int j = 0; j < number_of_users / 3; ++j)
            {
              int index = j * 3 + distance_index;
              for(int k = 0; k < number_of_training_frames; ++k)
              {
                descriptors.row(30 * index + k).
                    copyTo(closest_descriptors.row(j + k));
              }
            }

            const int ind = t.id;
            const float TOP_LEFT_X = t.box_2D.x * cols_proportion;
            const float TOP_LEFT_Y = t.box_2D.y * rows_proportion;
            const float WIDTH = t.box_2D.width * cols_proportion;
            const float HEIGHT =t.box_2D.height * rows_proportion;
            const float SCALED_HEIGHT = HEIGHT * height_percentage;
            cv::Point top_left(TOP_LEFT_X, TOP_LEFT_Y);
            cv::Point bottom_right = top_left + cv::Point(WIDTH, HEIGHT);
            cv::Point scaled_top_left(std::min(top_left.x + WIDTH *
                                               left_width_percentage,
                                               float(onlineId.rgb_cv->image.cols)),
                                      std::max(0.0f, top_left.y - HEIGHT *
                                               top_height_offset));
            cv::Point scaled_bottom_right(std::max(bottom_right.x - WIDTH *
                                                   left_width_percentage,
                                                   float(scaled_top_left.x)),
                                          std::max(float(scaled_top_left.y),
                                                   bottom_right.y -
                                                   HEIGHT *
                                                   bottom_height_offset));

            int current_user_id = t.id; //onlineId.active_ids[i++];

            //test print for id linking distance
            //onlineId.printDistances(current_user_id);

            if (cand_us.containsId(current_user_id) == -1)
            { //if new id create info windows and add id in cand_us

              cand_us.addId(current_user_id, code,
                            number_of_users);
              createInfoWindows(ind, current_user_id);
            }


            //face selection
            std::vector<Face> faces;
            cv::Mat imageSelectionGray;
            cv::Mat imageToDisplay =
                onlineId.rgb_cv->image.clone();
            onlineId.selectFaceWithROI(faceForestObject, 1, true,
                                       onlineId.rgb_cv->image, imageToDisplay,
                                       imageSelectionGray, faces, false,
                                       number_of_users, counter, trainingFlag,
                                       descriptorsPath,
                                       scaled_top_left, scaled_bottom_right);

            //show elaboration image
            stringstream ss;
            ss << "ELAB ID " << current_user_id;
            resize(imageToDisplay, imageToDisplay,
                   Size(300, 250));
            cv::imshow(ss.str(), imageToDisplay);
            cv::waitKey(1);

            if(faces.size() == 0) continue;


            // draw green rectangle
            cv::rectangle(imageToDisplay, cv::Point(20, 20),
                          cv::Point(60, 60), cv::Scalar(0, 255, 0),
                          20);

            // Face descriptor computation:
            Face currentFace = faces[0];
            cv::Mat faceDescriptor(SURF_DESCRIPTOR_DIMENSION,
                                   NUMBER_OF_FIDUCIAL_POINTS, CV_32FC1);
            FaceForest::compute_face_descriptor(
                  imageSelectionGray, currentFace,
                  faceDescriptor, keypointSize, false);
            //

            //create current descriptor based on selected mode
            Mat current_descriptor;
            if (mode == 0)
            { //current_descriptor only face
              current_descriptor = faceDescriptor.reshape(1,
                                                          1);
            }
            //match result variables
            int best_user;
            float score;

            if (svmFlag)
            { //SVM

              //reset med and dev
              onlineId.fillMedDevWithZero(med, dev, mode);

              //create current SVM table
              Mat svmTable(descriptors.rows,
                           descriptors.cols,
                           CV_32FC1);
              onlineId.normalizeSVM(current_descriptor,
                                    descriptors, svmTable, med, dev,
                                    number_of_users);
              //

              //find match
              onlineId.matchSVM(svmTable, h, b, best_user,
                                score, number_of_users);
            }
            else
            { //Nearest Neighbor

              //normalize current descriptors
              onlineId.normalizeData(current_descriptor, med,
                                     dev);
              //

              //flann
              matcher.match(current_descriptor, descriptors,
                            match);
              best_user = (int) (match[0].trainIdx / 30);
              score = match[0].distance;
              cout << best_user << " " << score << " " << t.height << " "
                   << heights[best_user] << " ";
              if (std::abs(t.height - heights[best_user]) >
                  max_height_difference)
              {
                std::cout << "Rejected for height constraint";
                best_user = -1;
                score = std::numeric_limits<int>::max();
              }
              std::cout << std::endl;
            }
            //            cout << "bestuser " << best_user << " score "
            //                 << score << " height: " << t.height << "  " << t.age

            double threshold;
            if(distance_index == 0)
            {
              threshold = threshold_1;
            }
            else if (distance_index == 1)
            {
              threshold = threshold_2;
            }
            else
            {
              threshold = threshold_3;
            }

            if ((svmFlag
                 && ((mode == 0 && score >= 190)
                     || (mode == 1 && score >= 30)
                     || (mode == 2 && score >= 0)))
                ||
                (!svmFlag
                 && ((mode == 0 && score <= threshold)
                     || (mode == 1 && score <= 1.05)
                     || (mode == 2 && score <= 41))))
            {
              //if the score of the found match is good enough

              //cout << "Adding to data structure: "
              // 	<< current_user_id << ", " << best_user
              //		<< ", " << code << ", " << endl;
              //update data structure
              cand_us.addMatch(current_user_id, best_user,
                               code, number_of_users);

              //show matched user
              Mat userface = users_faces[best_user].clone();
              onlineId.showMatch(current_user_id, best_user,
                                 cand_us.containsId(current_user_id),
                                 cand_us, code, userface, number_of_users,
                                 cols, rows, false);
            }
            else
            {
              //update data structure (only the code)
              cand_us.updateCode(ind, code);
              cand_us.unknownMatch();

              //show black screen
              onlineId.showMatch(current_user_id, -1,
                                 cand_us.containsId(current_user_id),
                                 cand_us, code, unknown, number_of_users,
                                 cols, rows, false);
            }

          }


          //stop conditions
          //          std::cout << "Elapsed: " << timeFromStart.elapsed() << std::endl;
          //          std::cout << "Seconds: " << seconds_to_take_decision * 1000 <<
          //                       std::endl;
          if (
              stopCondition == 0
              && timeFromStart.elapsed() > seconds_to_take_decision * 1000)
          {  //15seconds recognition

            cand_us.printMatches();

            cout << "--- " << seconds_to_take_decision <<
                    " seconds finished ---" << endl;
            int best_id = cand_us.findMatchID(code);
            int best_index = cand_us.findMatchINDEX();
            int best_u = cand_us.findMatchUSER();

            stringstream ss;
            ss << "ID " << best_id;

            // Ordering matches
            std::pair<int, int> maximum_match;
            maximum_match.second = 0;
            std::vector<int> matches(number_of_users / 3,0);
            int n_frames = 0;
            int i = 0;
            for( std::map<int, int>::iterator iter = cand_us.matches_.begin();
                 iter != cand_us.matches_.end();
                 ++iter)
            {
                matches[i++ / 3] += iter->second;
            }
            matches[0] -= cand_us.matches_[-1]; //deleting unknown detection to the first person
            for(int u = 0; u < matches.size(); ++u)
            {
                if(maximum_match.second < matches[u])
                {
                    maximum_match.second = matches[u];
                    maximum_match.first = u;
                }
            }

            std::cout << "Real matches: ";
            std::copy(matches.begin(), matches.end(), std::ostream_iterator<int>(std::cout, " "));
            std::cout << std::endl;

            // if "giusto"
            if (/*best_index != -1
                && ((float) cand_us.candidate[best_index].matches[best_u]
                    / (float) cand_us.candidate[best_index].totalmatches)
                >= 0.5*/
                maximum_match.second > min_frames_to_be_classified)
            { //matched user is valid
              //

              //update window
              Mat userface = users_faces[best_u].clone();
              onlineId.showMatch(best_id, best_u, best_index, cand_us,
                                 code, userface, number_of_users, cols, rows,
                                 true);

              //send the matched id in the topic person_to_follow
              //              onlineId.sendIdMsg(pub, best_id);
              //reset candidateusers
              cand_us.resetAll();
              sc.say("Ciao! Ti conosco!", "voice_lp_diphone");
              ros::shutdown();
              return 0;
            }
            else
            {
              //user not authorized, exit
              cout << "--- AUTHORIZED USER NOT FOUND ---" << endl;
              sc.say("Non conosciuto","voice_lp_diphone");
              sleepok(2, nh);
              break;
            }
			/*
            // Comunicate to the state manager that the navigation can resume
            if (face_recognition_request)
            {
                orobot_state_manager::FaceRecognitionFinishedService obj;
                if(!ros::service::call("orobot_state_manager/face_recognition_finished_service", obj))
                {
                    std::cout << "FACERECOGNITION: Could not resume the navigation... I'm dying..." << std::endl;
                    ros::shutdown();
                }
                else
                {
                    face_recognition_request = false;
                }
            }*/

          }

          if (cand_us.ids_contained > 0 && stopCondition == 1)
          {  //until found recognition

            int best_id = cand_us.findMatchID(code);
            int best_index = cand_us.findMatchINDEX();
            int best_u = cand_us.findMatchUSER();

            if (best_index != -1
                && cand_us.candidate[best_index].totalmatches >= 25)
            { //matched user, check if valid
              if (((float) cand_us.candidate[best_index].matches[best_u]
                   / (float) cand_us.candidate[best_index].totalmatches)
                  >= 0.5)
              { //matched user is valid

                //update info window
                stringstream ss;
                ss << "ID " << best_id;
                Mat userface = users_faces[best_u].clone();
                onlineId.showMatch(best_id, best_u, best_index,
                                   cand_us, code, userface, number_of_users,
                                   cols, rows, true);

                //send the matched id in the topic person_to_follow
                //                onlineId.sendIdMsg(pub, best_id);
                //reset candidateusers
                cand_us.resetAll();
                sc.say("User found");
                sleepok(2, nh);
              }
              else
              {
                cand_us.candidate[best_index].resetMatches(code);
                cout << "Reset matches of user" << best_index
                     << endl;
              }
            }
          }

          if (onlineId.do_identification == 2)
          { sc.say("Error");
            sleepok(2, nh);
            break;
          }
          //
        }
      }
      else
      {
        if (!coutFlag)
        {
          cout << "--- Waiting PointCloud message ---" << endl;
          timeFromStart.restart();
          coutFlag = true;
        }
      }

    }
  }
  //

  cout << "--- Node onlineIdentification will end ---" << endl;
  // sc.say("Closing");
  // sleepok(2, nh);

  return 0;
}
