#ifndef CANDIDATE_USERS_H_
#define CANDIDATE_USERS_H_

#include <stdio.h>
#include <stdlib.h>
#include <onlineIdentification/Cu.h>
#include <vector>
#include <iostream>
#include <faceRecognition/timing.hpp>
#include <map>

class CandidateUsers
{

public:

	/**
	 * \brief Constructor.
	 */
	CandidateUsers();

	/**
	 * \brief Add a new id in candidate vector.
	 *
	 * \param[in] id openni skeleton id of the user.
	 * \param[in] code current code.
	 * \param[in] number_of_users number of trained users.
	 */
	void addId(int id, Timing code, int number_of_users);

	/**
	 * \brief Add a match to a user.
	 *
	 * \param[in] id openni skeleton id of the user.
	 * \param[in] best_user index of the matched user.
	 * \param[in] code current code.
	 * \param[in] number_of_users number of trained users.
	 */
	void addMatch(int id, int best_user, Timing code, int number_of_users);

	/**
	 * \brief Update code of a certain user.
	 *
	 * \param[in] id openni skeleton id of the user.
	 * \param[in] code new code.
	 */
	void updateCode(int id, Timing code);

	/**
	 * \brief Check if the candidate vector contains an entry with a certain id.
	 *
	 * \param[in] id the id to check.
	 * \param[out] vector index of the found element or -1 if the element isn't contained.
	 */
	int containsId(int id);

	/**
	 * \brief Return the id of the best match user.
	 *
	 * \param[in] code current code.
	 * \param[out] id of the best matched user.
	 */
	int findMatchID(Timing code);

	/**
	 * \brief Return the index of the best match user. Use AFTER findMatchID.
	 *
	 * \param[out] "candidate" index of the best matched user.
	 */
	int findMatchINDEX();

	/**
	 * \brief Return the user index of the best match user. Use AFTER findMatchID.
	 *
	 * \param[out] user index of the best matched user.
	 */
	int findMatchUSER();

	void resetAll();

	void stamp();

	/**
	 * @brief Adds a match for the unknown to the data structure
	 */
	void unknownMatch();

	void printMatches();

	std::vector<Cu> candidate;
	int last_id_matched;
	int last_index_matched;
	int best_id, best_index, best_user, ids_contained;
	std::map<int, int> matches_;

private:

};

#endif /* CANDIDATE_USERS_H_ */
