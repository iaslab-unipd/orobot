#!/usr/bin/env python3

# NOTE: this example requires PyAudio because it uses the Microphone class

import speech_recognition as sr
from record_audio_from_microphone import *
from os import path

# obtain audio from the microphone
r = sr.Recognizer()
#with sr.Microphone() as source:
#    print("Say something!")
#    audio = r.listen(source, timeout=5)

record_to_file('demo.wav') 
AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)), "demo.wav")

# use the audio file as the audio source
r = sr.Recognizer()
with sr.AudioFile(AUDIO_FILE) as source:
    audio = r.record(source) # read the entire audio file

# recognize speech using Google Speech Recognition
try:
    print("Google Speech Recognition thinks you said " + r.recognize_google(audio, key="AIzaSyAcvgKoUOJeLyri7h6fJV-cgOTBNoFeVOQ", language="it-IT"))
except sr.UnknownValueError:
    print("Google Speech Recognition could not understand audio")
except sr.RequestError as e:
    print("Could not request results from Google Speech Recognition service; {0}".format(e))