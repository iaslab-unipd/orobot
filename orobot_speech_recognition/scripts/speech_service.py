#!/usr/bin/env python

from orobot_speech_recognition.srv import *
import rospy
import speech_recognition as sr
from record_audio_from_microphone import *
from os import path
# remove accents
from unidecode import unidecode

# for suppressing annoying errors on terminal
from ctypes import *
from contextlib import contextmanager
ERROR_HANDLER_FUNC = CFUNCTYPE(None, c_char_p, c_int, c_char_p, c_int, c_char_p)
def py_error_handler(filename, line, function, err, fmt):
        pass
c_error_handler = ERROR_HANDLER_FUNC(py_error_handler)
@contextmanager
def noalsaerr():
        asound = cdll.LoadLibrary('libasound.so')
        asound.snd_lib_error_set_handler(c_error_handler)
        yield
        asound.snd_lib_error_set_handler(None)


def handle_request(req):
    print ("Arrived")
    record_to_file("/tmp/demo.wav") 
    AUDIO_FILE = path.join(path.dirname(path.realpath(__file__)), "/tmp/demo.wav")

    # use the audio file as the audio source
    r = sr.Recognizer()
    with sr.AudioFile(AUDIO_FILE) as source:
        audio = r.record(source) # read the entire audio file

    try:
        recognized = r.recognize_google(audio, key="AIzaSyAcvgKoUOJeLyri7h6fJV-cgOTBNoFeVOQ", language="it-IT")
        print("Google Speech Recognition thinks you said " + recognized)
        if "SI" in unidecode(recognized.upper()):
            return SpeechRecognitionResponse(1)
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
        return SpeechRecognitionResponse(-1)
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))
        return SpeechRecognitionResponse(-2)
    return SpeechRecognitionResponse(0)

def speech_recognition_server():
    rospy.init_node('speech_recognition_server')
    s = rospy.Service('speech_recognition', SpeechRecognition, handle_request)
    print ("Ready for speech recognition!")
    rospy.spin()

if __name__ == "__main__":
        speech_recognition_server()