#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <visualization_msgs/MarkerArray.h>
#include <std_srvs/Empty.h>

#include "coverage_planner/CoveragePlanner.h"
#include "orobot_state_manager/NavigationAckService.h"
#include "simple_navigation_goals/StopSimpleNavigation.h"

//#include <geometry_msgs/PoseWithCovarianceStamped.h>
//#include <tf/transform_listener.h>
#include <clear_costmap_recovery/clear_costmap_recovery.h>
//#include <kobuki_msgs/BumperEvent.h>
#include <geometry_msgs/Twist.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

namespace simple_navigation {

class SimpleNavigator {

private:
    ros::NodeHandle nh_;
    ros::ServiceServer stop_navigation_service_;
    bool preemption_request_;
    uint k_;
    visualization_msgs::MarkerArray marker_array_;
    const int RECOVERY_THRESHOLD_;
    const int TRIAL_THRESHOLD_;
    const int MAX_TRIALS_;

    bool stop_navigation(simple_navigation_goals::StopSimpleNavigation::Request  &req,
        simple_navigation_goals::StopSimpleNavigation::Response &res)
    {
      preemption_request_ = true;

      ROS_INFO("Preemption request...");

      return true;
    }

    void publishMarker(const std::vector<move_base_msgs::MoveBaseGoal>& goals, const int i)
    {
        for(int j = 0; j < marker_array_.markers.size(); ++j)
        {
            visualization_msgs::Marker& marker = marker_array_.markers[j];
            marker.header.seq = ++k_;
            marker.header.frame_id = "map";
            marker.header.stamp = ros::Time();
            marker.ns = "my_namespace";
            marker.id = j;
            marker.type = visualization_msgs::Marker::SPHERE;
            marker.action = visualization_msgs::Marker::ADD;
            marker.pose.position.x = goals[j].target_pose.pose.position.x;
            marker.pose.position.y = goals[j].target_pose.pose.position.y;
            marker.pose.position.z = goals[j].target_pose.pose.position.z;
            marker.pose.orientation.x = 0.0;
            marker.pose.orientation.y = 0.0;
            marker.pose.orientation.z = 0.0;
            marker.pose.orientation.w = 1.0;
            marker.scale.x = 1;
            marker.scale.y = 0.2;
            marker.scale.z = 0.2;
            marker.color.a = 1.0; // Don't forget to set the alpha!
            marker.color.r = 0.0;
            marker.color.g = 0.0;
            marker.color.b = 1.0;
            if (j == i)
            {
                marker.color.b = 0.0;
                marker.color.g = 1.0;
                marker.color.r = 0.0;
            }
            else if(j > i)
            {
                marker.color.b = 0.0;
                marker.color.g = 0.0;
                marker.color.r = 1.0;

            }
            //only if using a MESH_RESOURCE marker type:
            //marker.mesh_resource = "package://pr2_description/meshes/base_v0/base.dae";
        }
    }

    bool
    send_ack(const bool success)
    {
        ros::ServiceClient client = nh_.serviceClient<orobot_state_manager::NavigationAckService>
                ("orobot_state_manager/one_goal_navigation_ack");

        orobot_state_manager::NavigationAckService srv;
        srv.request.success = success;

        if (client.call(srv))
        {
            ROS_INFO("Sent");
            return true;
        }
        else
        {
            ROS_ERROR("Failed to call service orobot_state_manager/one_goal_navigation_ack");
            return false;
        }
    }

public:

    int
    fake_get_and_send_goals()
    {
        ros::Rate loop_rate(5.0);
        while (ros::ok())
        {
            std::cout << "fake_get_and_send_goals" << std::endl;

            if (preemption_request_)
            {
                std::cout << "premption_request_" << std::endl;
                preemption_request_ = false;
            }

            ros::spinOnce();
            loop_rate.sleep();
        }
    }

    int
    get_and_send_goals()
    {
        // To send each goal

        MoveBaseClient ac("move_base", true);
        while(!ac.waitForServer(ros::Duration(5.0)))
        {
            ROS_INFO("Waiting for the move_base action server to come up");
        }

        // To manage goals

        ros::Publisher vis_pub = nh_.advertise<visualization_msgs::MarkerArray>( "goals_marker", 0 );
        ros::ServiceClient client = nh_.serviceClient<orobot_srvs::GoalsArray>("goals_array");

        // For recovery

        ros::ServiceClient clear_maps_client = nh_.serviceClient<std_srvs::Empty>("/move_base/clear_costmaps");
        ros::Publisher cmd_vel_pub = nh_.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);

        // Get goals (also more than one per time) and send ack

        ros::Rate loop_rate(0.5);
        while (ros::ok())
        {
            std::cout << "Waiting for next goal" << std::endl;

            // Get goals, also more than one per time

            orobot_srvs::GoalsArray srv;
            std_srvs::Empty empty;
            srv.request.req = true;
            if (client.call(srv))
            {
                std::copy(srv.response.goals.begin(), srv.response.goals.end(),
                          std::ostream_iterator<move_base_msgs::MoveBaseGoal>(std::cout, "\n"));
                marker_array_.markers.resize(srv.response.goals.size());
            }
            else
            {
                ROS_ERROR_STREAM("orobot_srvs_client: Failed to call service goals_array");
                ros::spinOnce();
                loop_rate.sleep();
                continue;
            }

            std::cout << "srv.response.goals.size() " << srv.response.goals.size() << std::endl;
            if (srv.response.goals.size() == 1)
            {

                // Try to reach them
                // Each movement can be preempted and the robot stopped

                int number_of_trials = 1;
                std::vector<bool> responses(srv.response.goals.size(), false);
                for (unsigned int i = 0; ros::ok() && i < srv.response.goals.size(); i++)
                {
                    std::cout << "Sending goal " << i + 1 << " of " << srv.response.goals.size() << std::endl;
                    std::cout << "number_of_trials " << number_of_trials << std::endl;

                    // Recovery procedure

                    if (number_of_trials >= RECOVERY_THRESHOLD_)
                    {
                        if (number_of_trials >= TRIAL_THRESHOLD_)
                        {
                            std::cout << "Robot is stuck, unstucking..." << std::endl;

                            geometry_msgs::Twist base_cmd;
                            base_cmd.linear.x = -0.2;
                            base_cmd.linear.y = base_cmd.linear.z = 0.0;
                            base_cmd.angular.x = base_cmd.angular.y = base_cmd.angular.z = 0.0;
                            ros::Duration duration(0.1);
                            for(unsigned int i = 0; i < 30; ++i)
                            {
                              cmd_vel_pub.publish(base_cmd);
                              duration.sleep();
                            }
                        }

                        // clear map (to be sure...)
                        // if it doesn't work please check laser_min_range in amcl.launch.xml

                        std::cout << "Clearing costmaps...";
                        bool ret = clear_maps_client.call(empty);
                        if(!ret)
                        {
                            std::cerr << "I cannot do it!" << std::endl;
                        }
                        else
                        {
                            std::cout << "Done!" << std::endl;
                        }

                        // reset amcl covariance (it seems useless...)
                        /*
                        std::cout << "Resetting amcl covariance...";
                        ros::param::set("/amcl/initial_cov_xx", 0.5*0.5);
                        ros::param::set("/amcl/initial_cov_yy", 0.5*0.5);
                        ros::param::set("/amcl/initial_cov_aa", (PI/12.0)*(PI/12.0));
                        std::cout << "Done!" << std::endl;
                        */
                    }

                    // Publish goal and wait
                    // Stop if preemption is requested

                    move_base_msgs::MoveBaseGoal goal = srv.response.goals[i];

                    publishMarker(srv.response.goals, i);
                    vis_pub.publish(marker_array_);
                    ros::spinOnce();

                    ac.sendGoal(goal);

                    actionlib::SimpleClientGoalState res = ac.getState();
                    while(!res.isDone())
                    {
                        std::cout << "state " << res.toString() << std::endl;

                        if (preemption_request_)
                        {
                            ac.cancelAllGoals();
                            break;
                        }

                        res = ac.getState();

                        ros::spinOnce();
                        loop_rate.sleep();
                    }

                    // Feedback

                    res = ac.getState();
                    if (res  == actionlib::SimpleClientGoalState::SUCCEEDED)
                    {
                        ROS_INFO("Hooray, the base moved forward");
                        number_of_trials = 1;
                        responses[i] = true;
                    }
                    else if (preemption_request_ /*res  == actionlib::SimpleClientGoalState::RECALLED
                             || res  == actionlib::SimpleClientGoalState::PREEMPTED*/)
                    {
                        ROS_INFO("Preemption!");
                        number_of_trials = 1;
                        responses[i] = false;
                        preemption_request_ = false;
                        break;
                    }
                    else
                    {
                        ROS_INFO("The base failed to move forward for some reason");

                        if (number_of_trials < MAX_TRIALS_)
                        {
                            ROS_INFO("Retrying");
                            number_of_trials++;
                            i--; //redo current goal
                        }
                        else
                        {
                            ROS_INFO("Max number of trials reached. Skipping to next goal");
                            number_of_trials = 1;
                        }
                    }
                }

                std::cout << "Sending ack to state manager" << std::endl;
                bool success = true;
                for (unsigned int i = 0; i < responses.size(); i++)
                {
                    if (!responses[i])
                        success = false;
                }
                send_ack(success);
            }
        }

        return 0;
    }

    SimpleNavigator() :
        nh_(),
        preemption_request_(false),
        k_(0),
        marker_array_(),
        RECOVERY_THRESHOLD_(2),
        TRIAL_THRESHOLD_(5),
        MAX_TRIALS_(10)
    {
        // For making the node preemptable
        stop_navigation_service_ = nh_.advertiseService("one_goal_navigation_node/stop_navigation", &SimpleNavigator::stop_navigation, this);
    }

};

}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "one_goal_navigation");

    simple_navigation::SimpleNavigator sn;
    int ret = sn.get_and_send_goals();

    ros::spin();
    return 0;
}




