#!/bin/bash
UBUNTU_VERSION=`lsb_release -c -s`
ROS_DISTRO=indigo
 
#if [ $UBUNTU_VERSION = trusty ] || [ $UBUNTU_VERSION = saucy ] ; then
#  ROS_DISTRO=indigo
#fi

ROS_PACKAGES="python-rosinstall ros-$ROS_DISTRO-robot-state-publisher ros-$ROS_DISTRO-cmake-modules ros-$ROS_DISTRO-openni-launch ros-$ROS_DISTRO-openni2-launch ros-$ROS_DISTRO-camera-info-manager-py ros-$ROS_DISTRO-cmake-modules ros-$ROS_DISTRO-urg-node ros-$ROS_DISTRO-hokuyo-node ros-$ROS_DISTRO-hector-slam ros-$ROS_DISTRO-frontier-exploration"

TURTLEBOT="ros-$ROS_DISTRO-orobot ros-$ROS_DISTRO-orobot-apps ros-$ROS_DISTRO-orobot-interactions ros-$ROS_DISTRO-orobot-simulator ros-$ROS_DISTRO-kobuki-ftdi"

APPS="nvidia-331 terminator synaptic qtcreator valgrind vim-gnome gitg nautilus-open-terminal gimp meld mplayer"

echo "deb http://packages.ros.org/ros/ubuntu $UBUNTU_VERSION main" | sudo tee /etc/apt/sources.list.d/ros-latest.list
wget http://packages.ros.org/ros.key -O - | sudo apt-key add -
sudo apt-get update
sudo apt-get upgrade -y --force-yes
sudo apt-get install ros-$ROS_DISTRO-desktop-full -y
sudo rosdep init
rosdep update
sudo apt-get install $ROS_PACKAGES -y --force-yes
sudo apt-get install $TURTLEBOT -y --force-yes
## For Kinect1 problems wit 14.04
sudo dpkg -i libopenni-sensor-primesense*
#####

sudo apt-get install $APPS -y --force-yes

### For Kinect 2
## Replacing OpenCL for Nvidia Computer
sudo apt-get install -y --force-yes ocl-icd-opencl-dev
## Install with the script
chmod +x kinect2_install.sh
./kinect2_install.sh
##########


# Configure Workspace
source /opt/ros/indigo/setup.bash
mkdir -p ~/workspace/ros/catkin/src
cd ~/workspace/ros/catkin
catkin_make --force-cmake
echo "source ~/workspace/ros/catkin/devel/setup.bash" >> ~/.bashrc

#fixing description.launch.xml with the one linked to the correct model
yes | sudo cp description.launch.xml /opt/ros/$ROS_DISTRO/share/orobot_bringup/launch/includes

#replacing minimal driver fro our repo
yes | sudo cp minimal.launch.xml /opt/ros/$ROS_DISTRO/share/orobot_bringup/launch

#open_ptrack for people detection
source ~/workspace/ros/catkin/devel/setup.bash
roscd 
cd ../src
git clone https://github.com/OpenPTrack/open_ptrack.git
cd open_ptrack/scripts
sh openptrack_install.sh
sh openptrack_install.sh



